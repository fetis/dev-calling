<?php
$JOB = array (
  'act' => 'restore',
  'type' => 'run',
  'db' => 'landing',
  'charset' => '0',
  'file' => 'landing_2014-07-08_11-23-06.sql.gz',
  'strategy' => '0',
  'correct' => '0',
  'autoinc' => '0',
  'obj' => 
  array (
    'TA' => 
    array (
      0 => '*',
    ),
  ),
  'job' => 'esxiC2mf',
  'file_ext' => 'sql.gz',
  'file_name' => 'backup/landing_2014-07-08_11-23-06.sql.gz',
  'file_tmp' => 'backup/landing_2014-07-08_11-23-06.sql.gz',
  'file_rtl' => 'backup/esxiC2mf.rtl',
  'file_log' => 'backup/esxiC2mf.log',
  'file_stp' => 'backup/esxiC2mf.stp',
  'todo' => 
  array (
    'TA' => 
    array (
      0 => 
      array (
        0 => 'TA',
        1 => 'access_log',
        2 => '172',
        3 => '65536',
      ),
      1 => 
      array (
        0 => 'TA',
        1 => 'charge',
        2 => '5',
        3 => '16384',
      ),
      2 => 
      array (
        0 => 'TA',
        1 => 'city',
        2 => '4370',
        3 => '1589248',
      ),
      3 => 
      array (
        0 => 'TA',
        1 => 'delivery_state',
        2 => '8',
        3 => '16384',
      ),
      4 => 
      array (
        0 => 'TA',
        1 => 'delivery_tracking',
        2 => '1100',
        3 => '5783552',
      ),
      5 => 
      array (
        0 => 'TC',
        1 => 'delivery_type',
        2 => '0',
        3 => '16384',
      ),
      6 => 
      array (
        0 => 'TC',
        1 => 'form_field',
        2 => '0',
        3 => '16384',
      ),
      7 => 
      array (
        0 => 'TA',
        1 => 'image',
        2 => '63',
        3 => '16384',
      ),
      8 => 
      array (
        0 => 'TA',
        1 => 'item',
        2 => '37',
        3 => '16384',
      ),
      9 => 
      array (
        0 => 'TC',
        1 => 'item_input',
        2 => '0',
        3 => '16384',
      ),
      10 => 
      array (
        0 => 'TA',
        1 => 'item_log',
        2 => '94',
        3 => '65536',
      ),
      11 => 
      array (
        0 => 'TA',
        1 => 'landing',
        2 => '2',
        3 => '16384',
      ),
      12 => 
      array (
        0 => 'TA',
        1 => 'landing_category',
        2 => '5',
        3 => '16384',
      ),
      13 => 
      array (
        0 => 'TA',
        1 => 'landing_extlist',
        2 => '5',
        3 => '16384',
      ),
      14 => 
      array (
        0 => 'TA',
        1 => 'landing_list',
        2 => '3',
        3 => '16384',
      ),
      15 => 
      array (
        0 => 'TA',
        1 => 'landing_region',
        2 => '12',
        3 => '16384',
      ),
      16 => 
      array (
        0 => 'TA',
        1 => 'landing_settings',
        2 => '12',
        3 => '16384',
      ),
      17 => 
      array (
        0 => 'TA',
        1 => 'landing_textblock',
        2 => '1',
        3 => '16384',
      ),
      18 => 
      array (
        0 => 'TA',
        1 => 'option',
        2 => '12',
        3 => '16384',
      ),
      19 => 
      array (
        0 => 'TA',
        1 => 'order',
        2 => '5657',
        3 => '7880704',
      ),
      20 => 
      array (
        0 => 'TA',
        1 => 'order_item',
        2 => '5761',
        3 => '311296',
      ),
      21 => 
      array (
        0 => 'TA',
        1 => 'order_log',
        2 => '38550',
        3 => '76120064',
      ),
      22 => 
      array (
        0 => 'TA',
        1 => 'order_review',
        2 => '23',
        3 => '16384',
      ),
      23 => 
      array (
        0 => 'TA',
        1 => 'order_state',
        2 => '11',
        3 => '16384',
      ),
      24 => 
      array (
        0 => 'TA',
        1 => 'order_state_arc',
        2 => '15',
        3 => '16384',
      ),
      25 => 
      array (
        0 => 'TA',
        1 => 'order_state_exclude',
        2 => '4',
        3 => '16384',
      ),
      26 => 
      array (
        0 => 'TA',
        1 => 'order_state_log',
        2 => '44',
        3 => '16384',
      ),
      27 => 
      array (
        0 => 'TA',
        1 => 'project',
        2 => '12',
        3 => '16384',
      ),
      28 => 
      array (
        0 => 'TA',
        1 => 'project_log',
        2 => '48',
        3 => '16384',
      ),
      29 => 
      array (
        0 => 'TA',
        1 => 'project_option',
        2 => '3',
        3 => '16384',
      ),
      30 => 
      array (
        0 => 'TC',
        1 => 'project_salary',
        2 => '0',
        3 => '16384',
      ),
      31 => 
      array (
        0 => 'TA',
        1 => 'project_settings',
        2 => '22',
        3 => '16384',
      ),
      32 => 
      array (
        0 => 'TA',
        1 => 'region',
        2 => '52',
        3 => '65536',
      ),
      33 => 
      array (
        0 => 'TA',
        1 => 'remote_log',
        2 => '5503',
        3 => '1589248',
      ),
      34 => 
      array (
        0 => 'TA',
        1 => 'request_log',
        2 => '23502',
        3 => '41484288',
      ),
      35 => 
      array (
        0 => 'TA',
        1 => 'resource',
        2 => '9',
        3 => '16384',
      ),
      36 => 
      array (
        0 => 'TC',
        1 => 'resource_role',
        2 => '0',
        3 => '16384',
      ),
      37 => 
      array (
        0 => 'TA',
        1 => 'role',
        2 => '2',
        3 => '16384',
      ),
      38 => 
      array (
        0 => 'TA',
        1 => 'service_log',
        2 => '1110',
        3 => '7880704',
      ),
      39 => 
      array (
        0 => 'TC',
        1 => 'stat_monthly',
        2 => '0',
        3 => '16384',
      ),
      40 => 
      array (
        0 => 'TC',
        1 => 'task',
        2 => '0',
        3 => '16384',
      ),
      41 => 
      array (
        0 => 'TA',
        1 => 'user',
        2 => '15',
        3 => '16384',
      ),
      42 => 
      array (
        0 => 'TA',
        1 => 'user_role_project',
        2 => '48',
        3 => '16384',
      ),
      43 => 
      array (
        0 => 'TC',
        1 => 'user_settings',
        2 => '0',
        3 => '16384',
      ),
    ),
  ),
);
?>