App.Views.ImageLoader = Backbone.View.extend({
	
	values : null,
	uploader_panel : null,
	multiple: true,
	urlUpload:'/image/default/upload/',
	urlCancel:'/image/default/cancel/',
	urlLoad:'/image/default/load/',
	urlUpdate:'/image/default/update/',
	use_crop:false,
	crop_width:'',
	crop_height:'',
    maxConnections: 3,
    allowedExtensions: ['png','jpg','gif','jpeg'],               
    sizeLimit: 0,   
    minSizeLimit: 0,
    multiple:true,
    delimiter : ',',
	
	initialize:function (options) {
		var value = this.$el.val()
			;
		this.values = this.explode(this.delimiter, value);
        return this;
    },
    
    render : function() {
    	var that = this;
    	that.initUploadPanel();
    	that.$el.bind('change',function(){
    		that.loadImages($(this));
    	});
    	if(that.$el.val()){
    		that.$el.trigger('change');
    	}
    	return this;
    },
    
    loadImages : function() {
    	var values = this.values
		;
		for (i in values) {
			this.loadImg(values[i]);
		}
    },
    
    loadImg : function(id) {
    	if (! id)
    		return;
    	
    	var that = this
			;
    	
    	$.ajax({
			  type: 'POST',
			  url: that.urlLoad,
			  data: {id:id},
			  dataType: 'json',
			  async:false,
			  beforeSend:function(){
				  var li=$('<li>').attr('id',id).html('<span class="qq-upload-spinner"></span>');
				  that.uploader_panel.find('ul').append(li);
				  if(that.multiple == false){
					  that.uploader_panel.find('.qq-upload-button').hide();
				  }
				  return true;
			  },
			  success: function(data) {
					if(data['success']==true) {
						$('<img>').attr('src',data.thumb_url).bind('load readystatechange',function(){
							$('#qu'+data['id'])
								.html('')
								.css('background-image','url('+data.thumb_url+')')
								.addClass('qq-upload-img ui-state-default ui-corner-all qq-upload-success')
								.on('mouseenter',function(){
									that.showPanel($(this));
								}).on('mouseleave',function(){
									that.hidePanel();
								});
						}).bind('error',function(){
							alert('Соединение с сервером было разорвано,обновите страницу');
						});
					} else {
						$('#qu'+data['id']).html('').addClass('qq-upload-err').addClass('qq-upload-success').addClass('qq-upload-error');
						if(settings['multiple']==false){
							that.uploader_panel.find('.qq-upload-button').show();
						}	
					}
			  },
			  error:function() {
				  if(that.multiple == false){
					  that.uploader_panel.find('.qq-upload-button').show();
				  }
				  alert('Соединение с сервером было разорвано,обновите страницу');
			  }
			  
		});
    },
    
    initUploadPanel : function() {
    	var that = this,
    		id = that.$el.attr('id'),
    		uploader_panel_id = 'uploader_'+id
    		;
		that.$el.after('<div id="'+uploader_panel_id+'" class="zenimages clearfix"></div>');
		that.uploader_panel	= $('#'+uploader_panel_id);
		
		var $uploader_panel = that.uploader_panel,
			$upload_button = $uploader_panel.find('.qq-upload-button'),
			uploader=new qq.FileUploader({
			    multiple : that.multiple,
				action : that.urlUpload,
				element : document.getElementById(uploader_panel_id),
				allowedExtensions: that.allowedExtensions,
				template: '<div class="qq-uploader">' + 
					'<ul class="qq-upload-list"></ul>' +
				    '<div class="qq-upload-button ui-state-default ui-corner-all button"><div class="qq-upload-drop-area ui-state-default ui-corner-all"><span></span></div>Загрузить файлы</div>' +
				 	'</div>',

			    fileTemplate: '<li class="ui-state-default ui-corner-all">'+
				    '<span class="qq-upload-file"></span>'+
				    '<span class="qq-upload-spinner"></span>'+
				    '<span class="qq-upload-size"></span>'+
				    '<a class="qq-upload-cancel" href="#">Отмена</a>'+
				    '<span class="qq-upload-failed-text ui-state-error ui-corner-all">Ошибка</span>'+
					'</li>',    
					
				onSubmit: function(id, fileName){
					$uploader_panel.find('.qq-upload-fail').remove();
					if(! that.multiple){
						$upload_button.hide();
					}
				},
				
				onCancel: function(id, fileName){
					$.post(that.urlCancel, {file:fileName});
					if(that.multiple == false){
						$upload_button.show();
					}
					return true;
				},
				
				onComplete: function(id, fileName, responseJSON){
					if (responseJSON) {
						var n=id+1;
						el=$(uploader._getItemByFileId(id));
						
						if (typeof(responseJSON.errors )!= "undefined") {
							var str="";
							var errors = responseJSON.errors;
							for (i in errors) {
								str += '<span>' + errors[i] + '</span>';
							}
							$uploader_panel.find('.qq-upload-failed-text').html(str);
						}
						
						if (typeof(responseJSON.thumb_url) != "undefined") {
							el.css('background-image','url('+responseJSON.thumb_url+')')
								.addClass('qq-upload-img')
								.on('mouseenter',function(){
									that.showPanel($(this));
								}).on('mouseleave',function(){
									that.hidePanel();
								});
							el.find('span').hide();
							el.attr('id','qu'+responseJSON.id);
							if(that.multiple == false){
								$upload_button.hide();
							}
							that.$el.val(that.pickUpValues());
						}
					} 
				},
				
				messages: {
			        typeError: "Файл {file} имеет неверное расширение. Только файлы с разрешениями {extensions} разрешены.",
			        sizeError: "Файл {file} слишком большой, максимальный размер {sizeLimit}.",
			        minSizeError: "Файл {file} слишком маленький , минимальный размер {minSizeLimit}.",
			        emptyError: "Файл {file} пустой, выберите файлы снова,но без него.",
			        onLeave: "Файлы загружаются на сервер, если вы уйдете прогресс не будет сохранён."            
			    }
			});

	   //make uploader list sortable
		$uploader_panel.find('.qq-upload-list').sortable({
			placeholder : 'ui-state-highlight',
			opacity : 0.6,
			tolerance : 'pointer',

			start : function(event, ui) {
				that.hidePanel();
			},
			
			stop : function() {

			},
			
			update : function(event, ui) {
				that.$el.val(that.pickUpValues());
			}
		});

		$uploader_panel.find('.qq_imageeditor').on('click',function(){
			that.hidePanel();
			//showimageeditor($(this));
			return false;
		});
    },
    
    pickUpValues : function(){
	   var  $uploader_panel = this.uploader_panel,
	   		serialized = []
	   		;
	   $uploader_panel.find('li').each(function(){
			id=$(this).attr('id');
			if(typeof(id) != 'undefined' && id){
				serialized.push(id);
			}
	   });
	   return (serialized.join(','));
	},
    
    explode : function(delimiter, string) 
    {
		var emptyArray = {
			0 : ''
		};
		if (arguments.length != 2 || typeof arguments[0] == 'undefined'
				|| typeof arguments[1] == 'undefined') {
			return null;
		}
		if (delimiter === '' || delimiter === false || delimiter === null) {
			return false;
		}
		if (typeof delimiter == 'function' || typeof delimiter == 'object'
				|| typeof string == 'function' || typeof string == 'object') {
			return emptyArray;
		}
		if (delimiter === true) {
			delimiter = '1';
		}
		return string.toString().split(delimiter.toString());
	},
	
	showPanel : function(el) {
		var that = this;
		if (!el.find('.cpan').length) {
			image_panel = "<div class='cpan'><a href='#' class='ui-corner-all ui-state-default button qq_image_delete' title='Удалить изображение'><span class='ui-icon ui-icon-trash'></span></a><a href='#' class='ui-corner-all ui-state-default button qq_imageeditor' title='Редактировать параметры изображения'><span class='ui-icon ui-icon-image'></span></a></div>";
			el.append(image_panel);
			el.find('.qq_image_delete').on('click',function(){
				that.hidePanel();
				$(this).closest('li').animate({width:0,height:0,opacity:0},
					600,
					function(){
						$(this).remove();
						that.$el.val(that.pickUpValues());
						if(that.multiple == false){
							$upload_button.show();
						}
					}
				);
				return false;
			});
		}else{
			el.find('.cpan').show();
		}
	},
	
	hidePanel : function(){
		$('.cpan').hide();
	} 
	
});

App.Views.ImageEditor = Backbone.View.extend({

	initialize:function (options) {
        return this;
    },
    
    render : function() {
    	
    },
    
    save : function() {
    	
    }
	
});