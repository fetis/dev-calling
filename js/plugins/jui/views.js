App.Views.Autocomplete = Backbone.View.extend({
	
	source : function(){},
	
	minLength: 2,
	
	attribs : {},
	
	initialize: function(options) {
		this.render();
		return this;
	},

	render: function() {
		var that = this;
		this.$el.autocomplete({
	      source: this.source(),
	      minLength: this.minLength,
	      select: function( event, ui ) {
	    	  that.select( event, ui );
	      }
	    });
	},
	
	select : function(event, ui) {
		
	}
	
	
});