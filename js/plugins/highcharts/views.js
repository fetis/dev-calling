App.Views.Highcharts = Backbone.View.extend({
	
	attribs : {
		'title' : 'title'
	},
	
	initialize: function(options) {
		this.attribs = (options.attribs == undefined) ? {} : options.attribs;
		return this;
	},

	render: function() {
		this.$el.highcharts(this.attribs);
	}
	
});