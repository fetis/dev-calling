App.Models.Products = Backbone.Model.extend({
	
	url_params : {},
	
	base_url : 'product/',
	
	url : function() {
		return (this.get('id') !== undefined) ? this.base_url + this.get('id')  + '/': this.base_url;
	},
	
	validation: {
		label : {required: true},
		selfprice : {required: true, pattern: 'number'},
		price : {required: true, pattern: 'number'}
	},
	
	remove : function (success, error) {
		this.destroy({success:success, error:error});
	}
	
});

App.Collections.ProductsCollection = Backbone.Collection.extend({
    
    model : App.Models.Products,
    
	url_params : {},
	
    url : function() {
		return App.assembleUrl("products/",this.url_params);
	},
	
	initialize : function () {
    },
		
	refresh : function() {
		this.fetch();
	}
	
});
