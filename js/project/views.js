App.Views.ModalFormView = Backbone.View.extend({

	el : '#myModal',
	
	btn_submit : "Ок",
	
	btn_submit_class : 'btn-primary',
	
	btn_cancel : "Отмена",
	
	container : '.js-popup-windows-cont',
	
	template:_.template($('#tpl-modal-form').html()),
	
	events : {'submit form' : 'submitForm',
			  'click .js-btn-cancel' : 'cancel',
			  'click .js-btn-submit' : 'submit'},		  
			  
	initialize:function (options) {
		var context = options.context,
			btn_submit = (context.btn_submit == undefined) ? this.btn_submit : context.btn_submit,
			btn_cancel = (context.btn_cancel == undefined) ? this.btn_cancel : context.btn_cancel,
			btn_class = this.btn_submit_class
			;
		$(this.el).html(this.template({
			title : context.title, 
			submit : btn_submit, 
			cancel : btn_cancel, 
			btn_class : btn_class
		}));
		this.context = context;
		this.bindContent();
		this.$el.modal('show');
	},
	
	bindContent : function() {
		var that = this,
			model = this.context.model
			success = function () {
						$(that.el)
						.find(that.container)
						.html(that.context.template({model:that.context.model.toJSON()}));
					  }
			;
		
		if (model.get('id') == undefined)
		{
			success();
		}
		else
		{
			model.fetch({
				'success' : success, 
				'error' : function () {} 
			});
		}
	},
			  
	submitForm : function(e) {
		this.context.submit(e);
	},
	
	submit : function () {
		var $form = this.$el.find('form');
		if ($form.length)
		{
			$form.trigger('submit');
		}
		else
		{
			this.success();
		}
	},
	
	cancel : function() {
		this.$el.modal('hide');
	},
	
	success : function () {}

});

App.Views.ModalAbstractDialogView = App.Views.ModalFormView.extend({
	
	events : {'click .js-btn-cancel' : 'cancel',
			  'click .js-btn-submit' : 'submit'},
	
	bindContent : function(context) {
		$(this.el).find(this.container).html(this.context.html);
	},
	
	success : function (e) {
		this.context.success(e);
		this.$el.modal('hide');
	}
	
});

App.Views.ModalDeleteView = App.Views.ModalAbstractDialogView.extend({
	
	btn_submit : "Удалить",
	
	btn_submit_class : 'btn-danger'
	
});


App.Views.ModalDialogView = Backbone.View.extend({
	
	el : '#myModal',

	title : "Заголовок",
	
	template_form:_.template($('#tpl-modal-form').html()),
	
	container : '.js-popup-windows-cont',
	
	btn_submit_class : 'btn-primary',
	
	btn_cancel : "Отмена",
	
	btn_submit : "Ок",
	
	views : [],
	
	events : {
			'submit form' : 'submitForm',
		  	'click .js-btn-cancel' : 'cancel',
		  	'click .js-btn-submit' : 'submit'
		  	},
		  	
	initialize:function (options) {
		var that = this;
		that.init(options);
		$(that.el).on('hidden.bs.modal', function (e) {
			 that.undelegateEvents();
			 _.each(that.views,function(el){
				 el.undelegateEvents();
				 el.remove();
			 });
		});
	},
	
	init : function(options) {
	},
		  	
	submit : function(e) {
	},
		  	
  	cancel : function(e) {
  		this.hide();
	},
	
	hide : function() {
		this.$el.modal('hide');
	},
	
	render : function() {
		this.renderWindow();
		this.renderForm();
		this.show();
	},
	
	renderWindow : function() {
		$(this.el).html(this.template_form({
			title : this.title, 
			submit : this.btn_submit, 
			cancel : this.btn_cancel, 
			btn_class : this.btn_submit_class
		}));
	},
	
	renderForm : function() {
		if (this.template != undefined && this.model != undefined) {
			$(this.el)
				.find(this.container)
				.html(this.template({model:this.model.toJSON()}));
		}
	},
	
	show : function() {
		this.$el.modal('show');
	}
	
});

App.Views.EditView = App.Views.ModalDialogView.extend({
	
	submit : function () {
		var $form = this.$el.find('form').eq(0);
		if ($form.length)
		{
			$form.trigger('submit');
		}
		else
		{
			this.success();
		}
	},
	
	success : function () {},
	
	submitForm : function(e) {}
	
});
