$(function () {

	var order_coll = new App.Collections.OrderCollection()
		;
	order_coll.fetch({
		success: function (collection, response, options) {
			new App.Views.OrderListView({collection:order_coll});
			new App.Views.OrderSearchView({collection:order_coll});
			new App.Views.OrderFilterStateView({collection:order_coll});
			new App.Views.OrderFilterRecallView({collection:order_coll});
			new App.Views.OrderFilterDateView({collection:order_coll});
			new App.Views.OrderRefreshColl();
			//new App.Views.TotalDailyGraphView().render();
		},
		error: function (collection, response, options) {
		}
	});
	
});
