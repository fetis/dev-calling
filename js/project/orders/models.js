App.Models.Order = Backbone.Model.extend({
	
	base_url : 'order/',
	
	url_params : {},
	
	url : function() {
		return (this.get('id') !== undefined) ? this.base_url + this.get('id')  + '/': this.base_url;
	},

	getSendRequest : function()
	{
		if (this.request == undefined)
		{
			this.request = new App.Models.OrderSendRequest({parent : this});
		}
		return this.request;
	},
	
	getInfoRequest : function()
	{
		if (this.info == undefined)
		{
			this.info = new App.Models.OrderInfoRequest({parent : this});
		}
		return this.info;
	},
	
	getInvoiceRequest : function()
	{
		if (this.invoice == undefined)
		{
			this.invoice = new App.Models.OrderInvoiceRequest({parent : this});
		}
		return this.invoice;
	},
	
	getReview : function()
	{
		if (this.review == undefined)
		{
			this.review = new App.Models.OrderReview({id : this.get('id')});
		}
		return this.review;
	},
	
	moveStateRequest : function(state_id)
	{
		if (this.moveState == undefined)
		{
			this.moveState = new App.Models.MoveStateRequest({parent : this});
		}
		this.moveState.set({state_id : state_id});
		return this.moveState;
	},
	
	remove : function (success, error) {
		this.destroy({success:success, error:error});
	}
	
});




App.Models.OrderFull = Backbone.Model.extend({
	
	base_url : 'order/',
	
	url_params : {},
	
	url : function() {
		var url = (this.get('id') !== undefined) ? this.base_url + this.get('id')  + '/': this.base_url;
			url = (this.get('action') !== undefined) ? url + '?action=' + this.get('action') : url;
		return url;
	},
	
	validation: {
		
	}
	
});

App.Collections.OrderCollection = Backbone.Collection.extend({
    
	base_url : 'orders/',
	
	model : App.Models.Order,
    
	url_params : {'filter[date_type]' : 'monthly', 'filter[state_id]' : 0},
	
    url : function() {
    	if ( this.url_params['key'] !== undefined ) {
    		this.url_params = {key : this.url_params['key']};
    	}
		return App.assembleUrl(this.base_url, this.url_params);
	},
	
	initialize : function (options) {
		EventManager.on('filterOrderList', this.filter, this);
	},
	
	search : function (key) {	
		if (key.length >= 2)	{
			this.url_params['key'] = key;
		} else if ( this.url_params['key'] !== undefined ) {
			delete this.url_params['key'];
		}
	},
	
	sort : function (sort) {
		if (sort) {
			this.url_params['sort'] = sort;
		} else	{
			delete this.url_params['sort'];
		}
	},
	
	filter : function (obj) {
		if (_.size(obj) == 0) {
			return;
		}
		var filter = "";
		for (var param in obj) {
			this.url_params['filter['+param+']'] = obj[param];
		}
	},
	
	refresh : function() {
		this.fetch({reset : true});
	},
	
	updateStates : function(callback) {
		var that = this,
			url = App.assembleUrl('update-stat/', []);
			;
		$.ajax({
			url : url,
			dataType: "json",
			success: function(res){
				callback(res);
	        }
		});
	} 
	
});

App.Collections.ClientHistoryCollection = Backbone.Collection.extend({
	
	base_url : 'history/',
	
	order_id : null,
	
	url : function() {
		return this.base_url + this.order_id + '/';
	},
	
});
