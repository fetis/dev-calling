App.Views.OrderRowView = Backbone.View.extend({

	waited : false,
	
	tagName : 'tr',

	template:_.template($('#tpl-order-row').html()),
	
	events : {
			  'click' : 'click',
			  'click .js-btn-view' : 'detail',
			  'click .js-btn-edit' : 'edit',
			  'click .js-btn-remove' : 'delete',
			 },
    initialize:function (options) {
        $(this.el).html(this.template({model : this.model.toJSON(), sound : options.sound})).addClass("tr-color-" + this.model.get('color'));
        if (this.model.get('owner_id') == null)
        {
        	$(this.el).addClass('tr-color-3');
        }
        return this;
    },
    
	delete : function(){
		new App.Views.OrderDeleteView({model:this.model}).render();
	},
	
	detail : function(){
		if (this.wait()) {
			return false;
		} 
		new App.Views.OrderDetailView({model : this.model}).render();
	},
	
	edit : function(){
		if (this.wait()) {
			return false;
		} 
		var that = this;
		
		$.get('order-bind/'+this.model.get('id')+'/', function(data){
			if (data.res == true)
			{
				new App.Views.OrderEditView({model:that.model}).render();
			}
			else
			{
				alert('Данный заказ обрабатывается другим менеджером');
			}
		},'json');
	},
	
	wait : function(state) {
		if (state == undefined) {
			return this.waited;
		} else {
			var $shadow = $('#shadow');
			if (state) {
				$shadow.addClass('in').show(0);
				this.$el.addClass('waited');
			} else {
				$shadow.removeClass('in').hide(0);
				this.$el.removeClass('waited');
			}
			this.waited = state;
		}
	},
	
	click : function() {
		var $table = this.$el.closest('table')
			;
		$table.find('.tr-color-1').removeClass('tr-color-1');
		this.$el.addClass('tr-color-1');
		App.Views.OrderListView.active_row = this.model.get('id');
		App.Views.OrderListView.active_row_near = this.$el.prev('tr').find('td:first span').text();
	}
});



App.Views.OrderRefreshColl = Backbone.View.extend({
	
	el : '#js-collection-refresh',
	
	events : {'click' : 'click'},
	
	click : function(e) {
		e.preventDefault();
		EventManager.trigger('resetOrderList');
	},
	
});

App.Views.OrderDetailView = App.Views.ModalDialogView.extend({
	
	title : 'Просмотр информации по заказу',
	
	template:_.template($('#tpl-order-detail').html()),
	
	submit : function (e) {
		this.hide();
	}
});

App.Views.OrderEditView = App.Views.EditView.extend({
	
	title : 'Редактирование товара',
	
	template:_.template($('#tpl-order-form').html()),
	
	render : function() {
		var that = this;
		this.renderWindow();
		this.renderForm();
		this.$el.find('#delivery_date').datepicker();
		this.$el.find('#date_pickup').datepicker();
		this.$el.find('#date_recall').datetimepicker({
			format:'Y-m-d H:i:s',
			lang:'ru'
		});
		
		this.$el.find('#delivery_date').mask("99.99.9999");
		this.$el.find('#date_pickup').mask("99.99.9999");
		
		$(this.el).find('.js-sms-preset').each(function(){
			new App.Views.SmsPresetView({el : $(this), model : that.model});
		});
		
		new App.Views.SmsPresetView();
		
		var	project_id = (this.model.get('project_id') == undefined) ? this.model.get('project_id') : null
			;
		
		this.show();
		
		$('.js-sms-send').on('click', function(){
			var $from = $(this).closest('form'),
				text = $from.find('#sms_text').val(),
				phone = $from.find('#phone').val()
				;
			$.post(
				'sms/',	{text:text, phone:phone}, 
				function(data){
					alert(data.message);
				},'json'
			);
		});
		
		$('.js-pay-send').on('click', function(){
			$.post(
				'paylink/'+that.model.get('id')+'/',{}, 
				function(data){
					alert(data.message);
				},'json'
			);		
		});	
			
		new App.Views.OrderClientHistoryView({el : this.$el.find('.b-history').eq(0), model : this.model});
	},
	
	submitForm : function(e) {
		e.preventDefault();
		var data = App.getFormData($(e.currentTarget)),
			that = this
			;
		this.model.set(data);
		this.model.save(null, {
			success: 	function(model, xhr, options){
				EventManager.trigger('resetOrderList');
			},
			error: 		function(model, response, options){ 
				alert('Ошибка при сохранении заказа '+response.responseText); 
			}
		});
		this.$el.modal('hide');
	}
	
});

App.Views.OrderListView = Backbone.View.extend({
	
	active_row : null,
	
	active_row_near : null,

	container : $('.js-order-list'),
	
	initialize:function (options) {
        EventManager.on('resetOrderList', this.refresh, this);
        this.collection.on('reset', this.render, this);
        $('a[href="#orders"]').append('&nbsp;<span class="badge badge-4"><span id="js-order-count"></span></span>');
        this.setRefreshInterval();
		this.render();
    },
	render:function () {
		var that = this,
			counter = 0;
			;
		that.container.html('');
		
		this.collection.each(function (order, i) {
			var sound = that.setSound(order),
            	html = new App.Views.OrderRowView({model:order, sound : sound}).el,
            	id = order.get('id')
            	;
			that.container.append(html);
			counter++;
        });
		$('#js-order-count').text(counter);
		that.fireActiveRow();
		that.container.find('.dropdown-toggle').dropdown();
	},
	
    refresh : function() {
    	var that = this;
    	that.wait(true);
    	this.collection.fetch({
    		success: function (collection, response, options) {
    			that.render();
    			that.wait(false);
    		},
    		error: function (collection, response, options) {
    			alert('Ошибка при обновлении списка заказов');
    			that.wait(false);
    		}
    	});
    },
    
    setRefreshInterval : function() {
    	window.setInterval(function () {
    		EventManager.trigger('resetOrderList');
    	}, 1000 * 60 * 2);
    },
    
    setSound : function(model) {
    	if (! model.get('date_recall'))
    		return false;
    	
    	var that = this,
    		now = new Date(),
    		to = new Date(now.getTime()+60 * 1000 * 2),
    		date_recall = model.get('date_recall')
    		;
    	
    	now = dateToMysqlDateTime(now);
    	to = dateToMysqlDateTime(to);
    	
    	if (date_recall <= now)
    		return false;
    		
		if (date_recall >= now && date_recall < to)
		{
			var audio = new Audio();
			audio.preload = 'auto';
	    	audio.src = '/audio/sncf.mp3';
			audio.play();
			return true;
		}
		return false;
    },
    
    wait : function(state) {
		if (state == undefined) {
			return this.waited;
		} else {
			var $shadow = $('#shadow');
			if (state) {
				$shadow.addClass('in').show(0);
				this.$el.addClass('waited');
			} else {
				$shadow.removeClass('in').hide(0);
				this.$el.removeClass('waited');
			}
			this.waited = state;
		}
	},
	
	fireActiveRow : function() {
		var that = this,
			rows = [App.Views.OrderListView.active_row, App.Views.OrderListView.active_row_near]
			;
		$.each(rows, function(i, el){
			if (that.container.find("tr span:contains('"+el+"')").length)
			{
				that.container.find("tr span:contains('"+el+"')").closest('tr').trigger('click');
				return;
			}
		});
	}
    
});

App.Views.OrderDeleteView = App.Views.ModalDialogView.extend({
	
	title : 'Удаление заказа',
	
	template:_.template($('#tpl-order-remove').html()),
	
	submit : function(e) {
		var that = this;
		e.preventDefault();
		this.model.remove(function(){	
			EventManager.trigger('resetOrderList');
			that.hide();
		},function(){
			alert('Ошибка при удалении заказа');
		});
	}
	
});

App.Views.OrderClientHistoryView = Backbone.View.extend({
	
	events : {
		'click .js-show-history' : 'click',
	},
	
	initialize:function (options) {
		this.collection = new App.Collections.ClientHistoryCollection();
		this.collection.order_id = this.model.get('id');
	},
	
	template:_.template($('#tpl-order-history').html()),
	
	click : function()
	{
		var that=this;
		that.collection.fetch({
			success: function (collection, response, options) {
				that.render();
			}
		});
	},
	
	render : function()
	{
		var html = this.template({collection:this.collection});
		$(this.el).html(html);
	}
});

App.Views.SmsPresetView = Backbone.View.extend({
	
	sms_input : '#sms_text',
	
	events : {
		'click' : 'click',
	},
	
	click : function(e)
	{
		var $sms_input = $(this.sms_input),
			message_tpl_selector = this.$el.attr('data-tpl'),
			template = _.template($(message_tpl_selector).html()),
			text = template({model : this.model.toJSON()})
			;
		$sms_input.val(text);
		e.preventDefault();
	},
});