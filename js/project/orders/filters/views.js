App.Views.OrderFilterStateView = Backbone.View.extend({
	
	el : '#js-order-filter-state',
	
	events : {'click .badge' : 'click'},
	
	initialize:function (options) {
		this.$el.find('.badge').eq(1).addClass('badge-filter-active');
	},
	
	click : function(e) {
		e.preventDefault();
		var	$el = $(e.currentTarget),
			value = {}
			;
		this.$el.find('.badge').not($el).removeClass('badge-filter-active');
		$el.toggleClass('badge-filter-active');
		value.state_id =  ($el.hasClass('badge-filter-active')) ? $el.attr('data-id') : 'all';
		EventManager.trigger('filterOrderList', value);
		EventManager.trigger('resetOrderList');
	}
	
});

App.Views.OrderFilterRecallView = Backbone.View.extend({
	
	el : '#js-order-filter-recall',
	
	events : {'click' : 'click'},
	
	click : function(e) {
		e.preventDefault();
		var	value = {}
			;
		this.$el.toggleClass('badge-filter-active');
		value.recall = (this.$el.hasClass('badge-filter-active')) ? 1 : 0;
		EventManager.trigger('filterOrderList', value);
		EventManager.trigger('resetOrderList');
	}
	
});

App.Views.OrderFilterDateView = Backbone.View.extend({
	
	el : '#js-order-filter-date',
	
	events : {'click .badge' : 'click',
			  'submit .custom-date-form' : 'submit'},
	
	initialize:function (options) {
		this.dates = this.$el.find('.date-filter');
		this.dates.datepicker();
		this.form = this.$el.find('form') 
		this.form.hide(0);
	},
	
	click : function(e) {
		e.preventDefault();
		var	$el = $(e.currentTarget),
			value = {}
			;
		
		if ($el.attr('data-id') == 'custom') {
			this.form.show(0);
		} else {
			this.form.hide(0);
			this.$el.find('.badge').not($el).removeClass('badge-filter-active');
			$el.toggleClass('badge-filter-active');
			value.date_type =  ($el.hasClass('badge-filter-active')) ? $el.attr('data-id') : 0;
			EventManager.trigger('filterOrderList', value);
			EventManager.trigger('resetOrderList');
		}
	},
	
	submit : function(e) {
		e.preventDefault();
		var $el = $(e.currentTarget),
			$badge = this.$el.find('.badge[data-id="custom"]'),
			value = {'date_type' : 'custom'},
			label = ""
			;
		if (this.dates.eq(0).val() == "" && this.dates.eq(1).val() == "") {
			return false;
		}
		if (this.dates.eq(0).val()) {
			label += this.dates.eq(0).val().substr(5,11);
			value.date_type_from = this.dates.eq(0).val();
		}
		if (this.dates.eq(1).val()) {
			label += "*"+this.dates.eq(1).val().substr(5,11);
			value.date_type_to = this.dates.eq(1).val();
		}
		$badge.text(label);
		this.$el.find('.badge').not($badge).removeClass('badge-filter-active');
		$badge.toggleClass('badge-filter-active');
		EventManager.trigger('filterOrderList', value);
		EventManager.trigger('resetOrderList');
		this.form.hide(0);
	}
	
});

App.Views.OrderSearchView = Backbone.View.extend({
	
	el : '#js-search-order-form',
	
	events : {'keyup input[type="text"]' : 'search'},
	
	search : function(e) {
		e.preventDefault();
		var value = $(this.el).find('input[type="text"]').val();
		this.collection.search(value);
		if (value.length > 2) {
			EventManager.trigger('resetOrderList');
		}
	}
});