function BbGridFilter() 
{
	this.el = null;
	this.grid = null;
	this.callback = null;
};

BbGridFilter.prototype.getListView = function() 
{
	var that = this;
	return Backbone.View.extend({
		
		events : {'click .badge' : 'click'},
		
		click : function(e) {
			e.preventDefault();
			var	$el = $(e.currentTarget),
				value = {}
				;
			this.$el.find('.badge').not($el).removeClass('badge-filter-active');
			$el.toggleClass('badge-filter-active');
			var key = this.$el.attr('data-key');
			value[key] =  ($el.hasClass('badge-filter-active')) ? $el.attr('data-id') : 'all';
			EventManager.trigger(that.grid.getFilterEvent(), value);
			EventManager.trigger(that.grid.getResetEvent());
			if (that.callback)
			{
				that.callback(e);
			}
		}
	});
};

BbGridFilter.prototype.init = function()
{
	var that = this,
		view_class = that.getListView(),
		view = new view_class({el : $(that.el).eq(0)})
	;
};