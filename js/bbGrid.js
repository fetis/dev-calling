function BbGrid() 
{
	this.alias = null;
	this.url = null;
	this.list_model = null;
	this.item_model = null;
	this.add_view = null;
	this.list_view = null;
	this.row_view = null;
	this.form_view = null;
	this.el = null;
	this.title = '';
}

BbGrid.prototype.run = function()
{
	var that = this,
		collection = this.getCollection();
	collection.fetch({
		success: function (collection, response, options) {
			var list_class = that.getListView();
			new list_class().render();
			var add_view = that.getAddView();
			new add_view({el : $(that.el).find('.js-list-add').eq(0)})
		},
		error: function (collection, response, options) {
			alert('Ошибка инициализации коллекции');
		}
	});
};

BbGrid.prototype.getModel = function() 
{
	var that = this;
	return Backbone.Model.extend({

		url : function() 
		{
			var url = that.url;
			return (this.get('id') !== undefined) 
					? url + this.get('id')  + '/'
					: url;
		}
		
	});
};

BbGrid.prototype.getCollection = function() 
{
	var that = this;
	
	if (that.collection != undefined)
		return that.collection;
	
	var collection_class = Backbone.Collection.extend({

		url_params : {},
		
		initialize:function (options) {
			this.model = that.getModel();
			EventManager.on(that.getFilterEvent(), this.filter, this);
		},
		
		filter : function (obj) {
			if (_.size(obj) == 0) {
				return;
			}
			var filter = "";
			for (var param in obj) {
				this.url_params['filter['+param+']'] = obj[param];
			}
		},
		
		url : function() {
			var url = that.url;
	    	if ( this.url_params['key'] !== undefined ) {
	    		this.url_params = {key : this.url_params['key']};
	    	}
			return that.assembleUrl(url, this.url_params);
		},

	});
	
	that.collection = new collection_class();
	return that.collection;
};

BbGrid.prototype.getListView = function() 
{
	var that = this;
	return Backbone.View.extend({

		container : '.js-item-list',
		
	    initialize:function (options) {
	    	this.delegateEvents();
	        EventManager.on(that.getResetEvent(), this.refresh, this);
	        return this;
	    },
		
		render:function () {
			var $container = $(that.el).find(this.container).eq(0),
				view = that.getRowView()
				;
			$container.html('');
			that.collection.each(function (model, i) {
	            var html = new view({model:model}).el;
	            $container.append(html);
	        });
		},
		
		refresh : function() {
	    	var self = this;
	    	that.collection.fetch({
	    		success: function (collection, response, options) {
	    			self.render();
	    		},
	    		error: function (collection, response, options) {
	    			alert('Ошибка при обновлении списка заказов');
	    		}
	    	});
	    }
		
	});
	
};

BbGrid.prototype.getAddView = function() 
{
	var that = this;
	return Backbone.View.extend({
	
		events : {'click' : 'click'},
		
		click : function(e) {
			e.preventDefault();
			var model_class = that.getModel(),
				form_class = that.getFormView()
				;
			new form_class({model : new model_class()}).render();
		},
	
	});
}

BbGrid.prototype.getRowView = function() 
{
	var that = this;
	return Backbone.View.extend({

		tagName : 'tr',
		
		events : {
			  'click .js-btn-edit' : 'edit',
			  'click .js-btn-remove' : 'delete'
			 },
		
		initialize:function (options) {
			var template = that.getTemplate('row');
	        $(this.el).html(template({model : this.model.toJSON()}));
	        return this;
	    },
	    
	    edit : function() {
	    	var self = this;
			this.model.fetch({
				success : function() {
					var form_class = that.getFormView();
					new form_class({model : self.model}).render();
				}, 
				error : function() {
					alert('Ошибка при загрузке модели');
				}
			});
	    },
	    
	    delete : function() {
	    	if (confirm("Вы действительно хотите удалить запись?"))
	    	{
	    		this.model.destroy({
	    			success : function() {
	    				EventManager.trigger(that.getResetEvent());
	    			}, 
	    			error : function() {
	    				alert('Ошибка при удалении записи');
	    			}
	    		});
	    	}
	    }
		
	});
};

BbGrid.prototype.getFormView = function() 
{
	var that = this;
	return form_class = Backbone.View.extend({
		
		el : '#myModal',
		
		container : '.js-popup-windows-cont',
		
		views : [],
		
		events : {
			'click .js-btn-cancel' : 'cancel',
			'click .js-btn-submit' : 'submit'
		},		
		
		template:_.template($('#tpl-modal-form').html()),
		
		initialize:function (options) {
			var self = this
				;
			$(this.el).html(this.template({
				title : that.title, 
				submit : 'Сохранить', 
				cancel : 'Отмена', 
				btn_class : '.btn-primary'
			}));
			
			this.$el.modal('show');
			
			$(this.el).on('hidden.bs.modal', function (e) {
				self.undelegateEvents();
				 _.each(self.views,function(el){
					 el.undelegateEvents();
					 el.remove();
				 });
			});
			
	        return this;
	    },
		
		render : function()
		{
			var template = _.template($('#tpl-form-'+that.alias).html()),
				html = template({model:this.model.toJSON()})
				;
			this.$el
				.find(this.container)
				.html(html);
			this.$el.modal('show');
		},
	    
	    submit : function(e)
	    {
			var data = App.getFormData(this.$el.find('form'));
			this.model.set(data);
			this.model.save(null, {success : function(){
					EventManager.trigger(that.getResetEvent());
				},
				error: function() {
					alert('Ошибка сохранения');
				}
			});
			this.$el.modal('hide');
	    }
		
	});
};

BbGrid.prototype.getTemplate = function(obj) 
{
	return _.template($('#tpl-' + obj + '-' + this.alias).html())
};

BbGrid.prototype.assembleUrl = function(base_url, params) 
{
	var url = base_url;
	if (_.size(params)) 
	{
		url += "?";
		for (var i in params) 
		{
			url +=i+'='+params[i]+'&';
		}
	}
	return url;
};

BbGrid.prototype.getResetEvent = function() 
{
	return 'resetList'+this.alias;
};

BbGrid.prototype.getFilterEvent = function() 
{
	return 'filterList'+this.alias;
};