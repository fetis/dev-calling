<?php
date_default_timezone_set('Europe/Moscow');
defined('ROOT_PATH') 		|| define('ROOT_PATH', realpath(dirname(__FILE__)));
// change the following paths if necessary
$yii=dirname(__FILE__).'/protected/framework/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

require_once($yii);
ini_set("display_errors", 1);
Yii::createWebApplication($config)->run();
