<?php $this->widget('OTabsWidget', array( 'type' => 'vertical', 'tabs' => array(
	'conferention'		=> array('class' => 'OptionTabSettingsConferentionWidget'),
	'speaker'			=> array('class' => 'OptionTabSettingsSpeakerWidget'),
    'items' 			=> array('class' => 'OptionTabSettingsProductWidget', 'active' => true),
    'sms-settings'		=> array('class' => 'OptionTabSettingsSmsWidget'),
)))?>