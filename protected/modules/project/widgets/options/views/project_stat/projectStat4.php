<script type="text/template" id="tpl-project-stat">

<% if (model.commercial.length != 0) { %>

	<h2>Общие показатели</h2>

	<h3>Общие экономические показатели</h3>
	<table class="table table-bordered">

		<tr>
			<td>Общая выручка:</td>
			<td width="150"><%=model.commercial.gain%></td>
		</tr>
		
		<tr>
			<td>Заказов оплачено:</td><td><%=model.commercial.order_count%></td>
		</tr>
	
	</table>

<% } %>

<% if (model.speakers.length != 0) { %>
	<% if (model.speakers.by_speakers.length != 0) { %>
		<h3>Распределение выручки по спикерам</h3>
		<table class="table table-bordered">
			<tr>
				<th>Спикер</th>
				<th width='150' class="text-right">Выручка</th>
				<th width='150' class="text-right">Количество</th>
			</tr>
			<% _.each(model.speakers.by_speakers, function(cell,i) { %>
			<tr>
				<td><%=cell.name%></td>
				<td class="text-right"><%=cell.price%></td>
				<td class="text-right"><%=cell.count%></td>
			</tr>	
			<% }); %>
		</table>
	<% } %>

	<% if (model.speakers.by_items.length != 0) { %>
		<h3>Распределение выручки по продуктам</h3>
		<table class="table table-bordered">
			<tr>
				<th>Спикер</th>
				<th>Продукт</th>
				<th class="text-right" width='150'>Выручка</th>
				<th class="text-right" width='150'>Количество</th>
			</tr>
			<% _.each(model.speakers.by_items, function(cell,i) { %>
			<tr>
				<td><%=cell.name%></td>
				<td><%=cell.label%></td>
				<td class="text-right"><%=cell.price%></td>
				<td class="text-right"><%=cell.count%></td>
			</tr>	
			<% }); %>
		</table>
	<% } %>
<% } %>

<% if (model.managers.length != 0) { %>
	
	<?php $model = new Stat4Manager()?>
	<?php $list = $model->getTeam()?>
	<% var managers=<?php echo CJSON::encode($list)?> %>
	<h2>Менеджеры</h2>
	<table class="table table-bordered">
		
		<% if (typeof managers == 'object') { %>
		<tr>
			<th></th>
			<% _.each(managers, function(cell,i) { %>
				<td width="150"><span class="badge badge-1"><%=cell%></span></td>
			<% }); %>
		</tr>
		<% } %>

		<% if (model.managers.k1 != undefined) { %>
		
			<% { %>
			
			<tr>
				<td>Конверсия</td>
				<% if (typeof model.managers.k1 == 'object') { %>
					<% _.each(model.managers.k1, function(cell,i) { %>
						<td><%=Math.floor(cell)%>%</td>
					<% }); %>
				<% } else { %>
					<td><%=model.managers.k1.toFixed(2)%></td>
				<% }; %>
			</tr>

			<% }; %>
		
		<% }; %>

		<% if (model.managers.k2 != undefined) { %>
			
			<% { %>
			<tr>
				<td>КПД</td>
				<% if (typeof model.managers.k2 == 'object') { %>
					<% _.each(model.managers.k2, function(cell,i) { %>
						<td><%=cell.toFixed(2)%></td>
					<% }); %>
				<% } else { %>
					<td><%=model.managers.k2.toFixed(2)%></td>
				<% }; %>
			</tr>
			<% }; %>
		
		<% }; %>
		
		<% if (model.managers.avg_price_order != undefined) { %>
		
			<% { %>
			<tr>
				<td>Средний чек</td>
				<% if (typeof model.managers.avg_price_order == 'object') { %>
					<% _.each(model.managers.avg_price_order, function(cell,i) { %>
						<td><%=cell.toFixed(2)%></td>
					<% }); %>
				<% } else { %>
					<td><%=model.managers.avg_price_order.toFixed(2)%></td>
				<% }; %>
			</tr>
			<% }; %>
		
		<% }; %>
		
		<% if (model.managers.refuse != undefined) { %>
		
			<% { %>
			<tr>
				<td>Отказы</td>
				<% if (typeof model.managers.refuse == 'object') { %>
					<% _.each(model.managers.refuse, function(cell,i) { %>
						<td><%=Math.floor(cell)%>%</td>
					<% }); %>
				<% } else { %>
					<td><%=model.managers.refuse.toFixed(2)%></td>
				<% }; %>
			</tr>
			<% }; %>
		
		<% }; %>
		
		<% if (model.managers.gain != undefined) { %>
		
			<% { %>
			<tr>
				<td>Выручка</td>
				<% if (typeof model.managers.gain == 'object') { %>
					<% _.each(model.managers.gain, function(cell,i) { %>
						<td><%=cell%></td>
					<% }); %>
				<% } else { %>
					<td><%=model.managers.gain%></td>
				<% }; %>
			</tr>
			<% }; %>
		
		<% }; %>

		
		<% if (model.managers.salary != undefined) { %>
		
			<% { %>
			<tr>
				<td>ЗП</td>
				<% if (typeof model.managers.salary == 'object') { %>
					<% _.each(model.managers.salary, function(cell,i) { %>
						<td><%=cell%></td>
					<% }); %>
				<% } else { %>
					<td><%=model.managers.salary%></td>
				<% }; %>
			</tr>
			<% }; %>
		
		<% }; %>

		<% if (model.managers.avg_margin_abs != undefined) { %>
		
			<% { %>
			<tr>
				<td>Средняя эффективность</td>
				<% if (typeof model.managers.avg_margin_abs == 'object') { %>
					<% _.each(model.managers.avg_margin_abs, function(cell,i) { %>
						<td><%=cell.toFixed(2)%></td>
					<% }); %>
				<% } else { %>
					<td><%=model.managers.avg_margin_abs.toFixed(2)%></td>
				<% }; %>
			</tr>
			<% }; %>
		
		<% }; %>

		<% if (model.managers.avg_margin_abs != undefined) { %>
		
			<% { %>
			<tr>
				<td>Средняя эффективность(%)</td>
				<% if (typeof model.managers.avg_margin_rel == 'object') { %>
					<% _.each(model.managers.avg_margin_rel, function(cell,i) { %>
						<td><%=cell.toFixed(2)%>%</td>
					<% }); %>
				<% } else { %>
					<td><%=model.managers.avg_margin_rel.toFixed(2)%></td>
				<% }; %>
			</tr>
			<% }; %>
		
		<% }; %>
		
	</table>

<% } %>

</script>