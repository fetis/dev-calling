<script type="text/template" id="tpl-order-history">
	<table class="table table-striped table-bordered">
		<tr>
			<th>#</th>
			<th>Дата</th>
			<th>Статус</th>
			<th>Конференция</th>
			<th>Спикер</th>
			<th>Продукт</th>
		</tr>
	<%collection.each(function(model, i){%>
		<tr>
			<td><span class="badge"><%=model.get('id')%></span></td>
			<td><span class="label label-default"><%=model.get('created')%></span></td>
			<td><span class="badge"><%=model.get('state')%></span></td>
			<td><span class="label label-default"><%=model.get('conferention')%></span></td>
			<td><span class="label label-default"><%=model.get('speaker')%></span></td>
			<td><span class="label label-default"><%=model.get('product')%></span></td>
		</tr>
	<%});%>
	</table>
</script>