<script type="text/template" id="tpl-order-form">
	<% if (model.fio || model.email || model.phone) { %>
	<div class="from-site-data row">
		<div class="col-md-6">
			<table class="table table-striped">
				<tr>
					<td><label>N/Статус:</label></td>
					<td>
						<span class="badge"><%=model.id%></span><span class="label label-default">(<%=getSmallDate(model.created)%>)</span> / 
					</td>
				</tr>

				<tr>
					<td><label>Статус:</label></td>
					<td><span class="badge badge-<%=model.state_id%>"><%=Common_Data_AllStates[model.state_id]%></span></td>
				</tr>

				<tr>
					<td><label>ФИО:</label></td>
					<td><%=model.site_fio%><span class="label label-default">(<%=getSmallDate(model.client_date)%>)</span></td>
				</tr>

				<tr>
					<td><label>Телефон/E-mail:</label></td>
					<td><%=model.site_phone%> / <%=model.site_email%></td>
				</tr>

			</table>
		</div>

		<div class="col-md-6">

			<table class="table table-striped">
				<tr>
					<td><label>Конференция:</label></td><td><%=Common_Data_Confs[model.conferention_id]%></td>
				</tr>

				<tr>
					<td><label>Спикер:</label></td>
					<td>
						<%=Common_Data_Speakers[model.speaker_id]%>
					</td>
				</tr>

				<tr>
					<td><label>Продукт:</label></td>
					<td>
						<%=getProductInfo(model.item_id) %> / <%=model.price%>руб.
						<% if (model.item.product_url) { %>
							<br/>(<a target="_blank" href="<%=model.item.product_url%>">Ссылка на продукт</a>)
						<% } %>
					</td>
				</tr>

				<tr>
					<tr>
						<td>Ссылка для покупки:</td><td><a target="_blank" href="<%='<?php echo OrderPayLink::getPayPage() ?>'+model.id%>">Ссылка</a></td>
					</tr>
				</tr>

			</table>

		</div>

	</div>
	<% } %>
	
	<form id="js-order-form" class="form-horizontal order-form" role="form">
	
		<div class="form-group">
			<div class="col-sm-3">
			  <input type="text" class="form-control" id="fio" name="fio" placeholder="Фамилия Имя Отчество" value="<%=model.fio%>">
			</div>
			
			<div class="col-sm-2">
			  <input type="text" class="form-control" id="phone" name="phone" placeholder="79XXXXXXXX" value="<%=model.phone%>">
			</div>

			<div class="col-sm-2">
			  <input type="text" class="form-control" id="email" name="email" placeholder="E-mail" value="<%=model.email%>">
			</div>

			<div class="col-sm-2">
			  <select type="text" class="form-control" id="state_id" name="state_id" placeholder="Статус">
				<%_.each(getAllNearSataes(model.state_id),function(el){ %>
					<option value="<%=el%>" <% if (model.state_id == el) { %>selected="selected"<%}%> ><%=Common_Data_AllStates[el]%></option>	
				<% });%>
			  </select>
			</div>

			<div class="col-sm-3">
			  <input type="text" class="form-control" id="date_recall" name="date_recall" placeholder="Перезвонить" value="<%=model.date_recall%>">
			</div>

		</div>

		<div class="form-group">
			<div class="col-sm-10">
			  <textarea class="form-control" id="comment" name="comment" placeholder="Комментарий"><%=model.comment%></textarea>
			</div>
			<div class="col-sm-2">
				 <span>Пометить цветом</span>
				 <select type="text" class="form-control" id="color" name="color" placeholder="Цвет">
					<option value="0" <% if (model.color == '0') { %>selected="selected"<%}%> >-</option>
					<option value="1" <% if (model.color == '1') { %>selected="selected"<%}%> >Зеленый</option>
					<option value="2" <% if (model.color == '2') { %>selected="selected"<%}%> >Синий</option>
					<option value="3" <% if (model.color == '3') { %>selected="selected"<%}%> >Желтый</option>
					<option value="4" <% if (model.color == '4') { %>selected="selected"<%}%> >Красный</option>
				  </select>
			</div>	
		</div>

		<div class="form-group">
			<div class="col-sm-12">
				<button class="btn btn-sms-preset js-sms-preset sms-preset-sb" data-tpl="#tpl-sms-sb" type="button">
					Сбербанк
				</button>
				<button class="btn btn-sms-preset js-sms-preset sms-preset-ym" data-tpl="#tpl-sms-yd" type="button">	
					ЯндексДеньги
				</button>
				<button class="btn js-pay-send" type="button">	
					Отправить ссылку на страницу оплаты
				</button>
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-12">
				<div class="input-group">
				 <input type="text" class="form-control" id="sms_text" placeholder="SMS-сообщение">
				 <span class="input-group-btn">
				   <button class="btn btn-default js-sms-send" type="button"><span class="glyphicon glyphicon-send"></span></button>
				 </span>
				</div><!-- /input-group -->
			</div>
		</div>

	</form>
	
	<div class="b-history">
		<div class="text-right">
			<span class="js-show-history show-history-link">Показать историю клиента</span>
		</div>
	</div>		

</script>

<script type="text/template" id="tpl-sms-sb">Переведите <%=model.price%>р. на Сберкарту №4276827010653223, получатель Ардашев Николай Витальевич. в комментарии к платежу email по которому сделан заказ.
</script>

<script type="text/template" id="tpl-sms-yd">В Евросети или др.салонах связи положите <%=model.price%>р. на ЯндексДеньги №41001736627230 возьмите чек у кассира, что дальше сообщу по телефону
</script>