	<script type="text/template" id="tpl-producs-select-item">
		<form method='get'>
			<div class="col-sm-6">
				<select name="product_id" class="form-control" value="<%=model.product_id%>">
					<% _.each(product_list, function(product) { %> 
						<option value="<%=product.id%>" <% if(product.id == model.product_id) {%> selected="selected" <%}%>><%=product.label%></option>
					<% }); %>
				</select>
			</div>
			<div class="col-sm-2">
				<input name="count" type="text" class="form-control" placeholder="Количество" value="<%=(model.count == undefined) ? 1 : model.count %>">
			</div>

			<div class="col-sm-2">
				<select name="discount_item" class="form-control" value="<%=model.discount_item%>">
					<% _.each([0,3,5,7,10,15,20], function(discount) { %> 
						<option value="<%=discount%>" <% if(discount == model.discount_item) {%> selected="selected" <%}%>><%=discount%></option>
					<% }); %>
				</select>
			</div>

			<div class="col-sm-2 text-right btn-group-col">
				<span class="item-price"></span>
				<span class="glyphicon glyphicon-plus-sign"></span>
				<span class="glyphicon glyphicon-minus-sign"></span>
			</div>
		</form>
	</script>