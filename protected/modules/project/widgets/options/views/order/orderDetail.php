<script type="text/template" id="tpl-order-detail">

	<div class="row">

		<div class="col-md-6">
			<table class="table table-striped">
				<tr>
					<td>N:</td><td><span class="badge"><%=model.id%></span><span class="label label-default">(<%=getSmallDate(model.created)%>)</span></td>
				</tr>

				<tr>
					<td>Статус:</td><td><span class="badge badge-<%=model.state_id%>"><%=Common_Data_AllStates[model.state_id]%></span></td>
				</tr>

				<tr>
					<td>ФИО:</td><td><%=model.fio%><span class="label label-default">(<%=getSmallDate(model.client_date)%>)</span></td>
				</tr>

				<tr>
					<td>Телефон:</td><td><%=model.phone%></td>
				</tr>

				<tr>
					<td>E-mail:</td><td><%=model.email%></td>
				</tr>
			</table>
		</div>

		<div class="col-md-6">

			<table class="table table-striped">
				<tr>
					<td>Конференция:</td><td><%=Common_Data_Confs[model.conferention_id]%></td>
				</tr>

				<tr>
					<td>Спикер:</td>
					<td>
						<%=Common_Data_Speakers[model.speaker_id]%>
					</td>
				</tr>

				<tr>
					<td>Продукт:</td><td><%=getProductInfo(model.item_id) %>
					<% if (model.item.product_url) { %>
						<br/>(<a target="_blank" href="<%=model.item.product_url%>">Ссылка на продукт</a>)
					<% } %></td>
				</tr>

				<tr>
					<td>Скидка:</td><td><%=model.discount%></td>
				</tr>

				<tr>
					<td>Ссылка для покупки:</td><td><a target="_blank" href="<%='<?php echo OrderPayLink::getPayPage() ?>'+model.id%>">Ссылка</a></td>
				</tr>

			</table>

		</div>

	</div>

	<%if (model.comment) { %>
		<hr/>
		<div class="row">
			<div class="col-md-12"><%=model.comment%></div>
		</div>
	<% } %>
</script>