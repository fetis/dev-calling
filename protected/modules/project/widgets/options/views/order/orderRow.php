<script type="text/template" id="tpl-order-row">
	<td>
		<span class="badge <%if(sound){%>badge-0<%}%>"><%=model.id%></span><br/>
		<span class="label label-default"><%=getSmallDate(model.created)%></span>
	</td>

	<td>
		<span class="label label-default"><%=Common_Data_Confs[model.conferention_id]%></span><br/>
		<span class="label label-default"><%=Common_Data_Speakers[model.speaker_id]%></span>
	</td>

	<td>
		<%=getProductInfo(model.item_id) %>
	</td>

	<td>
		<%=model.fio%><span class="label label-default"><%=getSmallDate(model.client_date)%></span><br/>
		<span class="label label-default"><%=model.phone%></span>
		<span class="label label-default"><%=model.email%></span>
	</td>

	<td>
		<%=Common_Data_Managers[model.owner_id]%><br/>
	</td>

	<td class="comment-cell">
		<% if (model.comment) {%>
			<%=model.comment.substr(0, 100)%>
			<% if (model.comment.length > 100) {%>...<% } %>
		<% } %>
	</td>

	<td>
		<span class="badge badge-<%=model.state_id%>">
			<%=Common_Data_AllStates[model.state_id]%>
			<% if(model.autoupdate_state == 1) { %>
				&nbsp;!
			<% } %>
		</span>
		<% if (model.date_recall) {%>
			<span class="glyphicon glyphicon-earphone"></span>
			<span class="label label-default"><%=getSmallDate(model.date_recall)%></span>
		<% } %>

	</td>

	<td class="text-right">
		<?php $this->widget('OrderPanelWidget')?>
	</td>
</script>