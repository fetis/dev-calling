<h2>Полная статистика по периоду</h2>
				
<div id="js-project-stat">
	
	<form class="form-inline" role="form">
		<div class="form-group">
			<input class="date-filter date-filter-from form-control" placeholder="Начальная дата"/>
		</div>
		<div class="form-group">
			<input class="date-filter date-filter-to form-control" placeholder="Конечная дата"/>
		</div>
		<button class="btn btn-primary">Ок</button>
	</form>
	<br/>
	<div class="order-stat-filter-result">
		<i>Сформируйте условия фильтрации</i>
	</div>
	
</div>			