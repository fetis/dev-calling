$('a[href="#settings"]').data('init',false).click(function() {
	if (! $(this).data('init')) {
		var settings_coll = new App.Collections.ProjectSettingCollection();
		settings_coll.fetch({
			success: function (collection, response, options) {
				new App.Views.ProjectSettingForms({collection:settings_coll});
			},
			error: function (collection, response, options) {
			}
		});
		$(this).data('init',true);
	}
});
