App.Views.ProjectSettingForms = Backbone.View.extend({
	
	el : '#settings',
	
	events : {
		'submit .form-settings' : 'submit',
	},
	
	form_сlass : '.form-settings',
	
	initialize:function (options) {
		this.fillForms();
        return this;
    },
    
    fillForms : function() 
    {
    	var that = this;
    	_.each(this.$el.find(that.form_сlass),function(form, i){
    		var $form = $(form),
    			namespace = $form.attr('data-namespace'),
    			model = that.collection.findWhere({'namespace' : namespace})
    			;
    		
    		if (model != undefined)
    		{
	    		var attribs = model.get('data');
	    		
	    		_.each(attribs, function(el,i){
	    			$form.find('#' + i).val(el);
	    		});
    		}
    		
    	});
    	
    },
    
    submit : function(e) {
    	e.preventDefault();
    	var $form = $(e.currentTarget),
    		data = App.getFormData($form)
    		namespace = $form.attr('data-namespace'),
    		model = this.collection.findWhere({'namespace' : namespace})
    	;
    	if (model == undefined)
    	{
    		model = new App.Models.ProjectSettingModel({'namespace' : namespace});
    	}
    	
    	model.set({data : data});
    	model.save(null, {
			success: 	function(model, xhr, options){ alert('Настройки успешно сохранены'); },
			error: 		function(model, response, options){ 
				alert('Ошибка при сохранении настроек '+response.responseText); 
			}
		});
    }
    
});