App.Models.ProjectSettingModel = Backbone.Model.extend({

	base_url : 'setting/',
	
	url_params : {},
	
	url : function() {
		return (this.get('namespace') !== undefined) ? this.base_url + this.get('namespace')  + '/': this.base_url;
	}
	
});

App.Collections.ProjectSettingCollection = Backbone.Collection.extend({

	base_url : 'settings/',
	
	model : App.Models.ProjectSettingModel,
    
    url : function() {
		return this.base_url;
	}

});

