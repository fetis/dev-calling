$(function () {
	
	function dateToMysqlDate(date)
	{
		return ("0" + date.getDate()).slice(-2) 
			+ '.' 
			+ ("0" + (date.getMonth()+1)).slice(-2) 
			+ '.' 
			+ date.getFullYear().toString() 
		;
	}
	
	$('a[href="#stat"]').on('click', function(){
		var model = new App.Models.OrderStat(),
			to = dateToMysqlDate(new Date()),
			from_obj = new Date(),
			from = ""
		;
		
		from_obj.setMonth(from_obj.getMonth()-1);
		from = dateToMysqlDate(from_obj);
		
		$('.date-filter-from').val(from);
		$('.date-filter-to').val(to);
		
		model.filter({'from' : from, 'to' : to});
		new App.Views.OrderStatView({model: model}).refresh();
		
		$(this).off('click');
	})
	
});