App.Views.OrderStatView = Backbone.View.extend({
	
	el : '#js-project-stat',
	
	template:_.template($('#tpl-project-stat').html()),
	
	events : {'click button' : 'submit'},
	
	initialize:function (options) {
		this.dates = this.$el.find('.date-filter');
		this.dates.datepicker();
	},
	
	submit : function(e) {
		e.preventDefault();
		var	value = {}
			;
		if (this.dates.eq(0).val() == "" && this.dates.eq(1).val() == "") {
			return false;
		}
		if (this.dates.eq(0).val()) {
			value.from = this.dates.eq(0).val();
		}
		if (this.dates.eq(1).val()) {
			value.to = this.dates.eq(1).val();
		}
		this.model.filter(value);
		this.refresh();
	},
	
	refresh : function() {
    	var that = this;
    	this.model.fetch({
    		success: function (collection, response, options) {
    			that.render();
    		},
    		error: function (collection, response, options) {
    			alert('Ошибка при обновлении фильтра статистики');
    		}
    	});
    },
    
    render : function() {
		$(this.el).find('.order-stat-filter-result').html(this.template({model : this.model.toJSON()}));
	},

});