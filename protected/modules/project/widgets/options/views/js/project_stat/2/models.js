App.Models.OrderStat = Backbone.Model.extend({
	
	base_url : 'filter_stat/',
	
	url_params : {},
	
	url : function() {
		return App.assembleUrl(this.base_url, this.url_params);
	},
	
	url_params : {},
	
	filter : function (obj) {
		if (_.size(obj) == 0) {
			return;
		}
		var filter = "";
		for (var param in obj) {
			this.url_params['filter['+param+']'] = obj[param];
		}
		return this;
	},
	
	getCollection : function(job_title) {
		var list = this.get(job_title);
		if (this.collection == undefined)
			this.collection = new Backbone.Collection(list);
		return this.collection;
	}
	
});
