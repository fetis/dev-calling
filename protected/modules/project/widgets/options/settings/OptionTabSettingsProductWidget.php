<?php 

class OptionTabSettingsProductWidget extends OTabWidget
{
	protected static $_alias = 'product';
	
	protected static $_head = 'Продукция';
	
	public function run()
	{
		$project_id = OrderModule::$project_id;
		Yii::app()->getClientScript()->registerScript(self::$_alias, '{
			var grid = new BbGrid(),
				filter_conf = new BbGridFilter(),
				filter_speaker = new BbGridFilter()
				;
			grid.url = "/project/' . $project_id . '/product/";
			grid.alias = "product";
			grid.el = "#product";
			grid.title = "' . self::$_head . '";
			grid.run();

			filter_conf.el = $("#product .js-conf-filter");	
			filter_conf.grid = grid;
			filter_conf.callback = function(e) {
				var	$el = $(e.currentTarget),
					id = $el.attr("data-id")
					;
				if (id != "all") {
					$("#product .js-speaker-filter").find(".badge-filter").addClass("hidden");
					$("#product .js-speaker-filter").find(".badge-filter[data-conf=\'"+id+"\']").removeClass("hidden");
				} else {
					$("#product .js-speaker-filter").find(".badge-filter").removeClass("hidden");
				}	
				e.preventDefault();
			};
			filter_conf.init();
			
			filter_speaker.el = $("#product .js-speaker-filter");	
			filter_speaker.grid = grid;
			filter_speaker.init();
				
		}', CClientScript::POS_END);
		$this->render('optionTabSettingsProductWidget');
	}
}