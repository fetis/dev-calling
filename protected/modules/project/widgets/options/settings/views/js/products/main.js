$(function () {
	
	var products_coll = new App.Collections.ProductsCollection();
	products_coll.fetch({
		success: function (collection, response, options) {
			App.Data.Products = products_coll;
			new App.Views.ProductsListView({collection:products_coll});
		},
		error: function (collection, response, options) {
		}
	});
	new App.Views.ProductsAddView();
	
});
