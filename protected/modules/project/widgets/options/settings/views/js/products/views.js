App.Views.ProductsRowView = Backbone.View.extend({

	tagName : 'tr',

	template:_.template($('#tpl-products-row').html()),
	
	events : {'click .js-btn-remove' : 'remove',
			  'click .js-btn-edit' : 'edit',
			 },
    initialize:function (options) {
        $(this.el).html(this.template(this.model.toJSON()));
        return this;
    },
	remove : function(){
		new App.Views.ProductsDeleteView({model:this.model}).render();
	},
	edit : function(){
		new App.Views.ProductsFormView({model:this.model}).render();
	}
});

App.Views.ProductsListView = Backbone.View.extend({

	container : $('.js-products-list'),
	
	initialize:function (options) {
        EventManager.on('resetProductsList', this.render, this);
		this.render();
    },

	render:function () {
		var that = this;
		that.container.html('');
		
		this.collection.fetch({
    		success: function (collection, response, options) {
    			that.collection.each(function (products, i) {
    	            var html = new App.Views.ProductsRowView({model:products}).el;
    				that.container.append(html);
    	        });
    		},
    		error: function (collection, response, options) {
    			alert('Ошибка при обновлении списка товаров');
    		}
    	});
		
	}

});

App.Views.ProductsFormView = App.Views.EditView.extend({
	
	title : 'Редактирование продукта',
	
	template:_.template($('#tpl-products-form').html()),
	
	render : function() {
		this.renderWindow();
		this.renderForm();
		this.show();
	},
	
	submitForm : function(e) {
		e.preventDefault();
		var data = App.getFormData($(e.currentTarget));
		this.model.set(data);
		this.model.save(null, {
			success : function(){
				EventManager.trigger('resetProductsList');
			},
			error : function() {
				return false;
			}
		});
		this.$el.modal('hide');
	},
	
});

App.Views.ProductsAddView = Backbone.View.extend({
	
	el : '#js-products-add',
	
	events : {'click' : 'click'},
	
	title : 'Добавление продукта',
	
	click : function(e) {
		e.preventDefault();
		new App.Views.ProductsFormView({model:new App.Models.Products(), title : this.title}).render();	
	},
	
});

App.Views.ProductsDeleteView = App.Views.ModalDialogView.extend({
	
	title : 'Удаление товара',
	
	template:_.template($('#tpl-products-remove').html()),
	
	submit : function(e) {
		var that = this;
		e.preventDefault();
		this.model.remove(function(){	
			EventManager.trigger('resetProductsList');
		},function(){
			alert('Ошибка при удалении товара');
		});
		that.hide();
	}
	
});

