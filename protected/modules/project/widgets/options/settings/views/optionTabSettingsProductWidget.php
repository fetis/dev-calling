<h2>Продукция</h2>
		
<div id="product">		
								
	<div class="row">
		
		<div class="col-md-2">
			<div class="btn-group">
				<button class="btn btn-primary js-list-add" data-toggle="modal" data-target="#myModal">
					<span class="glyphicon glyphicon-plus-sign"></span>&nbsp;Добавить
				</button>
			</div>
		</div>
		
		<div class="col-md-10">
			<div class="js-conf-filter" data-key="conferention">
				<span>Конференции:</span>
				
				<span class="badge badge-filter" data-id="all">Все</span>
				<?php $list = Conferention::model()->findAll();?>
				<?php foreach ($list as $item):?>
					<span data-id="<?php echo $item->id?>" class="badge badge-filter" >
						<?php echo $item->name?>
					</span>	
				<?php endforeach?>
			</div>
			
			<div class="js-speaker-filter" data-key="speaker">
				<span>Спикер:</span>
				
				<span class="badge badge-filter" data-id="all">Все</span>
				<?php $list = Speaker::model()->findAll();?>
				<?php foreach ($list as $item):?>
					<span data-id="<?php echo $item->id?>" data-conf="<?php echo $item->conferention_id?>" class="badge badge-filter">
						<?php echo $item->name?>
					</span>	
				<?php endforeach?>
			</div>
		</div>
	</div>
	
	<table class="table table-hover ">
		<thead>
		  <tr>
			<th width="50">#</th>
			<th>Конференция</th>
			<th>Спикер</th>
			<th>Название</th>
			<th class="text-right" width="100">Цена</th>
			<th width="100"></th>
		  </tr>
		</thead>
		<tbody class="js-item-list">
		</tbody>
	</table>

	<script type="text/template" id="tpl-form-product">	
		<form id="js-products-form" class="form-horizontal" role="form">

			<div class="form-group">
				<label for="label" class="col-sm-3 control-label">Спикер</label>
				<div class="col-sm-9">
					<select class="form-control" id="speaker_id" name="speaker_id">
						<%_.each(Common_Data_Speakers,function(el,id){ %>
							<option value="<%=id%>" <% if (model.speaker_id == id) { %>selected="selected"<%}%>>
								<%=el%>
							</option>	
						<% });%>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label for="label" class="col-sm-3 control-label">Название</label>
				<div class="col-sm-9">
				  <input type="text" class="form-control" id="label" name="label" placeholder="" value="<%=model.label%>">
				  <span class="help-block hidden"></span>
				</div>
			</div>

			<div class="form-group">
				<label for="label" class="col-sm-3 control-label">Ссылка на LP</label>
				<div class="col-sm-9">
				  <input type="text" class="form-control" id="product_url" name="product_url" placeholder="" value="<%=model.product_url%>">
				  <span class="help-block hidden"></span>
				</div>
			</div>
			
			<div class="form-group">
				<label for="price" class="col-sm-3 control-label">Цена</label>
				<div class="col-sm-9">
				  <input type="text" class="form-control" id="price" name="price" placeholder="" value="<%=model.price%>">
				  <span class="help-block hidden"></span>
				</div>
			</div>

			<div class="form-group">
				<label for="price" class="col-sm-3 control-label">Текст письма оплаты</label>
				<div class="col-sm-9">
					<textarea class="form-control" id="email_text" name="email_text"><%=model.email_text%></textarea>
				  	<span class="help-block hidden"></span>
				</div>
			</div>

		</form>
	</script>
	
	<script type="text/template" id="tpl-row-product">
		<td><%=model.id%></td>
		<td><%=Common_Data_Confs[model.conferention_id]%></td>
		<td><%=Common_Data_Speakers[model.speaker_id]%></td>
		<td><%=model.label%></td>
		<td class="text-right"><%=model.price%></td>
		<td class="text-right">
			<div class="btn-group btn-group-panel">
				<button class="btn btn-default js-btn-edit" type="button"><span class="glyphicon glyphicon-pencil"></span></button> 
				<button class="btn btn-default js-btn-remove" type="button"><span class="glyphicon glyphicon-minus-sign"></span></button>
			</div>
		</td>
	</script>
	
	<script type="text/template" id="tpl-remove-product">
		Вы действительно хотите удалить продукт
	</script>
	
</div>