<h2>Спикеры</h2>

<div id="speakers">

	<div class="row">
		
		<div class="col-md-2">
			<div class="btn-group">
				<button class="btn btn-primary js-list-add" data-toggle="modal" data-target="#myModal">
					<span class="glyphicon glyphicon-plus-sign"></span>&nbsp;Добавить
				</button>
			</div>
		</div>
		
		<div class="col-md-9">
		
			<div class="js-speaker-filter" data-key="conferention">
				<span>Конференции:</span>
				
				<span class="badge badge-filter" data-id="all">Все</span>
				<?php $list = Conferention::model()->findAll();?>
				<?php foreach ($list as $item):?>
					<span data-id="<?php echo $item->id?>" class="badge badge-filter" ><?php echo $item->name?></span>	
				<?php endforeach?>
			</div>
		</div>
		
	</div>
	
	<table class="table table-hover">
		<thead>
		  <tr>
			<th width="70">#</th>
			<th>Конференция</th>
			<th>Название</th>
			<th>Статистика</th>
			<th width="70"></th>
		  </tr>
		</thead>
		<tbody class="js-item-list">
		</tbody>
	</table>
	
	<script type="text/template" id="tpl-row-speaker">
	<td><%=model.id%></td>
	<td><%=Common_Data_Confs[model.conferention_id]%></td>
	<td><%=model.name%></td>
	<td><a target="_black" href="http://melius7.com/speaker/index.php?id=<%=model.id%>&hash=<%=md5(model.id + 'hash')%>">link</a></td>
	<td>
		<div class="btn-group btn-group-panel">
			<button class="btn btn-default js-btn-edit" type="button"><span class="glyphicon glyphicon-pencil"></span></button> 
			<button class="btn btn-default js-btn-remove" type="button"><span class="glyphicon glyphicon-minus-sign"></span></button>
		</div>
	</td>
	</script>
	
	<script type="text/template" id="tpl-form-speaker">
<form class="form-horizontal" data-namespace="speaker" role="form">

	<div class="form-group">
		<label for="label" class="col-sm-3 control-label">Наименование</label>
		<div class="col-sm-9">
		  <input type="text" class="form-control" id="name" name="name" value="<%=model.name %>" placeholder="Наименование"/>
		</div>
	</div>

	<div class="form-group">
		<label for="label" class="col-sm-3 control-label">Сайт</label>
		<div class="col-sm-9">
		  <input type="text" class="form-control" id="site" name="site" value="<%=model.site %>" placeholder="Сайт"/>
		</div>
	</div>

	<div class="form-group">
		<label for="label" class="col-sm-3 control-label">Конференция</label>
		<div class="col-sm-9">
			<select class="form-control" id="conferention_id" name="conferention_id">
				<%_.each(Common_Data_Confs,function(el,id){ %>
					<option value="<%=id%>" <% if (model.conferention_id == id) { %>selected="selected"<%}%>>
						<%=el%>
					</option>	
				<% });%>
			</select>
		</div>
	</div>

</form>
	</script>
	
	<script type="text/template" id="tpl-remove-speaker">
	Вы действительно хотите удалить учетную запись?
	</script>

</div>