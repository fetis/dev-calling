<form class="form-horizontal form-settings" data-namespace="sms" role="form">

	<div class="row">
		<div class="col-md-12">
		  <div class="form-group">
			<div class="col-sm-12">
			  <select class="form-control" id="type" name="type">
				<option value="smspilot">SMS Пилот</option>
				<option value="sms48">SMS 48</option>
			  </select>
			</div>
		  </div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
		  <div class="form-group">
			<div class="col-sm-12">
				<input type="text" class="form-control" id="api_key" name="api_key" placeholder="API ключ (sms-pilot)"/>
			</div>
		  </div>
		  
		  <div class="form-group">
			<div class="col-sm-12">
				<input type="text" class="form-control" id="login" name="login" placeholder="Логин"/>
			</div>
		  </div>
		  
		  <div class="form-group">
			<div class="col-sm-12">
				<input type="text" class="form-control" id="password" name="password" placeholder="Пароль"/>
			</div>
		  </div>
		  
		  <div class="form-group">
			<div class="col-sm-12">
				<input type="text" class="form-control" id="from" name="from" placeholder="От кого"/>
			</div>
		  </div>
		  
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-offset-3 col-sm-9 text-right">
		  <button type="submit" class="btn btn-primary"><b>Сохранить настройки</b></button>
		</div>
	</div>
	
</form>