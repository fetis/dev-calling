<h2>Конференции</h2>

<div id="conferention">

	<div class="row">
		
		<div class="col-md-2">
			<div class="btn-group">
				<button class="btn btn-primary js-list-add" data-toggle="modal" data-target="#myModal">
					<span class="glyphicon glyphicon-plus-sign"></span>&nbsp;Добавить
				</button>
			</div>
		</div>
		
	</div>
	
	<table class="table table-hover">
		<thead>
		  <tr>
			<th width="70">#</th>
			<th>Название</th>
			<th width="70"></th>
		  </tr>
		</thead>
		<tbody class="js-item-list">
		</tbody>
	</table>
	
	<script type="text/template" id="tpl-row-conferention">
	<td><%=model.id%></td>
	<td><%=model.name%></td>
	<td>
		<div class="btn-group btn-group-panel">
			<button class="btn btn-default js-btn-edit" type="button"><span class="glyphicon glyphicon-pencil"></span></button> 
			<button class="btn btn-default js-btn-remove" type="button"><span class="glyphicon glyphicon-minus-sign"></span></button>
		</div>
	</td>
	</script>
	
	<script type="text/template" id="tpl-form-conferention">
<form class="form-horizontal" data-namespace="conferention" role="form">

	<div class="form-group">
		<label for="label" class="col-sm-3 control-label">Наименование</label>
		<div class="col-sm-9">
		  <input type="text" class="form-control" id="name" name="name" value="<%=model.name %>" placeholder="Наименование"/>
		</div>
	</div>

</form>
	</script>
	
	<script type="text/template" id="tpl-remove-conferention">
	Вы действительно хотите удалить учетную запись?
	</script>

</div>