<?php 

class OptionTabSettingsSpeakerWidget extends OTabWidget
{
	protected static $_alias = 'speaker';
	
	protected static $_head = 'Спикеры';
	
	public function run()
	{
		$project_id = OrderModule::$project_id;
		Yii::app()->getClientScript()->registerScript(self::$_alias, "{
			var grid = new BbGrid()
				filter = new BbGridFilter();
				;
			grid.url = '/project/$project_id/speaker/';
			grid.alias = 'speaker';
			grid.el = '#speaker';
			grid.title = '" . self::$_head . "';
			grid.run();
			filter.el = $('#speakers .js-speaker-filter');	
			filter.grid = grid;
			filter.init();		
		}", CClientScript::POS_END);
		$this->render('optionTabSettingsSpeakerWidget');
	}
}