<?php 

class OptionTabSettingsConferentionWidget extends OTabWidget
{
	protected static $_alias = 'conferention';
	
	protected static $_head = 'Конференции';
	
	public function run()
	{
		$project_id = OrderModule::$project_id;
		Yii::app()->getClientScript()->registerScript(self::$_alias, "{
			var grid = new BbGrid();
			grid.url = '/project/$project_id/conferention/';
			grid.alias = 'conferention';
			grid.el = '#conferention';
			grid.title = '" . self::$_head . "';
			grid.run();
		}", CClientScript::POS_END);
		$this->render('optionTabSettingsConferentionWidget');
	}
}