<?php 

class OptionTabSettingsSmsWidget extends OTabWidget
{
	protected static $_alias = 'sms';
	
	protected static $_head = 'Настройки SMS';
	
	protected static $_option_id = 12;
	
	public function run()
	{
		$this->render('optionTabSettingsSmsWidget');
	}
}