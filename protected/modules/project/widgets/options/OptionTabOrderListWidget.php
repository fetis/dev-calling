<?php 
class OptionTabOrderListWidget extends OTabWidget
{
	protected static $_alias = 'orders';
	
	protected static $_head = 'Заявки';
	
	protected $_views = array(
			'orderList','orderRow','orderDetail','orderForm',
			'orderRemove','productSelectItem','orderHistory',
		);
	
	public function run()
	{
		$this->render('optionTabOrderListWidget');
		$views = $this->_views;
		foreach ($views as $view)
		{
			$this->render('order/'.$view);
		}
	}
}