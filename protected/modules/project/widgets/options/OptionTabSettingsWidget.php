<?php 

class OptionTabSettingsWidget extends OTabWidget
{
	protected static $_alias = 'settings';
	
	protected static $_head = '<span class="glyphicon glyphicon-cog"></span>';
	
	protected static $_project_check = 'get-projectsettings';
	
	public function run()
	{
		$path = realpath(dirname(__FILE__)) . '/views/js/project_settings';
		$path = Yii::app()->assetManager->publish($path, false, -1, true);
		Yii::app()->clientScript->registerScriptFile($path . '/models.js',	CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile($path . '/views.js',	CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile($path . '/main.js',	CClientScript::POS_END);
		$this->render('optionTabSettingsWidget');	
	}
	
}