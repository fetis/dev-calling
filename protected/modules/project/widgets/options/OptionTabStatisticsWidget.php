<?php 

class OptionTabStatisticsWidget extends OTabWidget
{
	protected static $_alias = 'stat';

	protected static $_head = 'Статистика';

	protected static $_project_check = 'get-allstat';
	
	protected static $_option_id = 9;
	
	protected $_scripts_path = array(1 => 1, 2 => 1, 3 => 1, 4 => 2);

	public function run()
	{
		$project_id = OrderModule::$project_id;
		$project = Project::model()->findByPk($project_id);
		$stat_type = $project->stat_type;
		
		$path = realpath(dirname(__FILE__)) . '/views/js/project_stat';
		$path = Yii::app()->assetManager->publish($path, false, -1, true);
		Yii::app()->clientScript->registerScriptFile($path . '/' . $this->_scripts_path[$stat_type] . '/models.js',	CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile($path . '/' . $this->_scripts_path[$stat_type] . '/views.js',	CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile($path . '/main.js',	CClientScript::POS_END);
		$this->render('optionTabStatisticsWidget');
		
		$this->render('project_stat/projectStat' . $stat_type);
	}
	
}