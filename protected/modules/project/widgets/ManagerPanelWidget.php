<?php 

class ManagerPanelWidget extends CWidget
{
	public function run()
	{
		$path = realpath(dirname(__FILE__)) . '/views/js/managers';
		$path = Yii::app()->assetManager->publish($path, false, -1, true);
		Yii::app()->clientScript->registerScriptFile($path . '/models.js',	CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile($path . '/views.js',	CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile($path . '/main.js',	CClientScript::POS_END);
		$this->render('managerPanelWidget');
	}
}