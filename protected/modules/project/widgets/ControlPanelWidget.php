<?php 

class ControlPanelWidget extends CWidget
{
	public $terms = array();
	
	protected $_terms_options = array(
		'daily' => 		array('label' => 'Сег', 'title' => 'Сегодня'),
		'tomorrow' => 	array('label' => '+Вче', 'title' => 'Сегодня и вчера'),
		'weekly' => 	array('label' => 'Нед', 'title' => 'Неделя'),
		'monthly' => 	array('label' => 'Мес', 'title' => 'Месяц'),
		'custom' => 	array('label' => '...', 'title' => 'Выбрать дату'),
	);
	
	public function run()
	{
		if (ProjectChecker::allowed('not-show-filter'))
			return false;
		
		$terms = array();
		$terms_options = $this->_terms_options;
		if (! $this->terms)
		{
			$terms = $terms_options;
		}
		else 
		{
			foreach ($this->terms as $item)
			{
				if (isset($terms_options[$item]))
					$terms[$item] = $terms_options[$item];
			}
		}
		
		$states = $this->_getViewStates();
		$this->render('controlPanelWidget', array('terms' => $terms, 'view_states' => $states));
	}
	
	protected function _getViewStates ()
	{
		$info = ProjectChecker::getUserInfo();
		$role_id = $info['role_id'];

		$states = array(
			ProjectChecker::MANAGER => array(0,1,2,3,4,5,6,7,8,9,10),
		);
		
		if (! isset($states[$role_id])) 
			return false;
		
		return $states[$role_id];
	}

}