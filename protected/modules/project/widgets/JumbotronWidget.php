<?php 

class JumbotronWidget extends CWidget
{
	public $title;
	
	public function run()
	{
		$this->render('jumbotronWidget', array('title' => $this->title));
	}
}