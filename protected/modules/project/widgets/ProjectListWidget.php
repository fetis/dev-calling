<?php 

class ProjectListWidget extends CWidget
{
	public function run()
	{
		$model = new ProjectUser();
		$data = $model->getProjectsList();
		$this->render('projectListWidget', array('list' => $data));
	}
}