<div id="js-manager-panel-info">

</div>

<script type="text/template" id="tpl-manager-panel-info">
	<div class="manager-panel-info">
	<table class="table">
		<tr>
			<th></th>
			<th class="text-center">Конверсия</th>
			<th class="text-center">Отказ</th>
			<th class="text-center">З/П</th>
			<th class="text-center">Заказов в обработке</th>
		</tr>
		<tr>
			<td><b>Мой месяц</b></td>
			<td class="text-center"><%=model.month.k1.toFixed(2)%></td>
			<td class="text-center"><%=model.month.refuse.toFixed(2)%></td>
			<td class="text-center"><%=model.month.salary.toFixed(2)%></td>
			<td class="text-center"><%=model.month.pended_count%></td>
		</tr>
		<% _.each(model.managers, function(manager,i) { %>
		<tr>
			<td><%=Common_Data_Managers[i]%></td>
			<td class="text-center"><%=manager.k1.toFixed(2)%></td>
			<td class="text-center"><%=manager.refuse.toFixed(2)%></td>
			<td class="text-center"><%=manager.salary.toFixed(2)%></td>
			<td class="text-center"><%=manager.pended_count%></td>
		</tr>
		<% }); %>
	</table>
	</div>
</script>