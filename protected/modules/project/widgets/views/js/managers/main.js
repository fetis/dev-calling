var manager_info_model = new App.Models.ManagersPanelModel()
	;
manager_info_model.fetch({
	success: function (collection, response, options) {
		new App.Views.ManagersPanelInfoView({model : manager_info_model}).render();
	},
	error: function (collection, response, options) {
	}
});