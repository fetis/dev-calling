App.Views.ManagersPanelInfoView = Backbone.View.extend({
	
	el : '#js-manager-panel-info',
	
	template :_.template($('#tpl-manager-panel-info').html()),
	
	initialize:function (options) {
		EventManager.on('resetManagerPanelInfo', this.refresh, this);
		this.setInterval();
        return this;
    },
	
	render : function()
	{
		$(this.el).html(this.template({model : this.model.toJSON()}));	
	},

	refresh : function()
	{
		var that = this;
		that.model.fetch({
			success: function (collection, response, options) {
				that.render();
			},
			error: function (collection, response, options) {
			}
		});
	},
	
	setInterval : function() {
    	window.setInterval(function () {
    		EventManager.trigger('resetManagerPanelInfo');
    	}, 1000 * 60 * 10);
    },
	
});