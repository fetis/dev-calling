	<?php $user = Yii::app()->user?>
	<div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">
			<img src="/img/logo-min.png" alt="landing"/>&nbsp;&nbsp;CallCenter&nbsp;&nbsp;&nbsp;
		  </a>
        </div>
        <div class="navbar-collapse collapse">

          <?php $this->widget('ProjectListWidget')?>
		
		  <ul class="nav navbar-nav navbar-right">
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span>&nbsp;<?php echo $user->getState('nick_name')?><b class="caret"></b></a>
				<ul class="dropdown-menu">
				  <li><a href="/logout/">Выйти</a></li>
				</ul>
			</li>
          </ul>		
        </div><!--/.navbar-collapse -->
      </div>
    </div>