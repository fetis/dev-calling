<div class="btn-group btn-group-panel">
	
  <button type="button" class="btn btn-default js-btn-view"><span class="glyphicon glyphicon-eye-open"></span></button>	
	  
  <?php if (ProjectChecker::allowed('edit-order')):?>
  	<?php if (! ProjectChecker::allowed('edit-order-all')):?>	
	  <% if (model.owner_id == Common_Data_UserId || model.owner_id == 0) { %>
	  	<button type="button" class="btn btn-default js-btn-edit"><span class="glyphicon glyphicon-pencil"></span></button>
	  <% } %>
	<?php else:?>
		<button type="button" class="btn btn-default js-btn-edit"><span class="glyphicon glyphicon-pencil"></span></button>
  	<?php endif?>
  <?php endif?>
  
  <?php if (ProjectChecker::allowed('delete-order')):?>
	<button type="button" class="btn btn-default js-btn-remove"><span class="glyphicon glyphicon-remove"></span></button>
  <?php endif?>

</div>