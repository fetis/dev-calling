<script type="text/javascript">
	Common_Data_AllNearStates = <?php echo CJSON::encode(OrderStatic::getAllNearStates())?>;
	Common_Data_AllStates = 	<?php echo CJSON::encode(OrderStatic::getStates())?>;
	<?php $model = new ProjectUser();?>
	Common_Data_Managers = <?php echo CJSON::encode($model->getTeamVector())?>;
	Common_Data_Confs = <?php echo CJSON::encode(CHtml::listData(Conferention::model()->findAll(), 'id', 'name'))?>;
	Common_Data_Speakers = <?php echo CJSON::encode(CHtml::listData(Speaker::model()->findAll(), 'id', 'name'))?>;
	<?php $dataProvider = new JSonActiveDataProvider(new Product(),array('attributes' => array('id','label','price','conferention_id', 'speaker_id'),'pagination' => false));?>
	Common_Data_Products = <?php echo $dataProvider->getJsonData();?>;
	Common_Data_UserId = <?php echo Yii::app()->user->getId();?>

	function dateToMysqlDateTime(date)
	{
		var str = date.getFullYear().toString() ;
		str += '-';
		str += ("0" + (date.getMonth()+1)).slice(-2) 
		str += '-' 
		str += ("0" + date.getDate()).slice(-2) 
		str += ' '
		str += ("0" + date.getHours()).slice(-2) 
		str += ':'
		str += ("0" + date.getMinutes()).slice(-2)
		str += ':'
		str += ("0" + date.getSeconds()).slice(-2)
		;
		return str;
	}
	
	function getSmallDate(str)
	{
		if (! str)
		{
			return "";
		}
		var date = new Date(str.substr(0,10)),
			day = date.getDate(),
			month = date.getMonth() + 1
			;
		return  ((day < 10)? "0" + day : day) + '-' + ((month < 10)? "0" + month : month) + ' ' + str.substr(11,5);
	}
	
	function getDate(str) 
	{
		if (! str)
		{
			return "";
		}
		var date = new Date(str),
			year = date.getFullYear(),
			day = date.getDate(),
			month = date.getMonth() + 1
			;
		return  ((day < 10)? "0" + day : day) + '.' + ((month < 10)? "0" + month : month) + '.' + year;
	}
	
	getAllNearSataes = function(state_id)
	{
		if (! state_id)
			return Common_Data_AllNearStates[""];
		return Common_Data_AllNearStates[state_id];
	};

	getAllNearSataesExcept = function(state_id)
	{
		var res = getAllNearSataes(state_id);
		if (res) {
	    	_.each(res, function(el, i) {
	    		if (el == state_id)
	    		{
		    		delete(res[i]);
	    		}	
	    	});
	    	return res;
    	}
	};

	getProductInfo = function(id)
	{
		var product = App.Data.Products.findWhere({id : id});
		if (product != undefined)
			return product.get('label');
	}
	
</script>