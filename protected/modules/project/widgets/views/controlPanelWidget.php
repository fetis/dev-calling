<div class="row">
	
	<div class="col-md-9">
		<div class="btn-group">

			<button id="js-collection-refresh" class="btn btn-primary">
				<span class="glyphicon glyphicon-refresh"></span>&nbsp;Обновить
			</button>
			
		</div>
	</div>
	
	<div class="col-md-3">
		
	  <form id="js-search-order-form" class="order-search-form search-form navbar-form navbar-right" role="search">
		<div class="form-group">
		  <input type="text" class="form-control" placeholder="фио/#/Город/Телефон">
		</div>
	  </form>
		
	</div>
	
</div>

<div class="row" style="padding-top: 20px;">
	
	<div class="col-md-3">
		
		<div id="js-order-filter-date" class="b-order-filter order-filter-date">
			<span>Дата создания:</span>
			<br/>
			<?php foreach ($terms as $key=>$term):?>
				<span data-id="<?php echo $key?>" class="badge badge-filter" title="<?php echo $term['title']?>"><?php echo $term['label']?></span>
			<?php endforeach?>
			<br/>
			<form class="form-inline custom-date-form" role="form">
				<div class="form-group">
					<input class="date-filter date-filter-from form-control" placeholder="Начальная дата"/>
				</div>
				<div class="form-group">
					<input class="date-filter date-filter-to form-control" placeholder="Конечная дата"/>
				</div>
				<button class="btn btn-primary">Ок</button>
			</form>
		</div>
	</div>
	
	<div class="col-md-9">
		
		<div id="js-order-filter-state" class="b-order-filter order-filter-state">
			<span>Статусы:</span>
			<br/>
			<span class="badge badge-filter" data-id="all">Все</span>
			<?php $states = OrderStatic::getStates();?>
			<?php foreach ($states as $key => $state):?>
				
				<?php if ($view_states && ! in_array($key, $view_states)):?>
					<?php continue;?>
				<?php endif;?>
			
				<span data-id="<?php echo $key?>" class="badge badge-filter" ><?php echo $state?></span>	
			<?php endforeach?>
		</div>
	</div>
	
	<!-- div class="col-md-2">
		
		<div class="b-order-filter order-filter-recall">
			<span>Дозвон сегодня:</span>
			<br/>
			<span id="js-order-filter-recall" class="badge badge-filter"><span class="glyphicon glyphicon-phone-alt"></span></span>
		</div>
	</div -->
	
</div>