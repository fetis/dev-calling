<span class="btn-group landing-select">
  <span class="" data-toggle="dropdown">
    LP <span class="caret"></span>
  </span>
  <ul class="dropdown-menu" role="menu">
    <?php if ($list):?>
		<?php foreach ($list as $key=>$label):?>
		  	<li><a href="/landing/<?=$key?>/"><?=$label?></a></li>
		<?php endforeach;?>
	<?php endif?>
    <li class="divider"></li>
    <li><a href="/landing/add/<?=$project_id?>/">Добавить</a></li>
  </ul>
</span>