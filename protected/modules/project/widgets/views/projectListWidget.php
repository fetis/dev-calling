<ul class="nav navbar-nav">
	<li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown">Проекты <b class="caret"></b></a>
		<ul class="dropdown-menu">
		
		  <li>	
			<a href="/newproject/">
				Создать проект
			</a>
		  </li>
		  
		  <li class="divider"></li>
		  
		<?php if ($list):?>
		  <?php foreach ($list as $item):?>
		  	<li><a href="/project/<?=$item['id']?>"><?=$item['label']?></a></li>
		  <?php endforeach;?>
		<?php endif?>
		
		  <li class="divider"></li>
		  
		  <li>	
			<a href="/total/">
				Все проекты
			</a>
		  </li>
		  
		</ul>
	</li>
</ul>	