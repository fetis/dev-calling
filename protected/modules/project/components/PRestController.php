<?php 
class PRestController extends RestController
{	
	public function loadModel($id = null)
	{
		$model_class = $this->model_class;
		if ($id)
		{
			if (($model = $model_class::model()->findByPk($id)) === null)
			{
				throw new CHttpException( 404, 'Requested page was not found!');
			}
		}
		else
		{
			$model = new $model_class();
		}
		$model->project_id = OrderModule::$project_id;
	
		return $model;
	}
}