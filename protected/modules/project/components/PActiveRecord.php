<?php 

class PActiveRecord extends CActiveRecord
{
	public function defaultScope()
	{
		$project_id = OrderModule::$project_id;
		return array(
			'condition'=>"t.project_id=:project_id and t.active=1",
			'params' => array(':project_id' => $project_id),
		);
	}
	
}