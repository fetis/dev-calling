<?php $this->project = $model?>
<?php foreach ($messages as $message):?>
	<div class="alert alert-<?=$message['type']?>">
		<strong>Внимание!</strong><?=$message['message']?>
	</div>
<?php endforeach?>
<script type="text/javascript">
	App_Projects = <?php echo CJSON::encode(array($this->project->id => $this->project->label))?>;
</script>