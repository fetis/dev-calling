<div class="form container">

    <?php $form = $this->beginWidget('CActiveForm', array('id' => 'registration-form','htmlOptions'=>array(
		'class' => "form-newproject form-horizontal form-white",
		'role' => "form",
	))); ?>
	
	
	<h1>Добавление нового проекта</h1>
	<hr>

    <?php echo $form->errorSummary($model); ?>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'label', array('class' => 'col-sm-4 control-label')); ?>
        <div class="col-sm-8">
        	<?php echo $form->textField($model, 'label', array("type"=>"text", "class"=>"form-control", "placeholder"=>"")); ?>
        </div>
    </div>
    
    <div class="form-group">
        <?php echo $form->labelEx($model, 'description', array('class' => 'col-sm-4 control-label')); ?>
        <div class="col-sm-8">
        	<?php echo $form->textArea($model, 'description', array("type"=>"text", "class"=>"form-control", "placeholder"=>"")); ?>
        </div>
    </div>
    
    <div class="form-group">
        <?php echo $form->labelEx($model, 'token', array('class' => 'col-sm-4 control-label')); ?>
        <div class="col-sm-8">
        	<?php echo $form->textField($model, 'token', array("type"=>"text", "class"=>"form-control", "placeholder"=>"")); ?>
        </div>
    </div>

    <div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
		  <button type="submit" class="btn btn-default">Добавить</button>
		</div>
	</div>

	<hr>
	
    <?php $this->endWidget(); ?>
</div><!-- form -->
