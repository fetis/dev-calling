<?php 

class NewProjectForm extends CFormModel
{
	
	public $label;
	public $description;
	public $token;
	
	public function rules()
	{
		return array(
				array('label, token', 'filter', 'filter' => 'trim'),
				array('label, token', 'filter', 'filter' => array($obj = new CHtmlPurifier(), 'purify')),
				array('label, token', 'required'),
				array('label,description', 'length', 'max' => 1024),
				array('token', 'length', 'min' => 32),
		);
	}
	
	public function attributeLabels()
	{
		return array(
			'label'  => Yii::t('UserModule.user', 'Название проекта'),
			'token'  => Yii::t('UserModule.user', 'Ключ удаленного доступа'),
			'description'      => Yii::t('UserModule.user', 'Описание'),
		);
	}
}