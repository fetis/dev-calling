<?php
Yii::import('application.modules.project.components.OrderModule');
class ProjectModule extends OrderModule
{
	public function init()
	{
		$this->setImport(array(
			'project.components.*',
			'project.forms.*',
			'project.models.*',
			'project.models.Log.*',
			'project.models.services.*',
			'project.models.services.sms.*',
			'project.models.services.sms.Smspilot.*',
			'project.models.Salary.*',
			'project.models.Order.*',
			'project.models.Order.State.*',
			'project.models.Order.State.Changer.*',
			'project.models.Order.Notify.*',
			'project.models.Speaker.*',
			'project.models.Product.*',
			'project.models.Project.*',
			'project.models.PayConfirm.*',
			'project.models.Stat.*',
			'project.models.Stat.Stat4.*',
			'project.widgets.*',
			'project.widgets.options.*',
			'project.widgets.options.settings.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			if ($controller instanceof RemoteController && $action == 'payment')
				return true;
			
			$project_id = Yii::app()->getRequest()->getParam('id');

			if ((! $project_id || ! Project::checkByUser($project_id)) && 
				(! $controller instanceof RemoteController) &&
				(! $controller instanceof ServiceController) &&
				(! $controller instanceof EmptyController)) 
			{	
				throw new CHttpException(403,'Доступ запрещен!');
			}
			self::$project_id = $project_id;
			ProjectChecker::initUserInfo();
			return true;
		}
		else
			return false;
	}
	
	public function getDefaultProject()
	{
		return Project::model()->getDefault();
	}
	
}
