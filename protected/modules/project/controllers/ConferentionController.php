<?php 

class ConferentionController extends PRestController
{
	public $model_class = 'Conferention';
	
	public function actions()
	{
		return array(
				'index' => array(
						'class' => 'application.components.actions.IndexAction',
						'attributes' => array('id','name'),
				),
				'one' => array(
						'class' => 'application.components.actions.OneAction',
				),
				'save' => array(
						'class' => 'application.components.actions.SaveAction',
				),
				'delete' => array(
						'class' => 'application.components.actions.CommonAction',
						'method' => 'delete',
				),
		);
	}
}