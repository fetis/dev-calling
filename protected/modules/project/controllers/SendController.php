<?php 

class SendController extends PRestController
{
	public $model_class = 'Order';
	
	public function actionSms()
	{
		$text = Yii::app()->request->getParam('text', false);
		$phone = Yii::app()->request->getParam('phone', false);
		
		$sms_service = ProjectService::getSmsService();
		$sms_service->setPhone($phone);
		$sms_service->setMessage($text);
		$res = $sms_service->send();
		echo CJSON::encode(array('success' => $res, 'message' => 'Сообщение успешно отправлено'));
	}
	
	public function actionPaylink()
	{
		$id = Yii::app()->request->getParam('iid', false);
		$model = $this->loadModel($id);
		$sender = new OrderPayLink($model);
		$res = $sender->send();
		echo CJSON::encode(array('success' => $res, 'message' => 'Сообщение успешно отправлено'));
	}
}