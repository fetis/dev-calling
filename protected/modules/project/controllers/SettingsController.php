<?php

class SettingsController extends RestController
{
	
	public function actionAll()
	{
		ProjectChecker::check('get-projectsettings');
		$model = new ProjectSettings();
		$res = $model->getAll();
		echo CJSON::encode($res);
	}
	
	public function actionSave($namespace)
	{
		ProjectChecker::check('save-projectsettings');
		$model = new ProjectSettings();
		$attributes = $this->getJsonInput();
		ProjectSettings::set($namespace,$attributes['data']);
		echo CJSON::encode(array('success' => true));
	}

}