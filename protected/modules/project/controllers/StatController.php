<?php

class StatController extends RestController
{
	
	public function actionFilter()
	{
		$filter = Yii::app()->request->getQuery('filter', false);
		$user = Yii::app()->user;
		$filter['owner_id'] = (in_array( 
									$user->getState("access_level"), 
									array(WebUser::SUPER_USER, WebUser::MANAGER_PLUS_STAT) 
									) 
							  ) 
								? null 
								: $user->getId();
		
		$filter_obj = new RussianDateFilter();
		
		if (isset($filter['from']) && $filter['from']) {
			$filter['from'] = $filter_obj->filter($filter['from']);
		}
		
		if (isset($filter['to']) && $filter['to']) {
			$filter['to'] = $filter_obj->filter($filter['to']);
		}  
		
		$project = Project::model()->findByPk(OrderModule::$project_id);
		$model = StatAbstract::factory($project->stat_type);
		$res = $model->getStat($filter);
		echo CJSON::encode($res);
	}
	
	public function actionManagers()
	{
		$project = Project::model()->findByPk(OrderModule::$project_id);
		$model = StatAbstract::factory($project->stat_type);
		$res = $model->getManagersInfo();
		echo CJSON::encode($res);
	}
	
}