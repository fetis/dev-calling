<?php

class OrderController extends PRestController
{
	
	public $model_class = 'Order';
	
	public function actions()
	{
		return array(
			'index' => array(
				'class' => 'application.modules.project.controllers.order.IndexAction',
				'attributes' => OrderStatic::getParamsToProjectOrderList(),
			),
			'one' => array('class' => 'application.modules.project.controllers.order.OneAction'),
			'create' => array('class' => 'application.modules.project.controllers.order.SaveAction'),
			'update' => array('class' => 'application.modules.project.controllers.order.SaveAction'),
			'delete' => array(
				'class' => 'application.components.actions.CommonAction',
				'check' => 'delete-order',
				'method' => 'remove',
			),
			'bind' => array(
				'class' => 'application.components.actions.CommonAction',
				'method' => 'bind',
			),
			'history' => array('class' => 'application.modules.project.controllers.order.HistoryAction'),
		);
	}
	
	public function loadModel($id = null)
	{
		$action = Yii::app()->request->getQuery('action', 'read');
		if ($action == 'update')
		{
			if ($id && ProjectChecker::allowed('lock-order'))
			{
				$order_locker = new OrderLocker();
				$order_locker->checkLocked($id);
			}
		}
	
		return parent::loadModel($id);
	}
	
}