<?php

class ProductController extends PRestController
{
	
	public $model_class = 'Product';
	
	public function actions()
	{
		return array(
				'index' => array(
						'class' => 'application.components.actions.IndexAction',
						'attributes' => array('id','label','speaker_id','conferention_id','price','product_page','product_url','email_text'),
						'criteria' => true,
				),
				'one' => array(
						'class' => 'application.components.actions.OneAction',
				),
				'save' => array(
						'class' => 'application.components.actions.SaveAction',
				),
				'delete' => array(
						'class' => 'application.components.actions.CommonAction',
						'method' => 'delete',
				),
		);
	}
	
}