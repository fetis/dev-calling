<?php 

class RemoteController extends CController 
{
	
	public function actionOrder()
	{
		if(! isset($_GET['token'], $_GET['params']))
			exit;
		
		$token = $_GET['token'];
		$params = $_GET['params'];
		$referer = $_SERVER['REMOTE_ADDR'];
		
		LogRemote::log($token, $referer, $params, 'add');
		
		$model = new LpAuth($token, $referer);
		
		$res = null;
		
		if ($model->isAuth())
		{
			echo CJSON::encode(array(
				'res' => $model::ERROR_NONE, 
				'id' => $model->save($params),
			));
		}
		else 
		{
			echo CJSON::encode(array(
				'res' => $model->errorCode,
			));
		}
		exit;
	}
	
	public function actionPayment()
	{
		$service_id = Yii::app()->request->getParam('service', null);
		$referer = $_SERVER['REMOTE_ADDR'];
		Yii::log($service_id . serialize($_REQUEST), CLogger::LEVEL_WARNING, 'app.payment');
		$service = PayConfirmAbstract::factory($service_id);
		$service->run();
		exit;
	}
	
	public function actionInfo()
	{
		$order_id = Yii::app()->request->getParam('iid', null);
		$order = Order::model()->findByPk($order_id);
		echo CJSON::encode($order->getInfoForPayment());
		exit;
	}
	
	public function actionSpeaker()
	{
		$id = Yii::app()->request->getParam('iid', null);
		$params = array('id' => $id);
		$model = new Stat4Speaker();
		echo CJSON::encode($model->get($params));
		exit;
	}
	
	public function actionProduct()
	{
		$id = Yii::app()->request->getParam('iid', null);
		$product = Product::model()->findByPk($id);
		echo CJSON::encode($product->getAttributes());
		exit;
	}
	
	public function actionPartner()
	{
		if(! isset($_GET['token'], $_GET['params']))
			exit;
		
		$token = $_GET['token'];
		$params = $_GET['params'];
		$referer = $_SERVER['REMOTE_ADDR'];
		
		LogRemote::log($token, $referer, $params, 'add');
		
		$model = new LpAuth($token, $referer);
		
		$res = null;
		
		if ($model->isAuth())
		{
			$partner = new Partner();
			$partner->setAttributes($params);
			$res = $partner->save(false);
			
			echo CJSON::encode(array(
				'res' => $model::ERROR_NONE, 
			));
		}
		else 
		{
			echo CJSON::encode(array(
				'res' => $model->errorCode,
			));
		}
		exit;
	}
}