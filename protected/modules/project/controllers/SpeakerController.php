<?php 

class SpeakerController extends PRestController
{
	public $model_class = 'Speaker';
	
	public function actions()
	{
		return array(
			'index' => array(
					'class' => 'application.components.actions.IndexAction',
					'attributes' => array('id','name','conferention_id'),
					'criteria' => true,
			),
			'one' => array(
					'class' => 'application.components.actions.OneAction',
			),
			'save' => array(
					'class' => 'application.components.actions.SaveAction',
			),
			'delete' => array(
					'class' => 'application.components.actions.CommonAction',
					'method' => 'delete',
			),
		);
	}
}