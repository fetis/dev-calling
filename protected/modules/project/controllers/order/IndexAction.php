<?php

class IndexAction extends CAction {
	
	public $attributes = array();
	
	public $check = null;
	
	public function run()
	{
		$model = $this->getController()->loadModel();
		
		$filter = Yii::app()->request->getQuery('filter', false);
		$sort = Yii::app()->request->getQuery('sort', 'date_recall desc, t.id Desc');
		$search = Yii::app()->request->getQuery('key', false);
		
		$filter_obj = new RussianDateFilter();
		
		if (isset($filter['date_type_from']) && $filter['date_type_from']) {
			$filter['date_type_from'] = $filter_obj->filter($filter['date_type_from']);
		}
		
		if (isset($filter['date_type_to']) && $filter['date_type_to']) {
			$filter['date_type_to'] = $filter_obj->filter($filter['date_type_to']);
		}
		
		$criteria = $model->getCriteria($filter, $search, $sort);
		$dataProvider = new JSonActiveDataProvider(
			$model,
			array(
					'attributes' => $this->attributes,
					'criteria' => $criteria,
					'pagination' => false,
			)
		);
		echo $dataProvider->getJsonData();
	}
	
}
