<?php 

class HistoryAction extends CAction 
{
	public function run($iid)
	{
		$model = $this->getController()->loadModel($iid);
		$res = $model->getSameOrders();
		echo CJSON::encode($res);
	}
}