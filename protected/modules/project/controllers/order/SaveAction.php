<?php

class SaveAction extends CAction {
	
	public function run()
	{
		$order_id = Yii::app()->request->getQuery('iid', null);
		
		$model = $this->getController()->loadModel($order_id);
		$attributes = $this->getController()->getJsonInput();
		$model->setAttributes($attributes);
		
		if (!$model->validate())
		{
			$errors = $model->getErrors();
			throw new CHttpException(403,'Ошибка при сохранении заказа: ' . CHtml::errorSummary($model));
		}
		if (!$model->save(false))
		{
			throw new CException('Cannot create a record');
		}
		echo CJSON::encode(array('success' => true));
	}
	
}
