<?php

class OneAction extends CAction {
	
	public function run($iid)
	{
		$model = $this->getController()->loadModel($iid);
		$action = Yii::app()->request->getQuery('action', 'read');
		if ($action == 'update')
		{
			$model->bind();
		}
		
		$item = Product::model()->findByPk($model->item_id);
		$res = $model->getAttributes();
		$res['product_url'] = array('product_url' => $item->product_page);
		echo CJSON::encode($res);
	}
	
}

