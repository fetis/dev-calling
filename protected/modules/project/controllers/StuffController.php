<?php 

class StuffController extends RestController 
{
	
	public function actionInvite()
	{
		$attributes = $this->getJsonInput();
		$model = new Stuff();
		$res = $model->invite($attributes);
		echo CJSON::encode($res);
	}
	
	public function actionIndex()
	{
		$model = new Stuff();
		$res = $model->getList();
		echo CJSON::encode($res);
	}
	
	public function actionAll()
	{
		$key = Yii::app()->request->getQuery('term', false);
		$model = new Stuff();
		$res = $model->getAll($key);
		echo CJSON::encode($res);
	}
	
	public function actionDelete($uid)
	{
		ProjectChecker::check('delete-stuff');
		$model = new Stuff();
		$res = $model->exclude($uid);
		echo CJSON::encode($res);
	}
	
}