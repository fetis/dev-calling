<?php

class DefaultController extends Controller
{
	
	public $layout='//layouts/project';
	
	public $project;
	
	public function actionIndex($id)
	{
		$model = $this->loadModel($id);
		$this->render('index', array('model' => $model, 'messages' => State::getMessages()));
	}
	
	public function loadModel($id)
	{
		$model = Project::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, Yii::t('tournament', 'Запрошенная страница не найдена.'));
		return $model;
	}
}