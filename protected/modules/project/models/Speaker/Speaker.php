<?php

class Speaker extends PActiveRecord
{

	public function tableName()
	{
		return 'speaker';
	}

	public function rules()
	{
		return array(
			array('id, project_id, active, conferention_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
			array('id, name, project_id, active', 'safe', 'on'=>'search'),
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function getCriteria($filter, $search=null, $sort=null)
	{
		$criteria = new SpeakerCriteria();
		return $criteria->get($filter, $search, $sort);
	}
}
