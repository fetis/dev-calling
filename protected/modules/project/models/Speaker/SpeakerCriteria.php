<?php 
class SpeakerCriteria
{
	public function get($filter, $search, $sort)
	{
		$criteria = new CDbCriteria();
		if ($filter)
		{
			$conferention_id = $filter['conferention'];
			if ($conferention_id != 'all')
			{
				$criteria->addCondition("conferention_id=$conferention_id");
			}
		}
		return $criteria;
	}
}