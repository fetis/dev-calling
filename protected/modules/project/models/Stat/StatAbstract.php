<?php 

class StatAbstract 
{
	const TOTAL_ID = "total";
	
	protected $_exclude_states = true;
	
	public function getStat($params)
	{
		return;
	}
	
	public static function factory($type)
	{
		return new Stat4();
	} 
	
	protected function _getWhere($params, $date_field = 'created')
	{
		$project_id = OrderModule::$project_id;
		$result = array();
	
		$where = array('o.project_id=:project_id');
		$where_params = array(':project_id' => $project_id);
	
		$where[] = "o.active=1";
	
		$from = (isset($params['from'])) ? $params['from'] : false;
		$to = (isset($params['to'])) ? $params['to'] : false;
		$owner_id = (isset($params['owner_id'])) ? $params['owner_id'] : false;
	
		if ($from)
		{
			$where[] = "DATE($date_field)>=:from";
			$where_params[':from'] = $from;
		}
	
		if ($to)
		{
			$where[] = "DATE($date_field)<=:to";
			$where_params[':to'] = $to;
		}
		if ($owner_id)
		{
			$where[] = "owner_id=:owner_id";
			$where_params[':owner_id'] = $owner_id;
		}
		
		if (isset($params['state_id']))
		{
			$where[] = "state_id=:state_id";
			$where_params[':state_id'] = $params['state_id'];
		}
		
		$excludes = $this->_getExceptedStates();
		if ($excludes)
		{
			$where[] = "state_id not in(" . implode(',', $excludes) . ")";
		}
		
		$where = (count($where)) ? implode(' and ', $where) : "";
	
		return array('clause' => $where, 'params'=> $where_params);
	}
	
	protected function _fillInterval($params)
	{
		$from = $params['from'];
		$to = ($params['to']) ? $params['to'] : date('Y-m') . '-01';
		$interval = array();
		$interval[0] = $from;
		
		if ($from < $to) {
			$current = $from;
			while ($current < $to) {
				$current = strtotime(date("Y-m-d", strtotime($current)) . " +1 day");
				$current = date('Y-m-d',$current);
				$interval[] = $current;
			}
		}
		return $interval;
	}

	protected function _getExceptedStates()
	{
		return array();
	}
}