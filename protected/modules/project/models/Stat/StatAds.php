<?php 
class StatAds {
	
	protected $_service;
	
	public function __construct($service) 
	{
		$this->_service = $service;
	}
	
	public function getCostByPeriod($params)
	{
		$model = AdsAbstract::factory($this->_service);
		return $model->getCostByPeriod($params);
	}
	
}