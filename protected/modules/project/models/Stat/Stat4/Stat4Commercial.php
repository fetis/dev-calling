<?php

class Stat4Commercial extends Stat4Abstract 
{
	public function get($params)
	{
		return array(
			'gain' => $this->getGain($params),
			'order_count' => $this->_getOrderCount($params),
		);
	}
	/**
	 * Возвращает реальную выручку
	 * @param unknown $params
	 */
	public function getGain ($params) 
	{
		$where = $this->_getWhere($params, 'date_paid');
		$res = Yii::app()->db->createCommand()
			->select('sum(price) as gain')
			->from('order o')
			->where($where['clause'], $where['params'])
			->queryScalar();
		return $res;
	}
	
	protected function _getOrderCount($params, $date = 'date_paid')
	{
		$where = $this->_getWhere($params, $date);
		$cmd = Yii::app()->db->createCommand()
			->select('count(1) as count')
			->from('order o')
			->where($where['clause'], $where['params'])
		;
		$res = $cmd->queryScalar();
		return $res;
	}

}