<?php 

class Stat4Speaker extends Stat4Abstract
{
	public function get($params)
	{
		$id = $params['id'];
		return array(
			'name' => $this->_getName($id),
			'items' => $this->_getItems($id),
			'states' => $this->_getStates(),
			'total' => $this->_getTotal($params),
			'orders' => $this->_getOrders($id),
		);
	}
	
	protected function _getName($id)
	{
		$speaker = Speaker::model()->findByPk($id);
		if (! $speaker)
			exit();
		return $speaker->name;
	}
	
	protected function _getItems($speaker_id)
	{
		return CHtml::listData(Product::model()->findAllByAttributes(array('speaker_id' => $speaker_id)), 'id', 'label');
	}
	
	protected function _getStates()
	{
		return CHtml::listData(OrderStateTable::model()->findAll(), 'id', 'label');
	}
	
	protected function _getTotal($params)
	{
		$speaker_id = $params['id'];
		$items = $this->_getItems($speaker_id);
		if (! $items)
			exit();
		
		$res = array();
		
		foreach ($items as $key => $item)
		{
			$params['item_id'] = $key;
			$res[$key] = $this->_getTotalForItem($params);
		}
		
		return $res;
	}
	
	protected function _getTotalForItem($params)
	{
		return array(
			'all' => $this->_getOrderCount($params, 'created'),
			'paid' => $this->_getOrderCount($params, 'date_paid'),
			'sum' => $this->_getTotalSum($params),
		);
	}
	
	protected function _getOrderCount($params, $date = 'created')
	{
		$where = $this->_getWhere($params, $date);
		$cmd = Yii::app()->db->createCommand()
			->select('count(1) as count')
			->from('order o')
			->where($where['clause'], $where['params'])
		;
		return $cmd->queryScalar();
	}
	
	protected function _getTotalSum($params)
	{
		$date = 'date_paid';
		$where = $this->_getWhere($params, $date);
		$cmd = Yii::app()->db->createCommand()
			->select('sum(price) as count')
			->from('order o')
			->where($where['clause'], $where['params'])
		;
		return $cmd->queryScalar();
	}
	
	protected function _getOrders($id)
	{
		$model = new Order();
		$criteria = new CDbCriteria();
		$criteria->addCondition("speaker_id=:speaker_id");
		$criteria->addCondition("state_id in (0,1,2,5)");
		$criteria->params = array(':speaker_id' => $id);
		$criteria->with = null;
		$criteria->order = 'created desc';
		$dataProvider = new JSonActiveDataProvider(
			$model,
			array(
				'attributes' => array('created','fio','phone','email','item_id','state_id','price',),
				'criteria' => $criteria,
				'pagination' => false,
			)
		);
		return $dataProvider->getArrayData();
	}
	
	protected function _getWhere($params, $date_field = 'created')
	{
		$where = parent::_getWhere($params, $date_field);
		
		$where['clause'] = $where['clause'] . " and $date_field is not null";
		
		if (isset($params['item_id']))
		{
			$where['clause'] = $where['clause'] . ' and item_id=:item_id';
			$where['params'][':item_id'] = $params['item_id'];
		}
		
		$id = $params['id'];
		$where['clause'] = $where['clause'] . ' and speaker_id=:speaker_id';
		$where['params'][':speaker_id'] = $id;
		return $where;
	}
}