<?php 

class Stat4Speakers extends Stat4Abstract
{
	public function get($params)
	{
		return array(
			'by_speakers' => $this->_getBySpeakers($params),
			'by_items' => $this->_getByItems($params),
		);
	}
	
	protected function _getBySpeakers($params)
	{
		$project_id = OrderModule::$project_id;
		$cmd = Yii::app()->db->createCommand()
			->select('s.`name`, sum(o.price) as price, count(o.id) as count')
			->from('order o')
			->leftJoin('speaker s','o.speaker_id=s.id')
			->where('o.state_id=2 and o.active=1 and o.project_id=:project_id', array(':project_id' => $project_id))
			->group('o.speaker_id')
			->order('o.speaker_id')
			;
		return $cmd->queryAll();
	}
	
	protected function _getByItems($params)
	{
		$project_id = OrderModule::$project_id;
		$cmd = Yii::app()->db->createCommand()
			->select('i.`label`, s.`name`, sum(o.price) as price, count(o.id) as count')
			->from('order o')
			->leftJoin('speaker s','o.speaker_id=s.id')
			->leftJoin('item i','o.item_id=i.id')
			->where('o.state_id=2 and o.active=1 and o.project_id=:project_id', array(':project_id' => $project_id))
			->group('o.item_id')
			->order('o.item_id')
		;
		return $cmd->queryAll();
	}
}