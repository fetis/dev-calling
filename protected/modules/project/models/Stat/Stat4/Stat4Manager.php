<?php 

class Stat4Manager extends Stat4Abstract
{
	/**
	 * Возвращает общую статисику по менеджеру(ам)
	 * @param unknown $params
	 * @return multitype:NULL
	 */
	public function get($params)
	{
		$this->_margin = null;
		return array(
			'k1' => $this->getK1($params),
			'refuse' => $this->getRefuse($params),
			'salary' => $this->_getSalary($params),
			'pended_count' => $this->_getPendedCount($params),
		);
	}
	
	/**
	 * Отношение принятых к общему числу заявок
	 * @param unknown $params
	 * @return number|multitype:number
	 */
	public function getK1($params)
	{
		$val1 = $this->_getOrderCount($params, 'date_paid');
		$val2 = $this->_getOrderCount($params, 'date_pay_error');
		$val1 = $this->_getSum($val1, $val2);

		$val2 = $this->_getOrderCount($params, 'created');
		$val3 = $this->_getOrderCount($params, 'date_spam');
		$val2 = $this->_getMinus($val2, $val3);
		
		return $this->_getRatio($val1, $val2, 100);
	}
	
	/**
	 * Процент отказа
	 * @param unknown $params
	 */
	public function getRefuse($params)
	{
		$this->_exclude_states = false;
		$val1 = $this->_getOrderCount($params, 'date_cancel');
		
		$val2 = $this->_getOrderCount($params, 'created');
		$val3 = $this->_getOrderCount($params, 'date_spam');
		$val2 = $this->_getMinus($val2, $val3);
		
		return $this->_getRatio($val1, $val2, 100);
	}
	
	protected function _getPendedCount($params)
	{
		$params['state_id'] = 1;
		return $this->_getOrderCount($params, 'created');
	}
	
	/**
	 * Возвращает реальную зарплату
	 * @param unknown $params
	 * @return multitype:
	 */
	protected function _getSalary($params)
	{
		$salary = $this->_getOrderItemPrice($params, 'date_paid', 1);
		$k1 = $this->getK1($params);
		return $this->_prepareSalary($salary, $k1);
	}
	
	protected function _prepareSalary($salary, $k1)
	{
		if (! is_array($salary) && ! is_array($k1))
		{
			return $salary * $this->_getSalaryKoeff($k1);
		}
		elseif (is_array($salary) && is_array($k1))
		{
			$res  = array();
			foreach ($salary as $key => $one)
			{
				$res[$key] = $salary[$key] * $this->_getSalaryKoeff($k1[$key]);
			}
			return $res;
		}
	}
	
	protected function _getSalaryKoeff($k1)
	{
		$koeff = 0;
		if ($k1 <= 50)
		{
			$koeff = 0.05;
		}
		elseif ($k1 > 50 && $k1 <= 60)
		{
			$koeff = 0.06;
		}
		else
		{
			$koeff = 0.07;
		}
		return $koeff;
	}
	
	/**
	 * Возвращает список менеджеров в проекте
	 */
	protected function _getManagers()
	{
		$model = new ProjectUser();
		$res = $model->getTeam(array(ProjectChecker::MANAGER));
		return $res;
	}
	
	protected function _getTeam()
	{
		return $this->_getManagers();
	}
	
	protected function _getWhere($params, $date_field = 'created')
	{
		$team_ids = $this->_getTeamIds();
		if (! $team_ids)
		{
			return parent::_getWhere($params, $date_field);
		}
		$where = parent::_getWhere($params, $date_field);
		$where['clause'] = $where['clause'] . ' and owner_id IN (' . implode(',', $team_ids) . ')';
		return $where;
	}
	
}