<?php 

class Stat4Abstract extends StatAbstract
{
	protected $_user_id = null;
	
	protected $_daily = false;
	
	public function getTeam()
	{
		$team = $this->_getTeam();
		$res = array();
		foreach ($team as $item) {
			$res[$item['id']] = $item['full_name'];
		}
		if (ProjectChecker::allowed('get-own-stat'))
		{
			return false;
		}
		return $res;
	}
	
	/**
	 * Определяет выручку по менеджеру(ам)
	 * @param unknown $params
	 */
	protected function _getOrderItemPrice($params, $date = 'date_paid', $koeff = 1)
	{
		$where = $this->_getWhere($params, $date);
		$cmd = Yii::app()->db->createCommand()
			->select("(sum(price))*$koeff as gain")
			->from('order o')
			->where($where['clause'], $where['params']);
		$cmd = $this->_prepareCmd($cmd, $date);
		$res = $cmd->queryAll();
		return $this->_prepareResult($res);
	}
	
	/**
	 * Количество пришедших заявок
	 * @param unknown $params
	 * @param string $date
	 */
	protected function _getOrderCount($params, $date = 'created')
	{
		$where = $this->_getWhere($params, $date);
		$cmd = Yii::app()->db->createCommand()
			->select('count(1) as count')
			->from('order o')
			->where($where['clause'], $where['params'])
			;
		$cmd = $this->_prepareCmd($cmd, $date);
		$res = $cmd->queryAll();
		return $this->_prepareResult($res);
	}
	
	protected function _getTeam()
	{
		
	}
	
	protected function _getTeamIds()
	{
		$res = array();
		$team = $this->_getTeam();
		if (! $team)
			return $res;
		
		foreach ($team as $one)
		{
			$res[] = $one['id'];
		}
		
		return $res;
	}

	
	/**
	 * Функция вычисляет отношение либо скалярной величины (в случае конкретного пользователя),
	 * либо вектонрной величины(по каждому пользователю отдельно)
	 * @param unknown $val1
	 * @param unknown $val2
	 */
	protected function _getRatio($val1, $val2, $mult = 1)
	{
		$user_id = $this->_user_id;
		if ($user_id)
		{
			if (! $val2)
				return 0;
			return $val1 / $val2 * $mult;
		}
		else
		{
			$res = array();
			$managers = $this->_getTeam();
			foreach ($managers as $manager) {
				$manager_id = $manager['id'];
				$v1 = (isset($val1[$manager_id])) ? $val1[$manager_id] : 0;
				$v2 = (isset($val2[$manager_id])) ? $val2[$manager_id] : 9223372036854775807;
				if (! $v2) 
					$res[$manager_id] = 0;
				else
					$res[$manager_id] = $v1 / $v2 * $mult;
			}
			return $res;
		}
	}
	
	protected function _getSum($val1, $val2)
	{
		$user_id = $this->_user_id;
		if ($user_id)
		{
			return $val1 + $val2;
		}
		else
		{
			$res = array();
			$managers = $this->_getTeam();
			foreach ($managers as $manager) {
				$manager_id = $manager['id'];
				$v1 = (isset($val1[$manager_id])) ? $val1[$manager_id] : 0;
				$v2 = (isset($val2[$manager_id])) ? $val2[$manager_id] : 0;
				$res[$manager_id] = $v1 + $v2;
			}
			return $res;
		}
	}
	
	protected function _getMinus($val1, $val2)
	{
		$user_id = $this->_user_id;
		if ($user_id)
		{
			return $val1 - $val2;
		}
		else
		{
			$res = array();
			$managers = $this->_getTeam();
			foreach ($managers as $manager) {
				$manager_id = $manager['id'];
				$v1 = (isset($val1[$manager_id])) ? $val1[$manager_id] : 0;
				$v2 = (isset($val2[$manager_id])) ? $val2[$manager_id] : 0;
				$res[$manager_id] = $v1 - $v2;
			}
			return $res;
		}
	}
	
	protected function _prepareCmd($cmd, $date)
	{
		$select = array();
		$group = array();
		$order = array();
	
		if (! $this->_user_id)
		{
			$select[] = 'owner_id';
			$group[] = 'owner_id';
			$order[] = 'owner_id';
		}
	
		if ($this->_daily)
		{
			$select[] = 'DATE($date) as date';
			$group[] = 'date';
			$order[] = 'date';
		}
	
		foreach (array('select', 'order', 'group') as $param)
		{
			if (! count($$param))
			{
				continue;
			}
			$$param = implode(',', $$param);
			$method = 'get' . ucfirst($param);
				
			$$param = ($cmd->$method()) ? $cmd->$method() . ',' . $$param : $$param;
			$cmd->$param($$param);
		}
		
		if ($this->_user_id)
		{
			$cmd->andWhere('owner_id=:owner_id', array(':owner_id' => $this->_user_id));
		}
		
		return $cmd;
	}
	
	protected function _prepareResult($result)
	{
		$team = $this->getTeam();
		$res = array();
		foreach ($team as $key => $one)
		{
			$res[$key] = 0;
		}

		if (! $result)
			return $res;
		
		if (! isset($result[0]['owner_id']))
		{
			return (int)array_shift($result[0]);
		}
		
		foreach ($result as $item)
		{
			$owner_id = $item['owner_id'];
			unset($item['owner_id']);
			$res[$owner_id] = (int)array_shift($item);
		}
		
		return $res;
	}
	
	/**
	 * Установка ИД пользователя, если он не установлен, запрашивается статистика по всем менеджерам
	 * @param unknown $id
	 * @return Stat4Manager
	 */
	public function setUserId($id)
	{
		$this->_user_id = $id;
		return $this;
	}
}