<?php 

class Stat4 extends StatAbstract 
{
	protected $_role_id = null;
	
	public function __construct()
	{
		$info = ProjectChecker::getUserInfo();
		$role_id = $info['role_id'];
		$this->_role_id = $role_id;
	}
	
	public function getStat($params)
	{
		return array(
			'commercial' => $this->_getCommercial($params),
			'managers' => $this->_getManagerStat($params),
			'speakers' => $this->_getSpeakerStat($params),
		);
	}
	
	protected function _getCommercial($params)
	{
		if (! ProjectChecker::allowed('get-allstat'))
		{
			return array();
		}
		$model = new Stat4Commercial();
		return $model->get($params);
	}
	
	protected function _getSpeakerStat($params)
	{
		if (! ProjectChecker::allowed('get-allstat'))
		{
			return array();
		}
		$model = new Stat4Speakers();
		return $model->get($params);
	}
	
	protected function _getManagerStat($params)
	{
		$info = ProjectChecker::getUserInfo();
		$role_id = $info['role_id'];
		
		if (! ProjectChecker::allowed('get-allstat') && $role_id != ProjectChecker::MANAGER)
		{
			return array();
		}
		$model = new Stat4Manager();
		if (ProjectChecker::allowed('get-own-stat'))
		{
			$user_id = Yii::app()->user->getId();
			$model->setUserId($user_id);
		}
		return $model->get($params);
	}
	
	public function getManagersInfo()
	{
		$res = array();
		$params = array();
		$params['from'] = date('Y-m-d');
		$params['to'] = date('Y-m-d');
		$user_id = Yii::app()->user->getId();
		$params['owner_id'] = $user_id;
		$model = new Stat4Manager();
		$model->setUserId($user_id);
		$res['daily'] = $model->get($params);
	
		$params['from'] = date('Y-m').'-01';
		$res['month'] = $model->get($params);
	
		$model->setUserId(null);
		unset($params['owner_id']);
		$managers_unfo = $model->get($params);
	
		$team = $model->getTeam();
		$managers = array();
		foreach ($team as $key => $one)
		{
			$managers[$key] = array();
		}
	
		foreach ($managers_unfo as $key1 => $item)
		{
			if (! $item)
				continue;
	
			foreach ($item as $key2 => $value)
			{
				$managers[$key2][$key1] = $value;
			}
		}
		unset($managers[$user_id]);
		$res['managers'] = $managers;
	
		return $res;
	}
	
}