<?php 

class ProjectSms 
{
	public function send($phone, $text)
	{
		$service = ProjectService::getSmsService();
		$service->setPhone($phone);
		$service->setMessage($text);
		$service->send();
	}
}