<?php 
class ProjectSettings
{
	
	public static function get($namespace)
	{
		$project_id = OrderModule::$project_id;
		if ( $namespace == 'main' )
		{
			$model = Landing::model()->findByAttributes(array('project_id' => $project_id));
			return ($model) ? $model->getAttributes() : array();
		}
		
		$res = Yii::app()->db->createCommand()
				->select('value')
				->from ('project_settings')
				->where ('project_id=:project_id and namespace=:namespace',
						 array(':project_id' => $project_id, ':namespace' => $namespace) 
						)
				->queryScalar();
		
		return ($res) ? unserialize($res) : array();
    }
    
	public static function set($namespace,$params)
	{
		if ( $namespace == 'main' )
		{
			return;
		}
		$project_id = OrderModule::$project_id;
		if (self::get($namespace))
		{
			Yii::app()->db->createCommand()
				->update('project_settings', 
					array('value' => serialize($params)) , 
					'project_id=:project_id and namespace=:namespace', 
					array(':project_id'=>$project_id, ':namespace' => $namespace)
				);
		} 
		else 
		{
			Yii::app()->db->createCommand()
				->insert('project_settings', 
					array('project_id' => $project_id, 
						  'namespace' => $namespace, 
						  'value' => serialize($params))
				);
		}
    }
    
    public function getAll()
    {
    	$project_id = OrderModule::$project_id;
    	$res = Yii::app()->db->createCommand()
    		->select('namespace,value')
    		->from ('project_settings')
    		->where ('project_id=:project_id',array(':project_id' => $project_id))
    		->queryAll();
    	
    	$res_new = array();
    	
    	foreach ($res as $item)
    	{
    		$res_new[] = array('namespace' => $item['namespace'], 'data' => unserialize($item['value']));
    	}
    	
    	$res = $res_new;
    	
    	return $res;
    }
    
}