<?php 

class ProjectChecker 
{
	const SUPER_USER = 0;
	
	const MANAGER = 1;
	
	const VISOR = 2;
	
	protected static $_user_info = null;
	
	protected static $_resources = array(
		'delete-order' => array('access_level' => array(self::SUPER_USER)),
		'get-settings' => array('access_level' => array(self::SUPER_USER)),
		'delete-stuff' => array('access_level' => array(self::SUPER_USER)),
		'get-allstat' => array('access_level' => array(self::SUPER_USER)),
		'get-projectsettings' => array('access_level' => array(self::SUPER_USER)),
		'save-projectsettings' => array('access_level' => array(self::SUPER_USER)),
		'edit-order' => array('access_level' => array(self::SUPER_USER, self::MANAGER, self::VISOR)),
		'edit-order-all' => array('access_level' => array(self::SUPER_USER, self::VISOR)),
		'only-own-orders' => array('access_level' => array(self::MANAGER)),
	);
	
	public static function check($resource, $params=array())
	{
		if (! self::allowed($resource)) {
			throw new CHttpException('400', 'Permissions denied');
		}
	} 
	
	public static function allowed($resource, $params=array())
	{
		if (Yii::app()->controller->module->id != 'project')
			return false;
			
		$user_info = self::_checkUserInfo();
		$role_id = $user_info['role_id'];
		
		$allowed = false;
		
		if (isset( self::$_resources[$resource]['access_level']))
		{
			$access_level = self::$_resources[$resource]['access_level'];
			if (! is_array($access_level) && $access_level == '*')
				$allowed = true;
			$allowed = in_array($role_id, self::$_resources[$resource]['access_level']);
		}
		
		if (isset( self::$_resources[$resource]['biz_rule']))
		{
			$biz_rule = self::$_resources[$resource]['biz_rule'];
			$class_name = $biz_rule['class'];
			$class = new $class_name();
			$method = $biz_rule['method'];
			$param_names = $biz_rule['params'];
			$args = array();
			foreach ($param_names as $name)
			{
				$args[$name] = $params[$name];
			}
			$allowed = call_user_func_array(array(&$class, $method), $args);
		}
		return $allowed;
	}
	
	public static function initUserInfo()
	{
		$model = new ProjectUser();
		self::$_user_info =  $model->getUserProjectInfo();
		return self::$_user_info;
	}
	
	protected static function _checkUserInfo()
	{
		if (! self::$_user_info)
		{
			throw new CHttpException('400', 'Uninitialized user info');
		}
		return self::$_user_info;
	}
	
	public static function getUserInfo()
	{
		return self::_checkUserInfo();
	}
}