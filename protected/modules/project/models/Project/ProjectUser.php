<?php 

class ProjectUser 
{
	protected $_user_id = null;
	protected $_project_id = null;
	
	public function __construct()
	{
		$this->_user_id = Yii::app()->user->getId();
	}
	
	public function getDefaultProjectId()
	{
		$project_id = Yii::app()->db->createCommand()
			->select('project_id')
			->from('user_role_project')
			->where('user_id=:id and banned=0', array(':id'=>$this->_user_id))
			->queryScalar();
		return $project_id;
	}
	
	public function getProjectsWithUser()
	{
		$rows = Yii::app()->db->createCommand()
			->select('project_id')
			->from('user_role_project')
			->where('user_id=:id', array(':id'=>$this->_user_id))
			->queryAll();
		
		$projects = array();
		if ($rows)
		{
			foreach ($rows as $row)
			{
				$projects[] = $row['project_id'];
			}
		}
		return $projects;
	}
	
	public function getProjectsList()
	{
		$data = Yii::app()->db->createCommand()
			->select('p.id, p.label')
			->from('project p')
			->join('user_role_project up', 'p.id=up.project_id')
			->where('up.user_id=:user_id and up.banned=0', array(':user_id'=>$this->_user_id))
			->queryAll();
		return $data;
	}
	
	public static function getProjectsListVector()
	{
		$model = new ProjectUser();
		$data = $model->getProjectsList();
		
		$res = array();
		foreach ($data as $item)
		{
			$res[$item['id']] = $item['label'];
		}
		return $res;
	}
	
	public function getTeam($role_id = null)
	{
		$project_id = OrderModule::$project_id;
		if ($project_id) 
		{
			$this->_project_id = OrderModule::$project_id;
			$projects = array($project_id);
		}
		else 
		{
			$projects = $this->getProjectsWithUser();
		}
		$projects = implode(',', $projects);
		
		if (! is_array($role_id))
		{
			$role = ($role_id) ? " and up.role_id=$role_id" : '';
		}
		else 
		{
			$role = " and up.role_id in (" . implode(',', $role_id) . ") ";
		}
		
		$team = Yii::app()->db->createCommand()
			->selectDistinct('u.id, u.full_name')
			->from('user_role_project up')
			->leftJoin('user u','u.id=up.user_id')
			->where("up.project_id IN ($projects) and u.hidden=0 and u.banned=0 $role")
			->order('u.id')
			->queryAll();
		return $team;
	}
	
	public function getTeamVector()
	{
		$team = $this->getTeam();
		$res = array();
		foreach ($team as $item)
		{
			$res[$item['id']] = $item['full_name'];
		}
		return $res;
	}
	
	public function getUserListForAutocomplete($key)
	{
		$users = Yii::app()->db->createCommand()
			->select("id, nick_name, CONCAT(full_name, ' ' ,nick_name) as label")
			->from('user')
			->order('id')
			->where("nick_name like :key and banned=0", array(':key' => "$key%"))
			->queryAll();
		
		foreach ($users as $key => $user)
		{
			$users[$key]['value'] = $user['id'];
		}
		
		return $users;
	}
	
	public function isUserInProject($user_id)
	{
		$this->_project_id = OrderModule::$project_id;
		$res = Yii::app()->db->createCommand()
			->select('id')
			->from('user_role_project')
			->where("project_id=:project_id and user_id=:user_id and banned=0", array(':project_id' => $this->_project_id, ':user_id' => $user_id))
			->order('id')
			->queryScalar();
		return $res;
	}
	
	public function addUserToProject($user_id, $project_id = null, $owner = false)
	{
		$this->_project_id = ($project_id) ? $project_id : OrderModule::$project_id;
		$res = Yii::app()->db->createCommand()->insert('user_role_project', array(
			'project_id' => $this->_project_id,
			'user_id' => $user_id,
			'banned' => 0,
			'role_id' => ($owner) ? ProjectChecker::SUPER_USER : ProjectChecker::MANAGER,
		));
		return $res;
	}
	
	public function excludeUserFromProject($user_id)
	{
		$this->_project_id = OrderModule::$project_id;
		$res = Yii::app()->db->createCommand()
			->delete(
				'user_role_project',
				"project_id=:project_id and user_id=:user_id",
				array(':project_id' => $this->_project_id, ':user_id' => $user_id));
		return $res;
	}
	
	public function getUserProjectInfo()
	{
		$this->_project_id = OrderModule::$project_id;
		$res = Yii::app()->db->createCommand()
			->select('role_id')
			->from('user_role_project')
			->where('project_id=:project_id and user_id=:user_id and banned=0', array(':project_id'=>$this->_project_id, ':user_id'=>$this->_user_id))
			->queryRow();
		return $res;
	}
	
	public function checkProjectByUser($project_id) {
		$res = Yii::app()->db->createCommand()
			->select('p.id')
			->from('project p')
			->join('user_role_project up', 'p.id=up.project_id')
			->where('p.id=:id and up.user_id=:user_id and up.banned=0', array(':id'=>$project_id, ':user_id'=>$this->_user_id))
			->queryScalar();
		return $res;
	}
	
	
}