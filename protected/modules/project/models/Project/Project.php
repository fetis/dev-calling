<?php

class Project extends CActiveRecord
{
	public function tableName()
	{
		return 'project';
	}

	public function rules()
	{
		return array(
			array('active, owner_id, stat_type', 'numerical', 'integerOnly'=>true),
			array('label, referer', 'length', 'max'=>255),
			array('token', 'length', 'max'=>32),
			array('description', 'safe'),
			array('id, label, description, active, owner_id, referer, token, stat_type', 'safe', 'on'=>'search'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'label' => 'Label',
			'description' => 'Description',
			'active' => 'Active',
			'owner_id' => 'Owner',
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public static function checkByUser($project_id) 
	{
		$model = new ProjectUser();
		return $model->checkProjectByUser($project_id);
	}
	
	public function getDefault() 
	{
		$model = new ProjectUser();
		return $model->getDefaultProjectId();
	}
	
	public function beforeSave()
	{
		$user_id = Yii::app()->user->getId();
		$this->owner_id = $user_id;
		return parent::beforeSave();
	}
	
	public function fillDefault()
	{
		SalaryAbstract::fillDefault($this->id);
	}
	
}
