<?php 

class ProjectService {
	
	public static function getSmsService()
	{
		$setting = new ProjectSettings();
		$sms_settings = $setting->get('sms');
		$type = (isset($sms_settings['type'])) ? $sms_settings['type'] : null;
		$service = ServiceSmsAbstract::factory($type);
		return $service;
	}
	
}