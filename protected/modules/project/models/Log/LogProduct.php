<?php 

class LogProduct extends LogAbstract 
{
	
	public static function log($model, $action, $result=0, $desc='')
	{
		$user_id = self::_getUserId();
		Yii::app()->db->createCommand()->insert("item_log", array(
			'project_id' => $model->project_id,
			//'order_id' => $model->order_id,
			'action' => $action,
			'user_id' => $user_id,
			'description' => $desc,
			'serialized' => serialize($model->getAttributes()),
			'result' => $result,
		));
		parent::_smsError();
	}
	
}