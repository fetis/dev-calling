<?php 

class LogProject extends LogAbstract 
{
	
	public static function log($model, $action, $level='INFO', $desc='')
	{
		$user_id = self::_getUserId();
		Yii::app()->db->createCommand()->insert("project_log", array(
			'project_id' => OrderModule::$project_id,
			'action' => $action,
			'user_id' => $user_id,
			'description' => $desc,
			'serialized' => '',
		));
		parent::_smsError();
	}
	
}