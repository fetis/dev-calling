<?php 

class LogOrderState extends LogAbstract 
{
	
	public static function log($model, $state_id_old, $auto = 0)
	{
		$user_id = self::_getUserId();
		Yii::app()->db->createCommand()->insert("order_state_log", array(
			'project_id' => $model->project_id,
			'order_id' => $model->id,
			'user_id' => $user_id,
			'old' => $state_id_old,
			'new' => $model->state_id,
			'date' => date('Y-m-d H:i:s'),
			'auto' => $auto,
			'delivery_type' => $model->service,
		));
	}
	
}