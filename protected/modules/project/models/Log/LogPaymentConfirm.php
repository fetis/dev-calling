<?php 

class LogPaymentConfirm
{
	public static function log($type, $order_id, $message, $state = "")
	{
		Yii::app()->db->createCommand()->insert("payment_confirm_log", array(
			'type' => $type,
			'message' => $message,
			'state' => $state,
			'order_id' => $order_id,
		));
	}
}