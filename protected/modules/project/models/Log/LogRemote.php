<?php 

class LogRemote extends LogAbstract 
{
	
	public static function log($token, $referer, $params, $action, $level='INFO')
	{
		Yii::app()->db->createCommand()->insert("remote_log", array(
			'project_id' => OrderModule::$project_id,
			'action' => $action,
			'token' => $token,
			'referer' => $referer,
			'serialized' => serialize($params),
		));
		parent::_smsError();
	}
	
}