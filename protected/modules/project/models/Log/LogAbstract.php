<?php 

class LogAbstract 
{
	
	protected static function _smsError()
	{
		$id = Yii::app()->db->getLastInsertId();
		new LogSmsInformer($id . ' ' . get_called_class() . ' Error');
	}
	
	protected static function _getUserId()
	{
		if (Yii::app()->hasComponent('user')) {
			return Yii::app()->user->getId();
		}
		else 
		{
			return null;
		}
	}
}