<?php 

class LogAccess extends LogAbstract 
{
	
	public static function log($action, $result=0)
	{
		$user_id = self::_getUserId();
		Yii::app()->db->createCommand()->insert("access_log", array(
			'action' => $action,
			'user_id' => $user_id,
		));
		parent::_smsError();
	}
	
}