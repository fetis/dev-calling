<?php 

class LogService extends LogAbstract 
{
	
	public static function log($service_name, $account, $action, $result, $url, $level = "INFO")
	{
		$user_id = self::_getUserId();
		
		try {
		
			Yii::app()->db->createCommand()->insert("service_log", array(
				'name' => $service_name,
				'account' => $account,
				'action' => $action,
				'user_id' => $user_id,
				'result' => $result,
				'level' => $level,
				'url' => $url,
			));
		
		}
		catch (Exception $e)
		{
			echo $e->getMessage();
		}
		
		parent::_smsError();
	}
	
}