<?php 

class LpAuth {
	
	const ERROR_NONE=0;
	const ERROR_TOKEN_INVALID=1;
	const ERROR_REFERER_INVALID=2;
	const ERROR_PROJECT_INVALID=3;
	const ERROR_ORDER_INVALID=4;
	
	protected $_token;
	protected $_referer;
	public $errorCode;
	
	public function __construct($token, $referer)
	{
		$this->_referer = $referer;
		$this->_token = $token;
	}
	
	public function isAuth()
	{
		if (($user = Project::model()->findByAttributes(array('token' => $this->_token))) === null) {
			$this->errorCode = self::ERROR_TOKEN_INVALID;
			return false;
		}
		$project_id = OrderModule::$project_id;
		if ($user->id != $project_id)
		{
			$this->errorCode = self::ERROR_PROJECT_INVALID;
		}
		$this->errorCode == self::ERROR_NONE;
		return ($this->errorCode == self::ERROR_NONE);
	}
	
	public function save($params)
	{
		$model = new Order();
		$project_id = OrderModule::$project_id;
		
		$params['project_id'] = $project_id;
		
		if (isset($params['item_id']) && $params['item_id'])
		{
			$item = Product::model()->findByPk($params['item_id']);
			$params['price'] = $item->price;
		}
		
		$model->setAttributes($params);
		
		$model->site_fio = $model->fio;
		$model->site_phone = $model->phone;
		$model->site_email = $model->email;
		
		if (!$model->save(false))
		{
			throw new CException('Cannot save order');
		}
		
		$binder = new OrderBinder();
		$binder->bindOwnClient($model);
		//Сохранить данные по проекту
		return $model->id;
	}
	
	/*
	public function paid($params)
	{
		if (! isset($params['id']))
			return self::ERROR_ORDER_INVALID;
		
		$id = $params['id'];
		$model = Order::findByPk($id);
		
		if (! $model)
			return self::ERROR_ORDER_INVALID;
		
		$model->state_id = OrderState::STATE_PAID;
		$model->save();
		
		return self::ERROR_NONE;
	}
	*/
}