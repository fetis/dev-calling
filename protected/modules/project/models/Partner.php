<?php

class Partner extends CActiveRecord
{
	public function tableName()
	{
		return 'partner';
	}

	public function rules()
	{
		return array(
			array('project_id, conferention_id, speaker_id, type', 'numerical', 'integerOnly'=>true),
			array('email, name', 'length', 'max'=>100),
			array('date', 'safe'),
			array('id, project_id, conferention_id, speaker_id, date, email, name, type', 'safe', 'on'=>'search'),
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
