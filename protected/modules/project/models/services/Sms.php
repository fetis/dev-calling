<?php 

include '../sms/smspilot/Class.php';

class Sms extends ServiceAbstract
{
	
	private $_client;
	
	private $_apikey;
	
	private $_phones = array();
	
	private $_message;
	
	public function _init()
	{
		$this->_client = new SMSPilot($this->_apikey);
	}
	
	public function setPhone($phone)
	{
		$phone = $this->_filter($phone);
		if ($this->_validate($phone))
			$this->_phones[] = $phone;
		return $this;
	}
	
	public function setPhones($phones)
	{
		if (! is_array($phones) || ! count($phones))
			return false;
		
		$this->_phones[] = array();
		
		foreach ($phones as $phone)
		{
			$this->setPhone($phone);
		}
		
		return $this;
	}
	
	protected function _validate($phone)
	{
		return true;
	}
	
	protected function _filter($phone)
	{
		return $phone;
	}
	
	public function setMessage($value)
	{
		$this->_message = $value;
		return $this;
	}
	
	public function send()
	{
		$phones = $this->_phones;
		if (! is_array($phones) || ! count($phones))
			return false;
		
		$message = $this->_message;
		
		foreach ($phones as $phone)
		{
			$this->_client->send($phone, $message);
		}
		
		return true;
	}
	
} 