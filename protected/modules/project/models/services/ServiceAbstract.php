<?php 
class ServiceAbstract extends CComponent 
{
	protected $_uri_base;
	
	protected $_namespace;
	
	protected $_required = array();
	
	public function __construct() 
	{
		$model = new ProjectSettings();
		$settings = $model->get($this->_namespace);
		$required = array_fill_keys($this->_required,'');
		if (is_array($settings) && count($settings))
		{
			foreach ($settings as $key=>$setting)		
			{
				$attrib = "_" . $key;
				$this->$attrib = $setting;
				if (isset($required[$key]))
				{
					unset($required[$key]);
				}
			}	
		}
		if (count($required))
		{
			LogProject::log($settings, 'initService', 'ERR', 'No required params: ' . implode(',', $required));
			throw new CHttpException(404,"Params are not defined: " . implode(',', array_keys($required)));
		}
		$this->_init();
	}
	
	protected function _init() {}
	
}