<?php 

class ServiceSms48 extends ServiceSmsAbstract 
{
	private $_client;
	
	private $_login;
	
	private $_password;
	
	private $_from;
	
	public function __construct()
	{
		$setting = new ProjectSettings();
		$sms_settings = $setting->get('sms');
		$this->_login = $sms_settings['login'];
		$this->_password = $sms_settings['password'];
		$this->_from = $sms_settings['from'];
	}
	
	public function send()
	{
		$phones = $this->_phones;
		if (! is_array($phones) || ! count($phones))
			return false;
	
		$message = $this->_message;
	
		foreach ($phones as $phone)
		{
			$this->_send($phone, $message);
		}
	
		return true;
	}
	
	protected function _send($phone, $message){
		if (! $phone || ! $message)
			return;
		
		$login = $this->_login; //Ваш логин на этом сайте
		$password = $this->_password;   //Ваш пароль на этом сайте
		$from = $this->_from;   //подпись (от кого)
		$msg = urlencode(iconv('utf-8','windows-1251',$message));
		$dlr = urlencode('http://balker.su/index.php?id=123456&type=%d');
		$checksumm = md5($login.md5($password).$phone); //Контрольная сумма
		$res = @file_get_contents("http://sms48.ru/send_sms.php?login=$login&to=$phone&msg=$msg&from=$from&check2=$checksumm&dlr_url=$dlr");
	}
	
}