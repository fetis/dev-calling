<?php 

class ServiceSmsPilot extends ServiceSmsAbstract 
{
	private $_client;
	
	public function __construct()
	{
		$setting = new ProjectSettings();
		$sms_settings = $setting->get('sms');
		$apikey = $sms_settings['api_key'];
		$this->_client = new SMSPilot($apikey);
	}
	
}