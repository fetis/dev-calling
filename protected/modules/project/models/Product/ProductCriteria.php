<?php 
class ProductCriteria
{
	public function get($filter, $search, $sort)
	{
		$criteria = new CDbCriteria();
		
		if ($filter)
		{
			$conferention_id = $filter['conferention'];
			if ($conferention_id != 'all')
			{
				$criteria->addCondition("conferention_id=$conferention_id");
			}
			
			if (isset($filter['speaker']))
			{
				$speaker_id = $filter['speaker'];
				if ($speaker_id != 'all')
				{
					$criteria->addCondition("speaker_id=$speaker_id");
				}
			}
		}
		return $criteria;
	}
}