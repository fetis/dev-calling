<?php

class Product extends PActiveRecord
{
	
	public function tableName()
	{
		return 'item';
	}

	public function rules()
	{
		return array(
			array('active, project_id, speaker_id, conferention_id', 'numerical', 'integerOnly'=>true),
			array('price', 'numerical'),
			array('label', 'length', 'max'=>255),
			array('min_label', 'length', 'max'=>20),
			array('label, price, active, project_id, min_label, speaker_id, conferention_id, product_url, product_page, email_text', 'safe'),
			array('id, label, price, active, project_id, min_label, speaker_id, conferention_id, product_url, product_page, email_text', 'safe', 'on'=>'search'),
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function beforeSave()
	{
		$speaker_id = $this->speaker_id;
		$speaker = Speaker::model()->findByPk($speaker_id);
		$this->conferention_id = $speaker->conferention_id;
		return parent::beforeSave();
	}
	
	public function afterSave()
	{
		parent::afterSave();
		LogProduct::log($this, ($this->isNewRecord) ? 'insert' : 'update');
	}
	
	public static function getList($fiels = 'label')
	{
		$rows = self::model()->findAll();
		$res = array();
		foreach ($rows as $row) 
		{
			$res[$row->id] = $row->$fiels;
		}
		return $res;
	}
	
	public function getCriteria($filter, $search=null, $sort=null)
	{
		$criteria = new ProductCriteria();
		return $criteria->get($filter, $search, $sort);
	}
	
}
