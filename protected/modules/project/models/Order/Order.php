<?php

class Order extends PActiveRecord
{
	public $state_id = 0;
	
	public function tableName()
	{
		return 'order';
	}

	public function rules()
	{
		return array(
			array('owner_id', 'required'),
			array('project_id, active, owner_id, state_id, discount, discount_type, timestamp, ab_number, color, speaker_id, conferention_id, item_id', 'numerical', 'integerOnly'=>true),
			array('fio, email', 'length', 'max'=>255),
			array('phone, source', 'length', 'max'=>20),
			array('phone','filter','filter'=>array($obj=new PhoneFilter(),'filter')),
			array('created, comment, price, date_recall, date_cancel, date_spam, date_paid, site_email, site_fio, site_phone, client_date, service_pay, user_paid, notify_date', 'safe'),
			array('id, project_id, fio, phone, active, created, owner_id, state_id, comment, email, discount, discount_type, timestamp, source, ab_number, color, site_email, site_fio, site_phone, client_date, service_pay, date_recall, user_paid, notify_date', 'safe', 'on'=>'search'),
		);
	}
	
	public function relations()
	{
		return array(
			'item'=>array(self::BELONGS_TO, 'Product', 'item_id'),
		);
	}
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function getCriteria($filter, $search, $sort = 'created')
	{
		$criteria = new OrdersCriteria();
		return $criteria->get($filter, $search, $sort);
	}
	
	/**
	 * Псевдоудаление заказов
	 */
	public function remove()
	{
		$this->active = 0;
		$this->save();
	}
	
	public function afterSave()
	{
		parent::afterSave();
		$notify = OrderNotifyAbstract::factory('create', $this);
		$notify->run();
		LogOrder::log($this, ($this->isNewRecord) ? 'insert' : 'update');
	}
	
	public function beforeSave()
	{
		if ($this->isNewRecord)
		{
			$this->_initDefaultValues();
		}
		else 
		{
			if (! $this->owner_id && $this->state_id != 0) {
				$user_id = Yii::app()->user->getId();
				$this->owner_id = $user_id;
			}	
			
			if (! $this->date_paid && $this->state_id == OrderState::STATE_PAID) {
				$this->getConfirmation()->confirm();
			}
			
			$autodates = OrderStatic::getAutoDates();
			foreach ($autodates as $autodate => $state)
			{
				if (! $this->$autodate && $this->state_id == $state) {
					$this->$autodate = date('Y-m-d H:i:s');
				}
			}
		}
		
		if (! $this->date_recall)
			$this->date_recall = null;
		
		return parent::beforeSave();
	}
	
	/**
	 * Подготовка данных при вставке новой записи
	 */
	protected function _initDefaultValues()
	{
		$user_id = Yii::app()->user->getId();
		$this->owner_id = $user_id;
		
		if (! $this->created)
			$this->created = date('Y-m-d H:i:s');
		if (! $this->date_recall)
			$this->date_recall = null;
		if (! $this->discount)
			$this->discount = 0;
		
		$item_id = $this->item_id;
		$item = Product::model()->findByPk($item_id);
		$this->conferention_id = $item->conferention_id;
		$this->speaker_id = $item->speaker_id;
	}
	
	/**
	 * Автоматическая привязка исполнителя к заказу при первом открытии заказа им
	 */
	public function bind()
	{
		$model = new OrderBinder();
		return $model->bind($this);
	}
	
	public function getSameOrders()
	{
		$email = $this->email;
		$phone = $this->phone;
		$id = $this->id;
		$res = array();
		
		$cmd = Yii::app()->db->createCommand()
			->select('o.id, o.state_id, created, os.label as state, s.name as speaker, c.name as conferention, i.label as product')
			->from('order o')
			->leftJoin('conferention c','o.conferention_id=c.id')
			->leftJoin('speaker s','o.speaker_id=s.id')
			->leftJoin('item i','o.item_id=i.id')
			->leftJoin('order_state os','o.state_id=os.id')
			->leftJoin('user u','o.owner_id=u.id')
			->where("o.active=1 and (o.email=:email or o.phone=:phone) and o.id!=:id", array(':email' => $email, ':phone' => $phone, ':id' => $id))
		;
		$res = $cmd->queryAll();
		return $res;
	}
	
	public function getInfoForPayment()
	{
		$formatter = new CDateFormatter('ru');
		$item = Product::model()->findByPk($this->item_id);
		return array(
			'date' => $formatter->format('dd.MM.yyyy', $this->created),
			'email' => $this->email,
			'price' => $this->price,
			'item' => $item->label,
		);
	}
	
	public function getConfirmation()
	{
		return OrderNotifyAbstract::factory('confirm', $this);
	}
}
