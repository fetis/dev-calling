<?php 

class OrderLocker
{
	protected function _getFields()
	{
		return array(
			ProjectChecker::MANAGER => 'owner_id',
		);
	}
	
	protected function _getStates()
	{
		return array(
			ProjectChecker::MANAGER => '0',
		);
	}

	/**
	 * Проверка - заблокирован ли заказ на взятие
	 * @param unknown $oid
	 */
	public function checkLocked($oid)
	{
		$user_id = Yii::app()->user->getId();
		$info = ProjectChecker::getUserInfo();
		$role_id = $info['role_id'];
		
		$states = $this->_getStates();
		$state = $states[$role_id];
		
		$fields = $this->_getFields();
		$field = $fields[$role_id];

		$model = Order::model()->findByPk($oid);
		if ($model->$field && $model->$field != $user_id)
		{
			throw new CHttpException('400', 'Данный заказ уже находится в обработке.');
		}
	}
}