<?php 

class OrderPayLink {
	
	protected $_model = null;
	
	protected $_message = null;
	
	protected static $_pay_page = "http://melius7.com/pay?id=";
	
	public function __construct($model)
	{
		$this->_model = $model;
	}
	
	public function send($model)
	{
		$message = $this->_formMessage();
		$this->_sendEmail($message);
		$this->_sendSms($message);
		return true;
	}
	
	public static function getPayPage()
	{
		return self::$_pay_page;
	}
	
	public function getPayLink()
	{
		return self::$_pay_page . $this->_model->id;
	}
	
	protected function _formMessage()
	{
		$item = Product::model()->resetScope()->findByPk($this->_model->item_id);
		$text = "";
		$text .= "Уважаемый " . $this->_model->fio . ". ";
		$text .= "Оплатить заказ N" . $this->_model->id . ", " . $item->label . " Вы можете по ссылке: " . $this->getPayLink();
		return $text;
	}
	
	protected function _sendSms($text)
	{
		$sms_service = ProjectService::getSmsService();
		$sms_service->setPhone($this->_model->phone);
		$sms_service->setMessage($text);
		$res = $sms_service->send();
	}
	
	protected function _sendEmail($text)
	{
		$message = new YiiMailMessage;
		$message->setBody($text, 'text/html');
		$message->subject = 'Закак N' . $this->_model->id;
		$message->addTo($this->_model->email);
		$message->from = Yii::app()->params['adminEmail'];
		Yii::app()->mail->send($message);
	}
	
}