<?php 

class OrderNotifyConfirmation extends OrderNotifyConfirmation
{
	protected function _formMessage()
	{
		return 'Заказ №' . $this->_model->id . '. "' . $item->label . '"'; 
	}
	
	protected function _formSubject()
	{
		$model = $this->_model;
		$item = $this->_item;
		$pay_link = new OrderPayLink($model);
	
		$text = "";
		$text .= "Здравствуйте, " . $model->fio . ".<br/>";
		$text .= "Вы только что оплатили заказ №" . $model->id . " «" . $item->label . "».<br/><br/>";
		$text .= $item->email_text;
		return $text;
	}
}