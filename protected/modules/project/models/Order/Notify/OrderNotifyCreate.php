<?php 

class OrderNotifyCreate extends OrderNotifyAbstract
{
	protected function _formSubject()
	{
		$model = $this->_model;
		return $model->fio . ", Ваш счёт для оплаты заказа №" . $model->id;
	}
	
	protected function _formMessage()
	{
		$model = $this->_model;
		$item = $this->_item;
		$pay_link = new OrderPayLink($model);
		
		return 
			$model->fio . ", Вы создали заказ №" . $model->id . '<br/>' .
			'<br/>' .
			'Название:<br/>' .
			$item->label  . '<br/>' .
			'<br/>' .
			'Сумма для оплаты: ' . $model->price . ' руб.<br/>' .
			'<br/>' .
			'Чтобы выбрать способ оплаты, нажмите здесь: <br/>' .
			$pay_link->getPayLink() . '<br/>' .
			'<br/>' .
			'Если Вы оплачиваете наличными, пришлите фотографию чека на Емайл: astroschool7@gmail.com' .
			'<br/>' .
			'P.S. Через 5 дней заказ будет аннулирован!' . 
			'<br/>'
		;
	}
	
}