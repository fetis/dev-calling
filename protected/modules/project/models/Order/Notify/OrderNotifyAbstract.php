<?php 

abstract class OrderNotifyAbstract
{
	protected $_model = null;
	
	protected $_item = null;
	
	public static function factory($type, $model)
	{
		switch ($type)
		{
			case 'create' 	: return new OrderNotifyCreate($model);
			case 'payment' 	: return new OrderNotifyPayment($model);
			case 'confirm' 	: return new OrderNotifyConfirmation($model);
		}
	}
	
	public function __construct($model)
	{
		$this->_model = $model;
		$this->_item = Product::model()->resetScope()->findByPk($model->item_id);
	}
	
	public function run()
	{
		$text = $this->_formMessage();
		$subject = $this->_formSubject();
		$this->_sendEmail($subject, $text);
	}
	
	abstract protected function _formMessage();
	
	abstract protected function _formSubject();
	
	protected function _sendEmail($subject, $text)
	{
		$item = $this->_item;
		$model = $this->_model;
		$mail=Yii::app()->Smtpmail;
		$mail->SetFrom($mail->Username, 'Melius7');
		$mail->Subject = $subject;
		$mail->MsgHTML($text);
		$mail->AddAddress($model->email, "");
		if(!$mail->Send()) {
			Yii::log("Mailer Error " . $model->id, CLogger::LEVEL_WARNING, 'app.payment');
		}else {
			Yii::log("Message sent! " . $model->id, CLogger::LEVEL_WARNING, 'app.payment');
		}
	}
}