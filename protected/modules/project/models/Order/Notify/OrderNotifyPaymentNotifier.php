<?php 

class OrderNotifyPaymentNotifier
{
	protected function _getNotifyList()
	{
		$criteria = new CDbCriteria();
		$criteria->addCondition("notify_count>0");
		$criteria->addCondition("state_id in (0,1)");
		$criteria->addCondition("active=1");
		$criteria->addCondition("notify_date > DATE_SUB(NOW(), INTERVAL 1 DAY)");
		return Order::model()->resetScope()->findAll($criteria);
	}
	
	protected function _confirmNotify($model)
	{
		$model->notify_count -= $model->notify_count;
		$model->notify_date = date('Y-m-d H:i:s');
		$model->save();
	}
	
	public function run()
	{
		$list = $this->_getNotifyList();
		foreach ($list as $item)
		{
			$notify = OrderNotifyAbstract::factory('payment', $item);
			$notify->run();
			$this->_confirmNotify($item);
		}
	}
}