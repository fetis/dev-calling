<?php 

class OrderBinder 
{
	/**
	 * Определяет статус который автоматически присваивается пользователю при обработке заказа
	 * @return multitype:multitype:number string
	 */
	protected function _getStates()
	{
		return array(
			ProjectChecker::MANAGER => array( 'state_id' => 0, 'field' => 'owner_id'),
		);
	}
	
	/**
	 * Автоматическая привязка исполнителя к заказу при первом открытии заказа им
	 */
	public function bind($model)
	{
		$states = $this->_getStates();
		$info = ProjectChecker::getUserInfo();
		$role_id = $info['role_id'];
	
		if (isset($states[$role_id]))
		{
			$states = $states[$role_id];
			$field = $states['field'];
			$user_id = Yii::app()->user->getId();
			if (ProjectChecker::allowed('only-own-orders'))
			{

				if ($model->state_id == $states['state_id'] && (! $model->$field || $model->$field == 0))
				{
					$model->$field = $user_id;
					$model->state_id = OrderState::STATE_PROCESSED;
					$model->save();
					$this->bindEmptyOrders($model, $field);
				}
				
				if ($model->$field != $user_id)
				{
					return array('res' => false);
				}
			}
		}
		return array('res' => true);
	}
	
	/**
	 * Привязка вновь поступившей заявки менеджеру, 
	 * к которому клиент был привязан ранее
	 * @param unknown $model
	 */
	public function bindOwnClient($model)
	{
		$email = $model->email;
		$criteria = new CDbCriteria();
		$criteria->addCondition("email=:email OR phone=:phone");
		$criteria->params = array(':email' => $model->email, ':phone' => $model->phone);
		$order = Order::model()->find($criteria);
		
		if ($order)
		{
			$model->owner_id = $order->owner_id;
			$model->save();
		}
		
	}
	
	/**
	 * В случае поступления нескольких заявок от одного человека, 
	 * который не был привязан к менеджерам, 
	 * выполняет привязку остальных заявок к тому же менеджеру, 
	 * к которому была привязана его первая заявка из уже существующих
	 * @param unknown $model
	 * @param unknown $field
	 * @return boolean
	 */
	public function bindEmptyOrders($model, $field)
	{
		$criteria = new CDbCriteria();
		$criteria->addCondition("email=:email OR phone=:phone");
		$criteria->addCondition("owner_id=0 OR owner_id is NULL");
		$criteria->addCondition("state_id=0");
		$criteria->params = array(':email' => $model->email, ':phone' => $model->phone);
		$orders = Order::model()->findAll($criteria);
		
		if (! $orders)
			return false;
		
		$user_id = Yii::app()->user->getId();
		foreach ($orders as $order)
		{
			$order->$field = $user_id;
			$order->state_id = OrderState::STATE_PROCESSED;
			$order->save();
		}
	}
}