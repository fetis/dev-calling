<?php 
class OrdersCriteria
{
	public function get($filter, $search, $sort)
	{
		$criteria = new CDbCriteria();
		$user_id = Yii::app()->user->getId();
		if ($search)
		{
			$criteria->addCondition("phone LIKE :param1 OR t.id=:param OR LOWER(fio) LIKE LOWER(:param1) OR t.email=:param");
			$criteria->params[':param'] = "$search%";
			$criteria->params[':param1'] = "%$search%";
		}
		else 
		{
			$field = "created";
			if (isset($filter['state_id']))
			{
				$state_id = $filter['state_id'];
				if ($state_id != 'all')
				{
					$criteria->addCondition("state_id=$state_id");
					$autodates = array_flip(OrderStatic::getAutoDates());
					$field = $autodates[$state_id];
				}
			}
			
			if (isset($filter['date_type']))
			{
				$filter_key = $filter['date_type'];
				switch ($filter_key) {
			
					case "weekly" 	:
						$criteria->addCondition("DATE($field) > DATE_SUB(CURDATE(), INTERVAL 1 WEEK)");
						break;
			
					case "monthly"	:
						$criteria->addCondition("DATE($field) > DATE_SUB(CURDATE(), INTERVAL 1 MONTH)");
						break;
			
					case "custom" 	:
						$from = ($filter['date_type_from']) ? $filter['date_type_from'] : '0000-00-00 00:00:00';
						$to = ($filter['date_type_to']) ? $filter['date_type_to'] : '9999-00-00 00:00:00';
						$criteria->addBetweenCondition("$field", $from, $to);
						break;
			
					case "tomorrow" :
						$criteria->addCondition("DATE($field) > DATE_SUB(CURDATE(), INTERVAL 2 DAY)");
						break;
							
					default /*daily*/:
						$criteria->addCondition("DATE($field) = DATE(NOW())");
				}
			}
			$criteria->order = $sort;
		}
		
		if (ProjectChecker::allowed('only-own-orders'))
		{
			$criteria->addCondition("state_id=0 and (owner_id is NULL or owner_id=0)  or owner_id=$user_id");
			$criteria->addCondition("'" . date('Y-m-d H:i:s') . "' > DATE_ADD(created, INTERVAL 1 HOUR)");
		}
		
		$criteria->with=array(
			'item' => array("select" => "product_url")
		);
	
		return $criteria;
	}
	
}