<?php

class OrderStatic
{
	
	public static function getParamsToProjectOrderList()
	{
		return array(
			'id', 
			'project_id', 
			'fio', 
			'phone',
			'active', 
			'created',
			'owner_id', 
			'state_id', 
			'comment', 
			'email',
			'discount', 
			'discount_type',
			'timestamp',
			'source',
			'ab_number',
			'color',
			'conferention_id',
			'speaker_id',
			'item_id',
			'discount',
			'discount_type',
			'price',
			'site_fio',
			'site_phone',
			'site_email',
			'date_recall',
			'client_date',
		);
	}
	
	public static function getStates()
	{
		$model = new OrderState();
		return $model->getStates();
	}
	
	public static function getAutoDates()
	{
		$model = new OrderState();
		return $model->getAutoDates();
	}
	
	public static function getAllNearStates() {
		$model = new OrderState();
		return $model->getNearStates();
	}
	
	public static function getLockedStates()
	{
		return array(
			ProjectChecker::MANAGER => array('state_id' => 0, 'field' => 'owner_id'),
		);
	}
	
	public static function getStatesForStat()
	{
		$model = new OrderState();
		return $model->getStatesForStat();
	}
	/*
	public static function getLocked()
	{
		$user_id = Yii::app()->user->getId();
		$locked_states = self::getLockedStates();
		$res = array();
		
		if (isset($locked_states[CheckerGlobal::getRoleId()]))
		{
			$locked_state = $locked_states[CheckerGlobal::getRoleId()]['state_id'];
			$field = $locked_states[CheckerGlobal::getRoleId()]['field'];
			$rows = Yii::app()->db->createCommand()
				->select('id')
				->from('order')
				->where("state_id=:state_id and $field=:user_id", array(':state_id'=>$locked_state, ':user_id'=>$user_id,))
				->queryAll();
			if (! $rows)
				return $res;
			
			foreach ($rows as $item)
			{
				$res[] = $item['id'];
			}
		}
		
		return $res;
	}
	*/
}
