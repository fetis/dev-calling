<?php 

class OrderConfirmation {
	
	protected $_model = null;
	
	public function __construct($model)
	{
		$this->_model = $model;
	}
	
	public function confirm()
	{
		$message = $this->_formMessage();
		$this->_sendEmail($message);
		//$this->_sendSms($message);
	}
	
	protected function _formMessage()
	{
		$item = Product::model()->resetScope()->findByPk($this->_model->item_id);
		$text = "";
		$text .= "Здравствуйте, " . $this->_model->fio . ".<br/>";
		$text .= "Вы только что оплатили заказ №" . $this->_model->id . " «" . $item->label . "».<br/><br/>";
		$text .= $item->email_text;
		return $text;
	}
	
	protected function _sendSms($text)
	{
		$sms_service = ProjectService::getSmsService();
		$sms_service->setPhone($this->_model->phone);
		$sms_service->setMessage($text);
		$res = $sms_service->send();
	}
	
	protected function _sendEmail($text)
	{
		$item = Product::model()->resetScope()->findByPk($this->_model->item_id);
		$mail=Yii::app()->Smtpmail;
        $mail->SetFrom('info@mail.onlinekonf.ru', 'Melius7');
        $mail->Subject = 'Заказ №' . $this->_model->id . '. "' . $item->label . '"';
        $mail->MsgHTML($text);
        $mail->AddAddress($this->_model->email, "");
        if(!$mail->Send()) {
			Yii::log("Mailer Error " . $this->_model->id, CLogger::LEVEL_WARNING, 'app.payment');
        }else {
			Yii::log("Message sent! " . $this->_model->id, CLogger::LEVEL_WARNING, 'app.payment');
        }
	}
}