<?php 

class OrderState {
	
	const STATE_PAID=2;
	
	const STATE_PROCESSED=1;
	
	const STATE_PAID_ERROR=6;
	/**
	 * Возвращает список всех возможных статусов для проекта
	 */
	public function getStates()
	{
		$project_id = OrderModule::$project_id;
		$rows = Yii::app()->db->createCommand()
			->select("id, label")
			->from("order_state")
			->order("weight")
			->queryAll();
		return $this->_prepareResult($rows);
	}
	
	/**
	 * Возвращает список соответствий дат смененам статусов
	 */
	public function getAutoDates()
	{
		$rows = Yii::app()->db->createCommand()
			->select("id, date_field")
			->from("order_state")
			->queryAll();
		$res = $this->_prepareResult($rows, "date_field");
		return array_flip ($res);
	}
	
	/**
	 * Возвращает списиок соседних состояний для каждого статуса
	 */
	public function getNearStates()
	{
		$res = array();
		$states = $this->getStates();
		$project_id = OrderModule::$project_id;
		$project = Project::model()->findByPk($project_id);
		
		foreach ($states as $key => $state)
		{
			$res[$key] = array($key);
		}
		
		$cmd = Yii::app()->db->createCommand()
			->select("*")
			->from("order_state_arc")
			->where("active=1")
			;
		
		$user_info = ProjectChecker::getUserInfo();
		$role_id = $user_info['role_id'];
		
		//Проверка допустимых переходов между состояниями для каждой роли
		$alloweds = Yii::app()->db->createCommand()
			->select("id")
			->from("order_state_role")
			->where("active=1 and role_id=:role_id", array(':role_id' => $role_id))
			->queryScalar();
		
		if ($alloweds)
		{
			$cmd->andWhere("id IN (select arc_id from order_state_role where role_id=:role_id)", array(':role_id' => $role_id));
		}
			
		$rows = $cmd->queryAll();
		
		foreach ($rows as $row)
		{
			$res[$row['from']][] = $row['to'];
		}
		$res[""] = array(0);
		
		return $res;
	}
	
	/**
	 * Возвращает список статусов для вывода статистики
	 */
	public function getStatesForStat()
	{
		$project_id = OrderModule::$project_id;
		$rows = Yii::app()->db->createCommand()
			->select("id, label")
			->from("order_state")
			->where("exclude_stat=0")
			->order("weight")
			->queryAll();
		return $this->_prepareResult($rows);
	}
	
	protected function _prepareResult($rows, $field = 'label')
	{
		$res = array();
		
		if (! $rows)
			return $res;
		
		foreach ($rows as $row)
		{
			$res[$row['id']] = $row[$field];
		}
		
		return $res;
	}
	
	public function moveState($model, $new)
	{
		$near_states = $this->getNearStates();
		$from = $model->state_id;
		if (isset($near_states[$from]) && in_array($new, $near_states[$from]))
		{
			if ($new == 6)
			{
				$model = new Order();
				$model->duplicate(array(
						'state_id' => 9,
						'owner_id' => null,
						'duplicated' => 1,
				));
			}
			$old = $model->state_id;
			$model->state_id = $new;
			$model->save();
			LogOrderState::log($model, $old);
			return true;
		}
		throw new CHttpException('Error while changing state');
		
	}
	
}