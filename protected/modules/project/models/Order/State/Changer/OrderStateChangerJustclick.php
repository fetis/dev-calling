<?php 

class OrderStateChangerJustclick
{
	public function run($res)
	{
		foreach ($res as $item)
		{
			if ($this->_checkPaid($item))
			{
				$item->autoupdate_state=1;
				$item->autoupdated = date('Y-m-d H:i:s');
				$item->state_id = OrderState::STATE_PAID;
				$item->save();
			}
		}
	}
	
	protected function _checkPaid($item)
	{
		return true;
	}
}