<?php 

class OrderStateChanger {
	
	public static $variants = array('justclick');
	
	public static function factory($type)
	{
		switch ($type) {
			case 'justclick' : return new OrderStateChangerJustclick();
		}
	}
	
	public function run() 
	{
		$variants = StateChangerAbstract::$variants;
		$projects = Project::model()->findAllByAttributes(array('autostate_change' => 1, 'active' => 1));
		
		if (! $projects)
			return false;
		
		$project_ids = array();
		foreach ($projects as $project) 
			$project_ids[] = $project->id;
		
		$criteria=new CDbCriteria;
		$criteria->addInCondition('project_id', $project_ids);
		$criteria->addInCondition('state_id', array(1));
		$criteria->addCondition('active=1');
		$criteria->order = "autoupdated, id";
			
		$model = new Order();
		$model->setDbCriteria($criteria);
		$res = $model->findAll();
		
		if (! $res)
			return;
		
		foreach ($variants as $variant) {
			$state_changer = OrderStateChanger::factory($variant);
			$state_changer->run($res);
		}
		
	}
	
}