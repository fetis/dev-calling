<?php 

class State 
{
	
	public static function getMessages()
	{
		$res = array();
		$res[] = self::_checkProducts();
		return array_filter($res);
	}
	
	protected static function _checkProducts()
	{
		return (! Product::model()->find()) 
			? array("type" => "danger", "message" => "Для проекта не заполнены товары")
			: null;
	}
	
}