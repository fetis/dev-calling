<?php 

class PayConfirmW1 extends PayConfirmAbstract
{
	protected static $_key = 'w1';
	
	protected $_password = 'UmlNWlI5XDM1MXo1dFZ2R1JPZTBER0RZWHdg';
	
	public function run()
	{
		$pass = $this->_password;
		
		$order_id = $_POST["WMI_PAYMENT_NO"];
		$model = $this->getOrderById($order_id);
		
		if (!isset($_POST["WMI_PAYMENT_AMOUNT"]))
		{
			LogPaymentConfirm::log(get_class($this), $order_id, "Отсутствует параметр WMI_PAYMENT_AMOUNT", "FALSE");
			return false;
		}
		
		$summ = $_POST["WMI_PAYMENT_AMOUNT"];
		if (! $this->_checkSum($model, $summ))
		{
			LogPaymentConfirm::log(get_class($this), $order_id, "Incorrect sum:" . $summ, "FALSE");
			return false;
		}
		
		if (!isset($_POST["WMI_SIGNATURE"]))
		{
			$this->_printAnswer("Retry", "Отсутствует параметр WMI_SIGNATURE");
			LogPaymentConfirm::log(get_class($this), $order_id, "Отсутствует параметр WMI_SIGNATURE", "FALSE");
			return false;
		}
		
		if (!isset($_POST["WMI_PAYMENT_NO"]))
		{
			$this->_printAnswer("Retry", "Отсутствует параметр WMI_PAYMENT_NO");
			LogPaymentConfirm::log(get_class($this), $order_id, "Отсутствует параметр WMI_PAYMENT_NO", "FALSE");
			return false;
		}
		
		if (!isset($_POST["WMI_ORDER_STATE"]))
		{
			$this->_printAnswer("Retry", "Отсутствует параметр WMI_ORDER_STATE");
			LogPaymentConfirm::log(get_class($this), $order_id, "Отсутствует параметр WMI_ORDER_STATE", "FALSE");
			return false;
		}
		
		if (strtoupper($_POST["WMI_ORDER_STATE"]) != "ACCEPTED")
		{
			$this->_printAnswer("Ok", "Заказ #" . $_POST["WMI_PAYMENT_NO"] . " оплачен!");
			LogPaymentConfirm::log(get_class($this), $order_id, "WMI_ORDER_STATE is not ACCEPTED", "FALSE");
			return false;
		}
		
		// Извлечение всех параметров POST-запроса, кроме WMI_SIGNATURE
		foreach($_POST as $name => $value)
		{
			if ($name !== "WMI_SIGNATURE") $params[$name] = $value;
		}
		
		// Сортировка массива по именам ключей в порядке возрастания
		// и формирование сообщения, путем объединения значений формы
		uksort($params, "strcasecmp"); $values = "";
		
		foreach($params as $name => $value)
		{
			//Конвертация из текущей кодировки (UTF-8)
			//необходима только если кодировка магазина отлична от Windows-1251
			//$value = iconv("utf-8", "windows-1251", $value);
			$values .= $value;
		}
		
		// Формирование подписи для сравнения ее с параметром WMI_SIGNATURE
		$signature = base64_encode(pack("H*", md5($values . $pass)));
		
		if ($signature == $_POST["WMI_SIGNATURE"])
		{
			print "WMI_RESULT=OK";
			$this->paid($model);
			return true;
		}
		else
		{
			$this->_printAnswer("Retry", "Неверная подпись " . $_POST["WMI_SIGNATURE"]);
			LogPaymentConfirm::log(get_class($this), $order_id, "Incorrect crc: " . $summ, "FALSE");
			return false;
		}
		$this->_printAnswer("Retry", "Неверное состояние ". $_POST["WMI_ORDER_STATE"]);
	}
	
	protected function _printAnswer($result, $description)
	{
		print "WMI_RESULT=" . strtoupper($result) . "&";
		print "WMI_DESCRIPTION=" .urlencode($description);
		exit();
	}
	
	
}