<?php 

abstract class PayConfirmAbstract {
	
	protected static $_key = null;
	
	public static function factory($type)
	{
		switch ($type) {
			case 'robocassa' : return new PayConfirmRobocassa();
			case 'w1' 		 : return new PayConfirmW1();
			case 'paypal' : return new PayConfirmPaypal();
		}
		throw new CHttpException(404,'');
	}
	
	abstract public function run();
	
	protected function _checkSum($model, $summ)
	{
		if ($summ < $model->price)
		{
			$this->_saveState($model, OrderState::STATE_PAID_ERROR);
			return false;
		}
		return true;
	}
	
	protected function getOrderById($id)
	{
		$order = Order::model()->resetScope()->findByPk($id);
		OrderModule::$project_id = $order->project_id;
		return $order;
	}
	
	public function paid($model, $user_paid = false)
	{
		ob_start();
		if ($model->state_id != OrderState::STATE_PAID)
		{
			$this->_saveState($model, OrderState::STATE_PAID, $user_paid);
			$model->getConfirmation()->confirm();
			Yii::log("SUCCESSFULL CONFIRM " . $model->id . ' ' . static::$_key, CLogger::LEVEL_WARNING, 'app.payment');
			LogPaymentConfirm::log(get_called_class(), $order_id, "Successfull payment", "TRUE");
		}
		ob_end_clean();
	}
	
	protected function _saveState($model, $state_id, $user_paid)
	{
		$model->state_id = $state_id;
		$model->service_pay = static::$_key;
		if ($user_paid)
		{
			$model->owner_id = 0;
		}
		$model->save();
		return $model;
	}
	
}