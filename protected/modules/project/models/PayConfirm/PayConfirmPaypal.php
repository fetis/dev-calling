<?php 

class PayConfirmPaypal extends PayConfirmAbstract
{
	protected static $_key = 'paypal';
	
	protected $_password = '';
	
	public function run()
	{
		$req = "cmd=_notify-validate";
		foreach($_POST as $key=>$val)
		{
			$req.= "&".$key."=".urlencode($val);
		}
		$summ   = $_POST['mc_gross'];
		$order_id = $_POST["invoice"];
		$model = $this->getOrderById($order_id);
		if (! $this->_checkSum($model, $summ))
		{
			return false;
		}
		$header = "POST http://www.sandbox.paypal.com/cgi-bin/webscr HTTP/1.0\r\n";
		$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
		$header .= "Content-Length: " . strlen ($req) . "\r\n\r\n";
		
		$fp = fsockopen ("www.paypal.com", 80, $errno, $errstr, 30);
		if (!$fp)
		{
			echo "$errstr ($errno)";
			return;
		}
		fputs ($fp, $header . $req);
		$res="";
		while (!feof($fp))
			$res .= fgets ($fp, 1024);
		fclose ($fp);
		if (strpos($res, "VERIFIED")===FALSE)
		{
			return;
		}
		if ($_POST["payment_status"]!="Completed")
		{
			if ($_POST["payment_status"]=="Pending" )
			{
				LogPaymentConfirm::log(get_class($this), $order_id, "ERROR - payment status is not Completed - $_POST[payment_status] | $_POST[pending_reason]", "FALSE");
				return;
			}
			LogPaymentConfirm::log(get_class($this), $order_id, "ERROR - payment status is not Completed - $_POST[payment_status] | $_POST[pending_reason]", "FALSE");
			return;
		}
		Yii::log("PAYPAL SUCCESS", CLogger::LEVEL_WARNING, 'app.payment');
		LogPaymentConfirm::log(get_class($this), $order_id, "SUCCESS", "TRUE");
		$this->paid($model);
	}
}