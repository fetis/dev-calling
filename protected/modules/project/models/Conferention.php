<?php

class Conferention extends PActiveRecord
{

	public function tableName()
	{
		return 'conferention';
	}

	public function rules()
	{
		return array(
			array('id, project_id, active', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
			array('id, name, project_id, active', 'safe', 'on'=>'search'),
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
