<?php

class CallRecords extends CActiveRecord
{

	public function tableName()
	{
		return 'call_records';
	}

	public function rules()
	{
		return array(

			array('from', 'numerical', 'integerOnly'=>true),
			array('filepath', 'length', 'max'=>100),
            array('to', 'length', 'max'=>50),
			array('date_record, date_insert', 'safe'),
			array('id, to,  from , date_record, filepath, date_insert', 'safe', 'on'=>'search'),
		);
	}


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
