<?php

class CallEmail
{
    const hostname = '{imap.mail.ru:993/imap/ssl}INBOX';
    const username = 'foreven@inbox.ru';
    const password = 'ien56voe89';

    public function run()
    {
        $array_data = array();
        $inbox = $this->connectImap();
        $emails = imap_search($inbox, 'ALL');

        if ($emails) {

            rsort($emails);
           foreach($emails as $keys=>$email)
           {
                $overview = imap_fetch_overview($inbox, $email, 0);

                $structure = imap_fetchstructure($inbox, $email);
                $attachments = array();
                if (isset($structure->parts) && count($structure->parts)) {

                    for ($i = 0; $i < count($structure->parts); $i++) {

                        $attachments[$i] = array(
                            'is_attachment' => false,
                            'filename' => '',
                            'name' => '',
                            'attachment' => ''
                        );

                        if ($structure->parts[$i]->ifdparameters) {
                            foreach ($structure->parts[$i]->dparameters as $object) {
                                if (strtolower($object->attribute) == 'filename') {
                                    $attachments[$i]['is_attachment'] = true;
                                    $attachments[$i]['filename'] = $object->value;
                                }
                            }
                        }

                        if ($structure->parts[$i]->ifparameters) {
                            foreach ($structure->parts[$i]->parameters as $object) {
                                if (strtolower($object->attribute) == 'name') {
                                    $attachments[$i]['is_attachment'] = true;
                                    $attachments[$i]['name'] = $object->value;
                                }
                            }
                        }

                        if ($attachments[$i]['is_attachment']) {
                            $attachments[$i]['attachment'] = imap_fetchbody($inbox, $email, $i + 1);
                            if ($structure->parts[$i]->encoding == 3) { // 3 = BASE64
                                $attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
                            } elseif ($structure->parts[$i]->encoding == 4) { // 4 = QUOTED-PRINTABLE
                                $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
                            }
                        }
                    }
                }

                foreach ($attachments as $key => $name_attach) {
                    $name = $name_attach['name'];
                    $part2 = strstr(substr(strstr($name, "."), 2), "-");
                    $to = substr(strrchr($overview[0]->subject, 'to'), 3); //куда звонили
                    $from = substr(strrchr($overview[0]->subject, 'from'), 5, 3); //кто звонил
                    $name_file = $to . $part2;
                    $timestamp_record = substr($name, 6, 10);
                    $date_record = date('Y-m-d H:i:s', $timestamp_record); //дата звонка
                    $contents = $name_attach['attachment'];

                }
                $month_day = substr($part2, 10, 5);
                $day = substr($month_day, 3, 2);
                $month = substr($month_day, 0, 2);
                $filepath = "../files/" . $month . "/" . $day . "/" . $name_file; //путь и название файла.
                if (is_dir("../files/" . $month . "/" . $day . "/")) // Если такой каталог есть
                {
                    file_put_contents($filepath, $contents);
                } else {
                    mkdir("../files" . "/" . $month . "/" . $day, 0777, true);
                    file_put_contents($filepath, $contents);
                }

                array_push($array_data, array('to' => $to, 'from' => $from, 'date_record' => $date_record, 'filepath' => $filepath, 'date_insert' => date("Y-m-d H:i:s")));

                $move = imap_mail_move($inbox, $email, 'Архив');
            }
        }
        imap_close($inbox);

        return $array_data;
    }

    protected function connectImap()
    {

        $inbox = imap_open(self::hostname, self::username, self::password) or die('Cannot connect to Mail.ru: ' . imap_last_error());
        return $inbox;
    }

    public function getCallList($phone_manager,$phone_user){


        $user = Yii::app()->db->createCommand()
            ->select('*')
            ->from('call_records')
            ->where('`from`=:from and `to`=:to',array(':from'=>$phone_manager,':to'=>$phone_user))
            ->order('date_record')
            ->queryAll();
        //здесь будет приходить id_manager(т.е значение from) и его надо вставить в where
        return $user;
    }

}
