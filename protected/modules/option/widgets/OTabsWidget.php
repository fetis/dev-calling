<?php 

class OTabsWidget extends CWidget {
	
	public $tabs = array();
	
	public $type = 'horizontal'; //vertical also
	
	protected $_views = array('horizontal' => 'oTabsWidgetHorizontal', 'vertical' => 'oTabsWidgetVertical');
	
	public function run() 
	{
		$tabs = $this->tabs;
		$tabs_new = array();
		foreach ($tabs as $key => $tab)
		{
			if (! $this->_checkTab($tab))
			{
				continue;
			}
			if (! isset($tab['active']))
			{
				$tab['active'] = false;
			}
			
			if (! isset($tab['css']))
			{
				$tab['css'] = '';
			}
			
			if (! isset($tab['params']))
			{
				$tab['params'] = array();
			}
			$tabs_new[] = $tab;
		}
		$tabs = $tabs_new;
		if (count($tabs))
		{
			$this->render($this->_getView(), array('tabs' => $tabs));
		}
	}
	
	protected function _checkTab($tab)
	{
		$class = $tab['class'];
		return $class::check();
	}
	
	protected function _getView() 
	{
		$views = $this->_views;
		$type = $this->type;
		return (isset($views[$type])) 
				? $views[$type] 
				: array_shift($views);
	}
	
}