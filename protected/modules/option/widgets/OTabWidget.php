<?php 

class OTabWidget extends CWidget {
	
	protected static $_project_check = null;
	
	protected static $_global_check = null;
	
	protected static $_option_id = null;
	
	protected static $_alias = '';
	
	protected static $_head = '';
	
	public function run() 
	{
	}
	
	public static function check()
	{
		if (! ProjectOption::check(static::$_option_id)) 
		{
			return false;
		}

		if (static::$_project_check && ! ProjectChecker::allowed(static::$_project_check)) 
		{
			return false;
		}
		
		return true;
	}
	
	public static function getAlias() {
		return static::$_alias;
	}
	
	public static function getHead() {
		return static::$_head;
	}
	
}