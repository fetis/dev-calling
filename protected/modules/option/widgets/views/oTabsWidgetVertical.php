<div class="row">

	<div class="col-md-3">
	
		<ul class="list-group">
			<?php foreach ($tabs as $tab):?>
			<li class="list-group-item <?php echo ($tab['active']) ? 'active' : ''?> <?php echo $tab['css']?>">
				<?php $class = $tab['class']?>
				<a data-toggle="tab" href="#<?php echo $class::getAlias()?>"><?php echo $class::getHead()?></a>
			</li>
			<?php endforeach?>
		</ul>

	</div>
				
	<div class="col-md-9">
	
		<div class="tab-content">
		<?php foreach ($tabs as $tab):?>
			<?php $class = $tab['class']?>
			<div class="tab-pane <?php echo ($tab['active']) ? 'active' : ''?>" id="<?php echo $class::getAlias()?>">
				<?php $this->widget($class, $tab['params'])?>
			</div>
		<?php endforeach?>
		</div>
		
	</div>
	
</div>