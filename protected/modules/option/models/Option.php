<?php
class Option extends CActiveRecord
{
	public function tableName()
	{
		return 'option';
	}

	public function rules()
	{
		return array(
			array('price, active', 'numerical', 'integerOnly'=>true),
			array('label', 'length', 'max'=>64),
			array('description', 'length', 'max'=>255),
			array('alias', 'length', 'max'=>20),
			array('id, label, price, active, description, alias', 'safe', 'on'=>'search'),
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
