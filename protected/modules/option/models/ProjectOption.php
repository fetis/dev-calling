<?php

class ProjectOption extends CActiveRecord
{
	protected static $_options = null;
	
	public function tableName()
	{
		return 'project_option';
	}

	public function rules()
	{
		return array(
			array('project_id, option_id, blocked', 'numerical', 'integerOnly'=>true),
			array('date_expire', 'safe'),
			array('id, project_id, option_id, date_expire, blocked', 'safe', 'on'=>'search'),
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public static function initOptions()
	{
		if (! self::$_options)
		{
			$project_id = OrderModule::$project_id;
			$criteria = new CDbCriteria();
			$criteria->addColumnCondition(array('project_id' => $project_id));
			$criteria->addColumnCondition(array('blocked' => 0));
			$criteria->addCondition('DATE(NOW()) < date_expire');
			$options = ProjectOption::model()->findAll($criteria);
			$res = array();
			if ($options)
			{
				foreach ($options as $option)
				{
					$res[] = $option->option_id;
				}
			}
			self::$_options = $res;
		}
		return self::$_options;
	}
	
	public static function check($option_id)
	{
		$options = self::initOptions();
		return ($option_id && ! in_array($option_id, $options)) ? false : true;
	}
	
}
