<?php 
class WebUser extends CWebUser
{
	
	const SUPER_USER = 0;

	const MANAGER_PLUS_STAT = 2;
	
	const MANAGER = 1;
	
	public function createUser(RegistrationForm $form)
	{
		$transaction = Yii::app()->db->beginTransaction();
	
		try
		{
			$user = new User;
			$data = $form->getAttributes();

			unset($data['cPassword'], $data['verifyCode']);
			
			$data = CMap::mergeArray($data, array(
					'hidden' => 0, 
					'banned' => 0, 
					'access_level' => 1, 
					'created' => date('Y-m-d H:i:s'), 
					'salt' => md5(time()),
					'last_visit' => date('Y-m-d H:i:s'), 
			));
			$user->setAttributes($data);
			$user->password = CPasswordHelper::hashPassword($form->password);
			
			if($user->save()) {
				$transaction->commit();
				return $user;
			}
			
			throw new CException(Yii::t('UserModule.user','Error creating account!'));
		}
		catch(Exception $e)
		{
			throw $e;
			$transaction->rollback();
	
			return false;
		}
	}
	
}