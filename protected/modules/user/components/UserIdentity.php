<?php

class UserIdentity extends CUserIdentity
{
	private $_id;
	
	public function authenticate()
	{
		if (($user = User::model()->findByAttributes(array('email' => $this->username, 'banned' => 0))) === null) {
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		} else if (!$user->validatePassword($this->password)) {
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
		} else {
			// запись данных в сессию пользователя
			$this->_id      = $user->id;
			$this->username = $user->nick_name;
		
			$this->setState('id', $user->id);
			$this->setState('access_level', $user->access_level);
			$this->setState('nick_name', $user->nick_name);
			$this->setState('email', $user->email);
			$this->setState('loginTime', time());
		
			// зафиксируем время входа
			$user->last_visit = new CDbExpression('NOW()');
			$user->update(array('last_visit'));
		
			$this->errorCode = self::ERROR_NONE;
		}
		return $this->errorCode == self::ERROR_NONE;
	}
	
	public function getId()
	{
		return $this->_id;
	}
}