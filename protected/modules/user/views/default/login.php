<?php
$this->pageTitle=Yii::app()->name . ' - Login';
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'htmlOptions'=>array(
		'class' => "form-login form-horizontal form-white",
		'role' => "form",
	),
)); ?>

	<h1>Вход в систему</h1>
	<hr>
	
	<div class="form-group">
		<?php echo $form->labelEx($model,'username', array('class' => 'col-sm-3 control-label')); ?>
		<div class="col-sm-9">
		  <?php echo $form->textField($model,'username', array("type"=>"email", "class"=>"form-control", "placeholder"=>"Email")); ?>
		  <?php echo $form->error($model,'username'); ?>
		</div>
	</div>
	
	<div class="form-group">
		<?php echo $form->labelEx($model,'password', array('class' => 'col-sm-3 control-label')); ?>
		<div class="col-sm-9">
		  <?php echo $form->passwordField($model,'password', array("class"=>"form-control", "placeholder"=>"Password")); ?>
		  <?php echo $form->error($model,'password'); ?>
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-3 col-sm-9">
		  <button type="submit" class="btn btn-default">Войти</button>
		</div>
	</div>

	<hr>

<?php $this->endWidget(); ?>