<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array('id' => 'registration-form','htmlOptions'=>array(
		'class' => "form-registration form-horizontal form-white",
		'role' => "form",
	))); ?>
	
	
	<h1>Рагистрация</h1>
	<hr>

    <?php echo $form->errorSummary($model); ?>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'nick_name', array('class' => 'col-sm-4 control-label')); ?>
        <div class="col-sm-8">
        	<?php echo $form->textField($model, 'nick_name', array("type"=>"text", "class"=>"form-control", "placeholder"=>"")); ?>
        </div>
    </div>
    
    <div class="form-group">
        <?php echo $form->labelEx($model, 'full_name', array('class' => 'col-sm-4 control-label')); ?>
        <div class="col-sm-8">
        	<?php echo $form->textField($model, 'full_name', array("type"=>"text", "class"=>"form-control", "placeholder"=>"")); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'email', array('class' => 'col-sm-4 control-label')); ?>
        <div class="col-sm-8">
        	<?php echo $form->textField($model, 'email', array("type"=>"email", "class"=>"form-control", "placeholder"=>"")); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'password', array('class' => 'col-sm-4 control-label')); ?>
        <div class="col-sm-8">
        	<?php echo $form->passwordField($model, 'password', array("type"=>"password", "class"=>"form-control", "placeholder"=>"")); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'cPassword', array('class' => 'col-sm-4 control-label')); ?>
        <div class="col-sm-8">
        	<?php echo $form->passwordField($model, 'cPassword', array("type"=>"password", "class"=>"form-control", "placeholder"=>"")); ?>
        </div>
    </div>

    <?php if (Yii::app()->getModule('user')->showCaptcha): ?>
        <?php if (CCaptcha::checkRequirements('gd')): ?>
            <div class="form-group">
                <?php echo $form->labelEx($model, 'verifyCode', array('class' => 'col-sm-4 control-label')); ?>
                <div class="col-sm-8">
                    <?php $this->widget('CCaptcha', array('showRefreshButton' => false)); ?>
                    <?php echo $form->textField($model, 'verifyCode'); ?>
                    <?php echo $form->error($model, 'verifyCode'); ?>
                </div>
                <div class="hint">
                    <?php echo Yii::t('UserModule.user', 'Please enter the text from the image'); ?>
                </div>
            </div>
        <?php endif; ?>
    <?php endif; ?>

    <div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
		  <button type="submit" class="btn btn-default">Зарегистрироваться</button>
		</div>
	</div>

	<hr>
	
    <?php $this->endWidget(); ?>
</div><!-- form -->
