<?php

class DefaultController extends Controller
{
	
	public $layout='//layouts/login';
	
	public function actions()
	{
		return array(
				'captcha'=>array(
						'class'=>'CCaptchaAction',
						'backColor'=>0xFFFFFF,
				),
		);
	}
	
	public function actionLogin()
	{
		if (Yii::app()->user->getId()) {
			Yii::app()->request->redirect('/site/index/');
		}
		
		$model=new LoginForm;

		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			
			if($model->validate() && $model->login()) 
			{
				$default_project_id = Yii::app()->getModule('project')->getDefaultProject();
				if ($default_project_id)
				{
					$return_url = ($default_project_id) ? '/project/'. $default_project_id : '/';
					$this->redirect($return_url);
				}
				else 
				{
					$this->redirect('/newproject/');
				}
			}
		}
		$this->render('login',array('model'=>$model));
	}

	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	
	public function actionRegistration()
	{
		if (Yii::app()->user->getId()) {
			Yii::app()->request->redirect('/site/index/');
		}
		
		$module = Yii::app()->getModule('user');
		
		if ($module->registrationDisabled) {
			throw new CHttpException(404, Yii::t('UserModule.user', 'requested page was not found!'));
		}
		
		$form = new RegistrationForm;
		
		if (($data = Yii::app()->getRequest()->getPost('RegistrationForm')) !== null) {
		
			$form->setAttributes($data);
		
			if ($form->validate()) {
		
				if(Yii::app()->user->createUser($form)) {
						
					Yii::app()->request->redirect('/login/');
				}
		
			}
		}
		
		$this->render('registration', array('model' => $form));
	}
	
}