<?php

class User extends CActiveRecord
{
	public function tableName()
	{
		return 'user';
	}

	public function rules()
	{
		return array(
			array('email, password, nick_name, creation_date, salt, last_visit, full_name', 'required'),
			array('access_level, banned, online_status', 'numerical', 'integerOnly'=>true),
			array('email, full_name', 'length', 'max'=>64),
			array('nick_name, salt, ', 'length', 'max'=>32),
			array('id, email, password, nick_name, access_level, banned, creation_date, online_status, salt, last_visit, phone', 'safe', 'on'=>'search'),
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function validatePassword($password)
	{
		return CPasswordHelper::verifyPassword(
				$password,
				$this->password
		);
	}
	
	public static function hashPassword($password)
	{
		return CPasswordHelper::hashPassword($password);
	}
}
