<?php
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'Route.php' ;
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Web Application',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'ext.json_dataprovider.*',
		'application.modules.option.models.*',
		'application.modules.option.widgets.*',
		'application.modules.project.models.*',
		'application.modules.project.models.Order.*',
		'application.modules.project.models.Project.*',
		'application.modules.project.models.Stat.*',
		'application.modules.project.widgets.*',
		'application.modules.landing.models.*',
		'application.modules.landing.modules.base.widgets.*',
		'ext.mail.YiiMailMessage',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'111111',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
		'project',
		'user',
		'total',
		'landing' => array(
			'modules'=>array(
				'base',
				'category',
				'extlist',
				'textblock',
				'region',
			)
		),
		'image',
	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
			'class' => 'application.modules.user.components.WebUser',
			'returnUrl' => '/',
		),
		'mail' => array(
			'class' => 'ext.mail.YiiMail',
			'transportType' => 'php',
			'viewPath' => 'application.views.mail',
			'logging' => true,
			'dryRun' => false,
		),
		'Smtpmail'=>array(
				'class'=>'application.extensions.smtpmail.PHPMailer',
				'Host'=>"smtp.yandex.ru",
				'Username'=>'info@mail.onlinekonf.ru',
				'Password'=>'nikk2005',
				'Mailer'=>'smtp',
				'Port'=> 465,
				'SMTPAuth'=>true,
				'SMTPSecure' => 'ssl',
				'CharSet' => 'utf-8',
		),
		// uncomment the following to enable URLs in path-format
		'urlManager'=>array(
			'urlFormat'=>'path',
			'urlSuffix' => '/',
			'rules'=>CMap::mergeArray(array(
					'projects/' => array('/total/project/index', 'verb'=>'GET'),				
					
					//project routes
					'external_order/<id:\d+>/' => '/project/remote/order',
					'external_info/<id:\d+>/<iid:\d+>/' => '/project/remote/info',
					'external_pay/<service:\w+>/' => '/project/remote/payment',
					'external_speaker/<id:\d+>/<iid:\d+>' => '/project/remote/speaker',
					'external_product/<id:\d+>/<iid:\d+>/' => '/project/remote/product',
					'external_partner/<id:\d+>/' => '/project/remote/partner',
					
					'project/<id:\d+>/' => '/project/default/index',
					'project/<id:\d+>/sms/' => '/project/send/sms',
					'project/<id:\d+>/paylink/<iid:\d+>/' => '/project/send/paylink',
					'project/<id:\d+>/history/<iid:\d+>/' => '/project/order/history',
						
					'project/<id:\d+>/filter_stat/' => array('/project/stat/filter', 'verb'=>'GET'),
					'project/<id:\d+>/filter_graph/<type:\w+>/<subtype:\w+>/' => array('/project/stat/graph', 'verb'=>'GET'),
					'project/<id:\d+>/manager_stat/' => array('/project/stat/managers', 'verb'=>'GET'),
						
					'project/<id:\d+>/orders/' => array('/project/order/index', 'verb'=>'GET'),
					'project/<id:\d+>/update-stat/' => array('/project/order/states'),
					'project/<id:\d+>/order/' => array('/project/order/create', 'verb'=>'POST'),
	
					'project/<id:\d+>/settings/' => '/project/settings/all',
					array('/project/settings/save', 	'pattern' => 'project/<id:\d+>/setting/<namespace>/', 'verb'=>'POST'),
					
					array('/project/order/update',  'pattern' => 'project/<id:\d+>/order/<iid:\d+>/', 'verb'=>'PUT'),
					array('/project/order/delete',  'pattern' => 'project/<id:\d+>/order/<iid:\d+>/', 'verb'=>'DELETE'),
					array('/project/order/one', 	'pattern' => 'project/<id:\d+>/order/<iid:\d+>/', 'verb'=>'GET'),
					array('/project/order/bind', 	'pattern' => 'project/<id:\d+>/order-bind/<iid:\d+>/', 'verb'=>'GET'),
						
					//common routes
					'/login/'	=> 'user/default/login',
					'/logout/'	=> 'user/default/logout',
					'/reg/'	=> 'user/default/registration',

					//default routes
					'<controller:\w+>/<id:\d+>'=>'<controller>/view',
					'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
					'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
				),
				Route::get('product'),
				Route::get('conferention'),
				Route::get('speaker')
			),
		),
		
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=call',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
			// включаем профайлер
			'enableProfiling'=>true,
			// показываем значения параметров
			'enableParamLogging' => true,
		),
		'errorHandler'=>array(
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
					'categories'=>'app.*',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	'params'=>array(
		'adminEmail'=>'webmaster@example.com',
	),
);