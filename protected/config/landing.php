<?php

return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'������� ��������',
	'preload'=>array('log'),

	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.modules.image.models.*',
	),

	'modules'=>array(
		'landing' => array(
			'modules'=>array(
				'basic',
				'category',
				'extlist',
				'textblock',
			)
		),
		'image'
	),

	// application components
	'components'=>array(
			
		'user',
			
		'urlManager'=>array(
			'urlFormat'=>'path',
			'urlSuffix' => '/',
			'rules'=>array(
				'/' => '/landing/index/index/',
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
				array('/landing/<module>/index/index', 		'pattern' => 'front/<module:\w+>/<render:\w+>/<id:\d+>/', 'verb'=>'GET'),
			),
		),
		
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=landing',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '111111',
			'charset' => 'utf8',
		),
			
		'errorHandler'=>array(
			'errorAction'=>'site/error',
		),
			
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),
	),

	'params'=>array(
		'adminEmail'=>'webmaster@example.com',
	),
);