<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
$main = require 'main.php';
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Console Application',

	// preloading 'log' component
	'preload'=>array('log'),
		
	'import'=>CMap::mergeArray($main['import'], array(
			'application.modules.project.models.*',
            'application.modules.project.models.Call.*',
			'application.modules.project.models.Order.*',
  			'application.modules.project.components.*',
			'application.modules.project.models.Log.*',
			'application.modules.project.models.Delivery.*',
			'application.modules.project.models.Delivery.Constructor.*',
			'application.modules.project.models.Delivery.Collection.*',
			'application.modules.project.models.Delivery.Info.*',
			'application.modules.project.models.Delivery.Order.*',
			'application.modules.project.models.Delivery.Response.*',
			'application.modules.project.models.Delivery.StateChanger.*',
			'application.modules.project.models.services.*',
			'application.modules.project.models.services.delivery.*',
			'application.modules.project.models.services.sms.*',
			'application.modules.project.models.services.sms.Smspilot.*',
	)),
		
	// application components
	'components'=>array(
		'db'=>$main['components']['db'],
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),
	),
);