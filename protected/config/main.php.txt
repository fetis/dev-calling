<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Web Application',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'ext.json_dataprovider.*',
		'application.modules.project.models.*',
		'application.modules.project.widgets.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'111111',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
		'project',
		'user',
		'total',
		'landing',
	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
			'class' => 'application.modules.user.components.WebUser',
			'returnUrl' => '/',
		),
		// uncomment the following to enable URLs in path-format
		'urlManager'=>array(
			'urlFormat'=>'path',
			'urlSuffix' => '/',
			'rules'=>array(
				'external_order/<id:\d+>/' => '/project/remote/order',
			
				'total/' => '/total/default/index',
				'total/orders/' => array('/total/order/index', 'verb'=>'GET'),
				array('/total/order/one', 	'pattern' => 'total/order/<oid:\d+>/', 'verb'=>'GET'),
				'total/products/' => array('/total/product/index', 'verb'=>'GET'),
				'projects/' => array('/total/project/index', 'verb'=>'GET'),
				
				'project/<id:\d+>/' => '/project/default/index',
				'project/<id:\d+>/filter_stat/' => array('/project/stat/filter', 'verb'=>'GET'),
				'project/<id:\d+>/orders/' => array('/project/order/index', 'verb'=>'GET'),
				'project/<id:\d+>/update-stat/' => array('/project/order/states'),
				'project/<id:\d+>/order/' => array('/project/order/create', 'verb'=>'POST'),

				'project/<id:\d+>/settings/' => '/project/settings/all',
				array('/project/settings/save', 	'pattern' => 'project/<id:\d+>/setting/<namespace>/', 'verb'=>'POST'),
				
				'project/<id:\d+>/invite/' => '/project/stuff/invite',
				'project/<id:\d+>/stuff/' => '/project/stuff/index',
				'project/<id:\d+>/allusers/' => '/project/stuff/all',
				'newproject/' => '/project/empty/new',
				
				array('/project/stuff/delete',  'pattern' => 'project/<id:\d+>/stuff/<uid:\d+>/', 'verb'=>'DELETE'),
				
				'project/<id:\d+>/ac_regions/<service>/' => array('/project/autocomplete/regions', 'verb'=>'GET'),
				
				
				'project/<id:\d+>/ac_regions/<service>/' => array('/project/autocomplete/regions', 'verb'=>'GET'),
				
				'project/<id:\d+>/invoice/<oid:\d+>' => '/project/order/invoice',
				
				array('/project/order/update',  'pattern' => 'project/<id:\d+>/order/<oid:\d+>/', 'verb'=>'PUT'),
				array('/project/order/delete',  'pattern' => 'project/<id:\d+>/order/<oid:\d+>/', 'verb'=>'DELETE'),
				array('/project/order/one', 	'pattern' => 'project/<id:\d+>/order/<oid:\d+>/', 'verb'=>'GET'),
				
				'project/<id:\d+>/products/' => array('/project/product/index', 'verb'=>'GET'),
				'project/<id:\d+>/product/' => array('/project/product/create', 'verb'=>'POST'),
				
				array('/project/product/one', 		'pattern' => 'project/<id:\d+>/product/<pid:\d+>/', 'verb'=>'GET'),
				array('/project/product/update', 	'pattern' => 'project/<id:\d+>/product/<pid:\d+>/', 'verb'=>'PUT'),
				array('/project/product/delete', 	'pattern' => 'project/<id:\d+>/product/<pid:\d+>/', 'verb'=>'DELETE'),
				
				'project/<id:\d+>/service/geo/' => array('/project/service/geo', 'verb'=>'POST'),
				'project/<id:\d+>/send/<oid:\d+>/' => array('/project/order/send'),
				'project/<id:\d+>/info/<oid:\d+>/' => array('/project/order/info'),
				
				
				'landing/<id:\d+>/' => '/landing/default/index',
				
				'/login/'	=> 'user/default/login',
				'/logout/'	=> 'user/default/logout',
				'/reg/'	=> 'user/default/registration',
				
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=landing',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '1',
			'charset' => 'utf8',
		),
		'errorHandler'=>array(
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	'params'=>array(
		'adminEmail'=>'webmaster@example.com',
	),
);