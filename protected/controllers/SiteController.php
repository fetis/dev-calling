<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	public function actionIndex()
	{
		if (Yii::app()->user->getId())
		{
			$model = new ProjectUser();
			$project_id = $model->getDefaultProjectId();
			if ($project_id)
			{
				Yii::app()->request->redirect('/project/'.$project_id);
			}
			else 
			{
				Yii::app()->request->redirect('/newproject/');
			}
		}
		else 
		{
			Yii::app()->request->redirect('/login/');
		}
		
	}
	
	public function actionPass()
	{
		$this->render('pass', array(
						'model' => new PassForm(),
						'pass' => (isset($_POST['PassForm'], $_POST['PassForm']['pass']) 
								? CPasswordHelper::hashPassword($_POST['PassForm']['pass']) 
								: false)
						)
					);
	}

	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

}