<?php

class Controller extends CController
{
	public $layout='//layouts/column1';

	public $menu=array();

	public $breadcrumbs=array();
	
	public $model_class=null;
	
	public function init()
	{
		parent::init();
		$requestUri = yii::app()->request->requestUri;
		$repairedRequestUri = $requestUri;
		
		while (false !== strpos($repairedRequestUri, '//'))
		{
			$repairedRequestUri = preg_replace("////", '/', $repairedRequestUri);
		}
		
		if (false === strpos($repairedRequestUri, '?') && '/' !== substr($repairedRequestUri, strlen($repairedRequestUri) - 1, 1))
		{
			$repairedRequestUri = "{$repairedRequestUri}/";
		}
		elseif ('/' !== substr($repairedRequestUri, strpos($repairedRequestUri, '?') - 1, 1))
		{
			$repairedRequestUri = substr($repairedRequestUri, 0, strpos($repairedRequestUri, '?')) . '/' . substr($repairedRequestUri, strpos($repairedRequestUri, '?'));
		}
		if ($repairedRequestUri !== $requestUri)
		{
			yii::app()->request->redirect($repairedRequestUri, true, 301);
		}
	}
	
	
	
}