<?php

class IndexAction extends CAction {
	
	public $attributes = array();
	
	public $check = null;
	
	public $criteria = false;
	
	public function run()
	{
		$model = $this->getController()->loadModel();
		$criteria = $this->criteria;
		$params = array(
				'attributes' => $this->attributes,
				'pagination' => false,
		);
		
		if ($criteria)
		{
			$filter = Yii::app()->request->getQuery('filter', false);
			$sort = Yii::app()->request->getQuery('sort', 't.id Desc');
			$search = Yii::app()->request->getQuery('key', false);
			$criteria = $model->getCriteria($filter, $search, $sort);
			$params['criteria'] = $criteria;
		}
		
		$dataProvider = new JSonActiveDataProvider($model, $params);
		echo $dataProvider->getJsonData();
	}
	
}
