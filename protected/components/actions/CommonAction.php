<?php

class CommonAction extends CAction {
	
	public $method;
	
	public $check = null;
	
	public function run($iid)
	{
		if (isset($this->check) && $this->check)
			ProjectChecker::check($this->check);
		
		$model = $this->getController()->loadModel($iid);
		$method = $this->method;
		$res = $model->$method();
		echo CJSON::encode($res);
	}
	
}

