<?php

class OneAction extends CAction {
	
	public function run($iid)
	{
		$model = $this->getController()->loadModel($iid);
		$res = $model->getAttributes();
		echo CJSON::encode($res);
	}
	
}