<?php 

class Route 
{
	public static function get($key)
	{
		return array(
			"project/<id:\d+>/$key/" => array("/project/$key/index", 'verb'=>'GET'),
			array("/project/$key/save", 	'pattern' => "project/<id:\d+>/$key/", 'verb'=>'POST'),
			array("/project/$key/one", 		'pattern' => "project/<id:\d+>/$key/<iid:\d+>/", 'verb'=>'GET'),
			array("/project/$key/save", 	'pattern' => "project/<id:\d+>/$key/<iid:\d+>/", 'verb'=>'PUT'),
			array("/project/$key/delete", 	'pattern' => "project/<id:\d+>/$key/<iid:\d+>/", 'verb'=>'DELETE'),
		);
	}	
	
}