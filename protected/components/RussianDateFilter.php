<?php 

class RussianDateFilter
{
	public function filter($content)
	{
		if (! $content)
			return "";
		$date = date_parse_from_format('d.m.Y', $content);
		return $date['year'] . '-' . $date['month'] . '-' . $date['day'];
	}
	
	public function filter1($content) {
		if (! $content)
			return "";
		
		$content = str_replace('T', ' ', $content);
		$content = substr($content, 0, 19); 
		
		$date = date_parse_from_format('Y-m-d H:i:s', $content);
		return 		str_pad($date['day'], 2, 0, STR_PAD_LEFT) 
			. '-' . str_pad($date['month'], 2, 0, STR_PAD_LEFT) 
			. '-' . $date['year'] 
			. '&nbsp' . str_pad($date['hour'], 2, 0, STR_PAD_LEFT) 
			. ':' . str_pad($date['minute'], 2, 0, STR_PAD_LEFT) 
			;
	}
	
	public function filter2($content) {
		if (! $content)
			return "";
	
		$date = date_parse_from_format('Y-m-d H:i:s', $content);
		return 		str_pad($date['day'], 2, 0, STR_PAD_LEFT)
		. '-' . str_pad($date['month'], 2, 0, STR_PAD_LEFT)
		. '-' . $date['year']
		;
	}
}