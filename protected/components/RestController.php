<?php 
class RestController extends Controller
{
	public function getJsonInput()
	{
		$body = file_get_contents('php://input');
		LogRequest::log($body);
		return CJSON::decode($body);
	}

	public function beforeAction($action)
	{
		header('Content-Type: application/json; charset=utf-8');
		return parent::beforeAction($action);
	}
	
	protected function afterAction($action)
	{
		parent::afterAction($action);
		Yii::app()->end();
	}
	
}