<?php 

class PhoneFilter {
	
	public function filter($phone)
	{
		if (! $phone)
			return '';
		
		$phone = preg_replace('/\\D/', '', $phone);
		if (strlen($phone) == 10)
		{
			$phone = '7' . $phone;
		}
		
		if (strlen($phone) == 11 && $phone[0] == 8)
		{
			$phone = '7' . substr($phone, 1, 10);
		}

		return $phone;
	}
	
}