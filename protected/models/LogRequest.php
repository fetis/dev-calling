<?php 

class LogRequest {
	
	public static function log($body)
	{
		$user_id = Yii::app()->user->getId();
		Yii::app()->db->createCommand()->insert("request_log", array(
		'user_id' => $user_id,
		'body' => $body,
		));
	}
	
}