<?php

class PassForm extends CFormModel
{
	public $pass;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array('pass', 'safe'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'pass'=>'Password',
		);
	}
}