<?php 

class CallLanding 
{
	const CRM_URI = "http://call.foreven.ru/";
	
	const BACKUP_URI = "http://udarnica5.ru/";
	
	const TEST_CRM_URI = "http://localhost:8087/";
	
	const TEST_BACKUP_URI = "http://localhost:8088/";
	
	const REQ_ORDER = 'external_order/';
	
	const REQ_INFO = 'external_info/';
	
	const REQ_PARTNER = 'external_partner/';
	
	const REQ_PRODUCT = 'external_product/';
	
	const TEST = 0;
	
	public static function sendOrder($id, $token = null, $params = array())
	{
		return self::_prepareUrl(self::REQ_ORDER, $id, $token, $params);
	}
	
	public static function getOrderInfo($id, $token = null, $params = array())
	{
		return self::_prepareUrl(self::REQ_INFO, $id, $token, $params);
	}
	
	public static function getProductInfo($id, $token = null, $params = array())
	{
		return self::_prepareUrl(self::REQ_PRODUCT, $id, $token, $params);
	}
	
	public static function setPartner($id, $token = null, $params = array())
	{
		return self::_prepareUrl(self::REQ_PARTNER, $id, $token, $params);
	}
	
	protected static function _prepareUrl($rel_url, $id, $token, $params)
	{
		$url = $rel_url . $id . '/?';
		if ($token)
		{
			$url .="token=$token";
		}
		if ($param)
		{
			$url .= self::_paramsToUrl($param);
		}
		self::_send($uri);
	}
	
	protected static function _paramsToUrl($param)
	{
		$query = '';
		
		if (! $param)
			return $url;
		
		$params['timestamp'] = time();
		foreach ($params as $key=>$param)
		{
			if (is_array($param))
			{
				foreach($param as $sub_key => $sub_param)
				{
					if (is_array($sub_param)) {
						foreach($sub_param as $subsub_key => $subsub_param)
						{
							$subsub_param = urlencode ($subsub_param);
							$query .="&params[$key][$sub_key][$subsub_key]=$subsub_param";
						}
					}
					else
					{
						$sub_param = urlencode ($sub_param);
						$query .="&params[$key][$sub_key]=$sub_param";
					}
				}
			}
			else
			{
				$param = urlencode ($param);
				$query .="&params[$key]=$param";
			}
		}
		
		return $query;
	}
	
	protected static function _send($uri)
	{
		if (self::TEST)
			return @file_get_contents(self::TEST_CRM_URI . $url);
		else
			return @file_get_contents(self::CRM_URI . $url);
	}
	
}