<div class="form">
<?php $form=$this->beginWidget('CActiveForm'); ?>
 
    <?php echo $form->errorSummary($model); ?>
 
    <div class="row">
        <?php echo $form->label($model,'pass'); ?>
        <?php echo $form->textField($model,'pass') ?>
    </div>
 
    <div class="row submit">
        <?php echo CHtml::submitButton('Generate'); ?>
    </div>
    
    <?php if ($pass):?>
	    <hr/>
	    <?php echo $pass; ?>
	    <hr/>
    <?php endif;?>
 
<?php $this->endWidget(); ?>
</div><!-- form -->