	<script type="text/template" id="tpl-modal-form">
		  <div class="modal-dialog">
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 id="js-modal-title" class="modal-title"><%=title%></h4>
			  </div>
			  <div class="modal-body js-popup-windows-cont">
			  </div>
			  <div class="modal-footer">
				<button class="btn <%=btn_class%> js-btn-submit" type="button"><%=submit%></button>
				<button class="btn btn-default js-btn-cancel" data-dismiss="modal" type="button"><%=cancel%></button>
			  </div>
			</div><!-- /.modal-content -->
		  </div><!-- /.modal-dialog -->
	</script>
	
