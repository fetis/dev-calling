<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>CRM. <?=$this->project->label?> - обработка заказов</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <link rel="stylesheet" href="/css/bootstrap.css">
        <link rel="stylesheet" href="/css/bootstrap-theme.css">
		<link rel="stylesheet" href="/css/project.css">
        <link rel="stylesheet" href="/css/main.css">
		<link rel="stylesheet" href="/vendors/jui/css/ui-lightness/jquery-ui-1.10.4.custom.css">
		<link rel="stylesheet" href="/vendors/datetimepicker/jquery.datetimepicker.css">
		
        <script src="/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    <body>
    <?php $this->widget('CommonDataWidget')?>
    <?php $this->widget('NavBarWidget')?>
    <?php $this->widget('JumbotronWidget', array('title' => $this->project->label))?>
    <?php $this->widget('ManagerPanelWidget')?>
    
    <div class="container main-container">
    	
    	<?php /*
    		$model = Order::model()->findByPk(5);
    		$model->getConfirmation()->confirm();
    		*/
    	?>
    	
    	<?php echo $content; ?>
    	
    	<?php $this->widget('OTabsWidget', array( 'tabs' => array(
    		'settings' 			=> array('class' => 'OptionTabSettingsWidget', 'css' => 'project-settings-tab'),
    		'orders' 			=> array('class' => 'OptionTabOrderListWidget', 'active' => true),
    		'stat' 				=> array('class' => 'OptionTabStatisticsWidget'),
    	)))?>
    	
		<hr/>

		<footer>
			<p>&copy; Lepotart 2012-<?=date('Y')?></p>
		</footer>
		
		<div id="shadow" class="modal fade"></div>
		
		<!-- Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		</div><!-- /.modal -->
	
		<?php 
		$scripts = array(
			'modalForm',
			'managerRow','managerRemove',
		);
		foreach($scripts as $script) {
			$this->beginContent("//layouts/scripts/$script");
			$this->endContent();
		}
		?>
	
    </div> <!-- /.main-container -->        
		<script src="/js/vendor/jquery-1.10.1.min.js"></script>
        <script src="/js/vendor/bootstrap.min.js"></script>
        <script src="/vendors/maskedinput/jquery.maskedinput.min.js"></script>
		<script src="/vendors/md5/md5.min.js"></script>
		
		<script src="/vendors/underscore/underscore-min.js"></script>
        <script src="/vendors/backbone/backbone.js"></script>
		<script src="/vendors/backbone/backbone-validation.js"></script>
		<script src="/vendors/jquery.arrow-increment/jquery.arrow-increment.js"></script>
		<script src="/vendors/highcharts/highcharts.js"></script>
		<script src="/vendors/highcharts/modules/exporting.js"></script>
		
		<script src="/vendors/jui/js/i18n/jquery.ui.datepicker-ru.min.js"></script>
		<script src="/vendors/jui/js/jquery-ui-1.10.4.custom.min.js"></script>
		
		<script src="/vendors/datetimepicker/jquery.datetimepicker.js"></script>
		
		<script src="/js/app.js"></script>
		<script src="/js/bbGrid.js"></script>
		<script src="/js/bbGridFilter.js"></script>
		
		<script src="/js/plugins/highcharts/views.js"></script>
		<script src="/js/plugins/jui/views.js"></script>
		
		<script src="/js/project/views.js"></script>
		
		<script src="/js/project/orders/models.js"></script>
		<script src="/js/project/orders/filters/views.js"></script>
		<script src="/js/project/orders/views.js"></script>
		<script src="/js/project/orders/main.js"></script>
		
		<script src="/js/project/products/main.js"></script>
    </body>
</html>
