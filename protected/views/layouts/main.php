<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <link rel="stylesheet" href="/css/bootstrap.css">
        <style>
            body {
                padding-top: 50px;
                padding-bottom: 20px;
            }
        </style>
        <link rel="stylesheet" href="/css/bootstrap-theme.css">
		<link rel="stylesheet" href="/css/login.css">
        <link rel="stylesheet" href="/css/main.css">

        <script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    <body>
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">
			<img src="/img/logo-min.png" alt="landing"/>&nbsp;&nbsp;Landing&nbsp;&nbsp;&nbsp;
		  </a>
        </div>
        <div class="navbar-collapse collapse">
		  <ul class="nav navbar-nav navbar-right">
			<!-- li><a href="/pricing/">Pricing</a></li-->
			<!-- li><a href="/about/">About</a></li-->	
		  <?php if (Yii::app()->user->getId()):?>
		  	<?php $user = Yii::app()->user;?>
		 	<?php $this->widget('ProjectListWidget')?>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span>&nbsp;<?php echo $user->getState('nick_name')?><b class="caret"></b></a>
				<ul class="dropdown-menu">
				  <!-- li><a href="/dashboard/">Профиль</a></li>
				  <li><a href="/profile/">Управление аккаунтом</a></li>
				  <li><a href="#">Пригласить друга</a></li>
				  <li class="divider"></li-->
				  <li><a href="/logout/">Выйти</a></li>
				</ul>
			</li>
          
          <?php else:?>
          	<li><a href="/login/">Войти</a></li>	
          	<li><a href="/reg/">Зарегистрироваться</a></li>
          <?php endif;?>
          </ul>		
		  <!-- form class="search-form navbar-form navbar-right" role="search">
		    <div class="form-group">
		      <input type="text" class="form-control" placeholder="Project">
		    </div><button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
		  </form-->

	  
        </div><!--/.navbar-collapse -->
      </div>
    </div>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
    
    	<div class="container">
			<?php echo $content?>
		</div>
		
    </div>

    <div class="container">

      <hr>

      <footer>
        <p>&copy; Company 2013</p>
      </footer>
	  
    </div> <!-- /container -->        
		<script src="/js/vendor/jquery-1.10.1.min.js"></script>

        <script src="/js/vendor/bootstrap.min.js"></script>

        <script src="/js/main.js"></script>
    </body>
</html>
